[PSquared](https://gitlab.com/nest.lbl.gov/psquared) provides the basic template needed to deploy the [psquared-core](https://gitlab.com/nest.lbl.gov/psquared-core) project as a web application.


To deploy this basic application you will need a JEE Application Server. The discussion here will use the standalone [Wildfly](http://wildfly.org/) implementation of this, so if you are using a different implementation you may need to alter various steps. Where known these alterations will mentioned but this may not be a complete list.


## Server Preparation ##

The server will need both Java for deployment and Git for access to some initialization files. Therefore you may need to install the appropriate packages that supply these. On Ubuntu 16.0.4 the following can be run.

    apt-get install openjdk-8-jdk
    apt-get install git-all
    apt-get install maven


The server needs to be provided with a suitable DataSource named `PSquared`. This is configured in the following file

    wildfly/standalone/configuration/standalone.xml

by adding an appropriate `datasource` element, normally placed after the element declaring the `ExampleDS` datasource. (Other JEE implementation will have a different approach.)

That datasource also need to be initialized. The following links (with `PSQUARED_VERSION` set to be the appropriate value) provide files that can be used initialize H2 and PostgreSQL instances respectively. (Other DBMSs are beyond the scope of this document)

    PSQUARED_VERSION=1.0.1
    http://nest.lbl.gov/projects/psquared/resources/${PSQUARED_VERSION}/h2/load.sql
    http://nest.lbl.gov/projects/psquared/resources/${PSQUARED_VERSION}/psql/load.sql

Before executing the PostgreSQL version, you will need to create the appropriate `ROLE` and `DATABASE`.

    CREATE ROLE psquared WITH PASSWORD 'psqrd_psql';
    CREATE DATABASE psquared OWNER psquared;


### `PSquared` Server Deployment ###

The simplest way to deploy the `psquared` service to so download a copy of the current `psquared` repository and build the WAR file using the following commands, with `PSQUARED_VERSION` set to be the appropriate value.

    PSQUARED_VERSION=1.0.1
    mkdir psquared
    git archive \
        --remote=git@gitlab.com:nest.lbl.gov/psquared.git \
        ${PSQUARED_VERSION} | tar -x -C psquared
    cd psquared
    mvn clean package
    cp target/psquared-${PSQUARED_VERSION}.war \
            ${HOME}/wildfly/standalone/deployments/psquared.war

The last command is Wildfly specific and will need to be changed for other JEE implementations.


### Preparation for RabbitMQ scheduler ###

In order to use the default RabbitMQ scheduler that is supplied with `PSquared`, the connection credentials must be supplied to the server. This is done by a properties file stored at the following location.

    ${HOME}/.psquared/server/rabbitMQ.properties

The file must contain the following properties.

*   `gov.lbl.nest.psquared.submission.rabbitmq.user`
*   `gov.lbl.nest.psquared.submission.rabbitmq.password`
*   `gov.lbl.nest.psquared.submission.rabbitmq.host`
*   `gov.lbl.nest.psquared.submission.rabbitmq.vhost`

And it can optionally contain the following.

*   `gov.lbl.nest.psquared.submission.rabbitmq.port`

*Note:* The location of the file is specified in the configuration of a `scheduler` and the one discussed here is only for the default instance.


## Python environment ##

Both the command line interface (`pp-cli`) and the various runners for `PSquared` are written in python. In order to make sure that the right python environment is available it is recommended to use the `conda` system for managing the python environment, therefore you need to install that package. The following commands can do that for ubuntu 16.0.4.

    wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
    bash Miniconda2-latest-Linux-x86_64.sh
    conda create -p ${HOME}/.psquared/client/conda pip
    source activate ${HOME}/.psquared/client/conda
    pip install requests
    pip install pika

In answer to the questions you can take the defaults or choose a different location, such as `opt/conda` for the install. In either case the easiest way to make the `conda` command available is to then link it into the `/usr/local/bin` directory.

*Note:* release 0.10.0 has an issue with handling system interrupt, so until a new release is published the three file in the following git commit should be used as replacements to the same files in the release.

    https://github.com/pika/pika/commit/59dfa06cb18ce53e12979547312941eba04b0a6d#diff-9983a471561afeb12f00eb610e0f0962


## The `PSquared` Runner ##

`PSquared` has various types of runner applications that are designed to execute the commands that are bound to a particular version of a configuration. The type of runner that processes the command depends on the type of Scheduler that is being invoked. In the default deployment the following schedulers exist.

*   DoNothing - which does not run the command but prints submission information to application log file;
*   Internal - which processes the command inside the server using the `runner/pp_engine.py` runner;
*   Background - which runs the command in background outside the server, using the `runner/pp_engine.py` runner;
*   RabbitMQ - which publishes command to a RabbitMQ queue from which the `runner.bash/Consume.py` runner will pull the commands and process then.


### Runner Preparation ###

The `pp_engine` runner is written to process command asynchronously within the context of the `PSquared` system. It handles the bookkeeping and such that is necessary to keep track of each requested process. This means that it will need to appropriate files installed. If the runner shares the `$HOME` file system with the server, the server will install the necessary files after its first activation. (The easiest way to do that is simply run the `pp-cli` once against the server - see below.)

If the runner does not share the `$HOME` file system with the server then at the server have been activated at least one, the following area should be copied onto any nodes that will be executing runners.

    ${HOME}/.psquared/runner

Also the `conda` package, should be installed on those nodes and set up as explained above.


### RabbitMQ Runner Preparation ###

The `consume.py` runner is written to use the [`pika`](https://pypi.python.org/pypi/pika) python package to pull processing requests of a RabbitMQ queue and then process them. This was installed earlier when setting up the python environment.

As with the RabbitMQ scheduler, the runner needs connection credentials in order to connect to the RabbitMQ server. This is done by an INI file whose default location is the following.

    ${HOME}/.psquared/runner/rabbitMQ.ini

That file must contain a section named `RabbitMQ` and that must container the following assignments.

*   `user`
*   `password`
*   `host`
*   `vhost`

And it can optionally contain the following.

*   `port`

*Note:* The location of the file can be specified as an argument to the runner so that different runners can consume different queues.
 

## Client Preparation ##

The RESTful command line interface client, `pp-cli`, also runs in a python environment so, as with the runner, the client needs access to a system for managing the python environment. For this discussion `conda` will provide that, and its installation has already been reviewed above in the section on Pika Runner Preparation. Once that is installed, the following commands creates the necessary environment for the client.

    mkdir -p ${HOME}/.psquared/client
    conda create -p ${HOME}/.psquared/client/conda
    . ${HOME}/.psquared/client/conda/bin/activate \
            ${HOME}/.psquared/client/conda
    pip install requests
    pip install pika


    PSQUARED_VERSION=1.0.1
    mkdir ${HOME}/bin
    export PATH=${HOME}/bin:${PATH}
    wget -O ${HOME}/bin/pp-cli \
            http://nest.lbl.gov/projects/psquared/resources/${PSQUARED_VERSION}/client/pp-cli
    chmod +x bin/pp-cli

## Using the Client ##

With the client, server and runners installed it is now possible to run some basic examples of how to use the client. By default the client expects the server to be available at the following URL.

    http://localhost:8080/psquared/local/report/

however this can be changed (and probably should!) by setting the `PP_APPLICATION` environmental variable.

The first example is to run the following command, which will list the currently active configurations.

    pp-cli

The next example lists the currently active version of the `Test` configuration returned by the previous command.

    pp-cli Test

The next example gets a list of items that `PSquared` has been attempted to processed with a specified version of a configuration.

    pp-cli -V -1 Test

Next, the following can be used to submit and item called `Zero` to be processed by a specified version of a configuration.

    pp-cli -s -V -1 Test Zero

The "DoNothing" scheduler is the default scheduler after deployment (this can be changed), so that the `Zero` item will still be in the "SUBMITTED" state for the -1 version of the `Test` configuration and the command that needs to be executed is printed into the servers log file. The status can be shown as follows.

    pp-cli -V -1 Test Zero



Item 'Fred' needs to be processed with version -1 of 'Test'. The submission URI is 'http://localhost:8080/psquared/local/report/state/8252'
