package gov.lbl.nest.psquared;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * This class test that a {@link PSquared} implementation fulfills its
 * requirements.
 * 
 * @author patton
 */
@SuppressWarnings("unchecked")
public abstract class AbstractPsquaredTest {

    // public static final member data

    /**
     * The known configuration name to use in tests.
     */
    public static final Family FAMILY = new Family() {

        List<ProcessDefinition> processDefinitions = new ArrayList<ProcessDefinition>();

        @Override
        public ProcessDefinition getDefaultDefinition() {
            if (processDefinitions.isEmpty()) {
                return null;
            }
            return processDefinitions.get(0);
        }

        @Override
        public String getDescription() {
            return "Test configurations";
        }

        @Override
        public String getIdentity() {
            return "-1";
        }

        @Override
        public String getName() {
            return "Test";
        }

        @Override
        public List<? extends ProcessDefinition> getProcessDefinitions() {
            return processDefinitions;
        }

        @Override
        public boolean isActive() {
            return true;
        }
    };

    /**
     * The known configuration name to use in tests.
     */
    public static final ProcessDefinition PROCESS = new ProcessDefinition() {

        @Override
        public KnownScheduler getDefaultScheduler() {
            return null;
        }

        @Override
        public String getDescription() {
            return "No operational version of test";
        }

        @Override
        public Family getFamily() {
            return FAMILY;
        }

        @Override
        public String getIdentity() {
            return "1";
        }

        @Override
        public String getName() {
            return "-1";
        }

        @Override
        public boolean isActive() {
            return true;
        }

        @Override
        public Boolean isDevelopment() {
            return false;
        }
    };

    static {
        ((List<ProcessDefinition>) (FAMILY.getProcessDefinitions())).add(PROCESS);
    }

    static final UriHandler TEST_URI_HANDLER = new UriHandler() {

        @Override
        public URI buildFromIdentity(String identity) {
            try {
                return new URI("http://localhost/report/state/" + identity);
            } catch (URISyntaxException e) {
                e.printStackTrace();
                fail("URI can not be constructed");
            }
            return null;
        }

        @Override
        public String getIdentity(URI uri) {
            final String s = uri.toString();
            final String[] components = s.split("/");
            return components[components.length - 1];
        }

        @Override
        public URI resolve(String str) {
            try {
                return new URI("http://localhost/").resolve(str);
            } catch (URISyntaxException e) {
                e.printStackTrace();
                fail("URI can not be constructed");
            }
            return null;
        }
    };

    /**
     * The known default submission instructions name to use in tests.
     */
    public static final String DEFAULT_INSTRUCTION = "instructions1";

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The file id to use in tests.
     */
    private static final String ITEM_ID = "item1";

    // private static member data

    // private instance member data

    private static void releaseHold(final MockScheduler mockScheduler,
                                    final Object holdingObject) {
        while (!mockScheduler.holding.get()) {
            Thread.yield();
        }
        synchronized (holdingObject) {
            holdingObject.notifyAll();
        }
    }

    /**
     * The {@link MockScheduler} instance used in tests.
     */
    private MockScheduler mockScheduler;

    // constructors

    // instance member method (alphabetic)

    /**
     * The instance being tested.
     */
    private PSquared testObject;

    /**
     * Attempts to make an illegal transition.
     */
    private void attemptIllegalTransition(String target) throws AlreadyExitedException {
        final State initial = testObject.getState(ITEM_ID,
                                                  PROCESS);
        try {
            testObject.exitState(ITEM_ID,
                                 PROCESS,
                                 target,
                                 "Illegal Transistion",
                                 TEST_URI_HANDLER);
            fail("ForbiddenTransitionException was not thrown");
        } catch (ForbiddenTransitionException e) {
            // Ignore
        }
        assertEquals(initial,
                     testObject.getState(ITEM_ID,
                                         PROCESS),
                     "The pairing changed state when attempting an illegal transition");
    }

    /**
     * Checks that an item moves into its executing state correctly.
     * 
     * @throws InterruptedException
     */
    private void checkCancellation1() throws InterruptedException,
                                      AlreadyExitedException,
                                      ForbiddenTransitionException {
        final String note = "Test cancellation";
        final MockScheduler scheduler = getMockScheduler();
        scheduler.submittingHold = new Object();
        checkSubmitted();
        testObject.exitState(ITEM_ID,
                             PROCESS,
                             "cancel",
                             note,
                             TEST_URI_HANDLER);
        assertEquals(State.READY,
                     testObject.getState(ITEM_ID,
                                         PROCESS),
                     "The pairing did not successfully cancelled.");
        releaseHold(scheduler,
                    scheduler.submittingHold);
        mockScheduler.enteredExecuting();
        assertTrue(mockScheduler.failedToExecute,
                   "The transition to Execution state did not fail");
    }

    /**
     * Checks that an item moves into its executing state correctly.
     * 
     * @throws InterruptedException
     */
    private void checkCancellation2() throws InterruptedException,
                                      AlreadyExitedException,
                                      ForbiddenTransitionException {
        final String note = "Test cancellation";
        final MockScheduler scheduler = getMockScheduler();
        scheduler.executingHold = new Object();
        checkExecuting();
        testObject.exitState(ITEM_ID,
                             PROCESS,
                             "cancel",
                             note,
                             TEST_URI_HANDLER);
        assertEquals(State.READY,
                     testObject.getState(ITEM_ID,
                                         PROCESS),
                     "The pairing did not successfully cancelled.");
        releaseHold(scheduler,
                    scheduler.executingHold);
        mockScheduler.exitedExecution();
        assertTrue(mockScheduler.failedToExecute,
                   "The transition to Completed state did not fail");
    }

    /**
     * Checks that an item moves into its executing state correctly.
     * 
     * @throws InterruptedException
     */
    private void checkExecuting() throws InterruptedException,
                                  AlreadyExitedException,
                                  ForbiddenTransitionException {
        final MockScheduler scheduler = getMockScheduler();
        scheduler.submittingHold = new Object();
        checkSubmitted();
        releaseHold(scheduler,
                    scheduler.submittingHold);
        scheduler.enteredExecuting();
        assertEquals(State.EXECUTING,
                     testObject.getState(ITEM_ID,
                                         PROCESS),
                     "The pairing did not successfully submitted.");
    }

    /**
     * Checks that an item that failed end up in the correct state.
     * 
     * @throws InterruptedException
     */
    private void checkFailed() throws InterruptedException,
                               AlreadyExitedException,
                               ForbiddenTransitionException {
        executeProcess();
        assertEquals(State.FAILED,
                     testObject.getState(ITEM_ID,
                                         PROCESS),
                     "The pairing did not successfully failed.");
    }

    /**
     * Checks that an item is processed correctly.
     * 
     * @throws InterruptedException
     */
    private void checkProcessed() throws InterruptedException,
                                  AlreadyExitedException,
                                  ForbiddenTransitionException {
        executeProcess();
        assertEquals(State.PROCESSED,
                     testObject.getState(ITEM_ID,
                                         PROCESS),
                     "The pairing did not successfully processed.");
    }

    /**
     * Checks that an item is submitted correctly.
     */
    private void checkSubmitted() throws ForbiddenTransitionException,
                                  AlreadyExitedException,
                                  ForbiddenTransitionException {
        final String note = "Test execution";
        testObject.exitState(ITEM_ID,
                             PROCESS,
                             "submit",
                             note,
                             TEST_URI_HANDLER);
        assertEquals(State.SUBMITTED,
                     testObject.getState(ITEM_ID,
                                         PROCESS),
                     "The pairing did not successfully submitted.");
    }

    /**
     * Shepards the execution of processing an item/process pairing.
     * 
     * @throws InterruptedException
     */
    private void executeProcess() throws InterruptedException,
                                  AlreadyExitedException,
                                  ForbiddenTransitionException {
        final MockScheduler scheduler = getMockScheduler();
        scheduler.executingHold = new Object();
        checkExecuting();
        releaseHold(scheduler,
                    scheduler.executingHold);
        scheduler.exitedExecution();
    }

    /**
     * Returns the {@link MockScheduler} instance used in tests.
     * 
     * @return the {@link MockScheduler} instance used in tests.
     */
    protected MockScheduler getMockScheduler() {
        if (null == mockScheduler) {
            mockScheduler = new MockScheduler(TEST_URI_HANDLER);
        }
        return mockScheduler;
    }

    /**
     * Sets the object to be tested.
     * 
     * @param psquared
     *            the object to be tested.
     */
    protected void setTestObject(final PSquared psquared) {
        testObject = psquared;
    }

    /**
     * @throws Exception
     */
    @AfterEach
    protected void tearDown() throws Exception {
        mockScheduler = null;
    }

    /**
     * Test that a cancellation before it starts works. executing.
     * 
     * 
     * @throws InterruptedException
     *             when the test in interrupted unexpectedly.
     * @throws AlreadyExitedException
     *             when attempting a transition from a state that's already been
     *             exited.
     * @throws ForbiddenTransitionException
     *             when attempting a transition that is forbidden.
     */
    @Test
    @DisplayName("Cancellation")
    public void testCancellation1() throws InterruptedException,
                                    AlreadyExitedException,
                                    ForbiddenTransitionException {
        getMockScheduler().targetState = State.READY;
        checkCancellation1();
    }

    /**
     * Test that a cancellation before it starts works. executing.
     * 
     * 
     * @throws InterruptedException
     *             when the test in interrupted unexpectedly.
     * @throws AlreadyExitedException
     *             when attempting a transition from a state that's already been
     *             exited.
     * @throws ForbiddenTransitionException
     *             when attempting a transition that is forbidden.
     */
    @Test
    @DisplayName("Cancellation 2")
    public void testCancellation2() throws InterruptedException,
                                    AlreadyExitedException,
                                    ForbiddenTransitionException {
        getMockScheduler().targetState = State.READY;
        checkCancellation2();
    }

    /**
     * Test that executing a item/process pairing works.
     * 
     * @throws InterruptedException
     *             when the test in interrupted unexpectedly.
     * @throws AlreadyExitedException
     *             when attempting a transition from a state that's already been
     *             exited.
     * @throws ForbiddenTransitionException
     *             when attempting a transition that is forbidden.
     */
    @Test
    @DisplayName("Executing")
    public void testExecuting() throws InterruptedException,
                                AlreadyExitedException,
                                ForbiddenTransitionException {
        getMockScheduler().targetState = State.EXECUTING;
        checkExecuting();
    }

    /**
     * Test that a failure of a item/process pairing works.
     * 
     * @throws InterruptedException
     *             when the test in interrupted unexpectedly.
     * @throws AlreadyExitedException
     *             when attempting a transition from a state that's already been
     *             exited.
     * @throws ForbiddenTransitionException
     *             when attempting a transition that is forbidden.
     */
    @Test
    @DisplayName("Failed")
    public void testFailed() throws InterruptedException,
                             AlreadyExitedException,
                             ForbiddenTransitionException {
        getMockScheduler().targetState = State.FAILED;
        checkFailed();
    }

    /**
     * Test that illegal transitions out of executing throw an exception.
     * 
     * @throws InterruptedException
     *             when the test in interrupted unexpectedly.
     * @throws AlreadyExitedException
     *             when attempting a transition from a state that's already been
     *             exited.
     * @throws ForbiddenTransitionException
     *             when attempting a transition that is forbidden.
     */
    @Test
    @DisplayName("Illegal Transition out of Executing")
    public void testIllegalTransitionsOutOfExecuting() throws InterruptedException,
                                                       AlreadyExitedException,
                                                       ForbiddenTransitionException {
        testExecuting();
        attemptIllegalTransition("submit");
        attemptIllegalTransition("executing");
    }

    /**
     * Test that illegal transitions out of failed throw an exception.
     * 
     * @throws InterruptedException
     *             when the test in interrupted unexpectedly.
     * @throws AlreadyExitedException
     *             when attempting a transition from a state that's already been
     *             exited.
     * @throws ForbiddenTransitionException
     *             when attempting a transition that is forbidden.
     */
    @Test
    @DisplayName("Illegal Transition out of Failed")
    public void testIllegalTransitionsOutOfFailed() throws InterruptedException,
                                                    AlreadyExitedException,
                                                    ForbiddenTransitionException {
        testFailed();
        attemptIllegalTransition("submit");
        attemptIllegalTransition("executing");
        attemptIllegalTransition("processed");
        attemptIllegalTransition("failed");
    }

    /**
     * Test that illegal transitions out of processed throw an exception.
     * 
     * @throws InterruptedException
     *             when the test in interrupted unexpectedly.
     * @throws AlreadyExitedException
     *             when attempting a transition from a state that's already been
     *             exited.
     * @throws ForbiddenTransitionException
     *             when attempting a transition that is forbidden.
     */
    @Test
    @DisplayName("Illegal Transition out of Processed")
    public void testIllegalTransitionsOutOfProcessed() throws InterruptedException,
                                                       AlreadyExitedException,
                                                       ForbiddenTransitionException {
        testProcessed();
        attemptIllegalTransition("submit");
        attemptIllegalTransition("executing");
        attemptIllegalTransition("processed");
        attemptIllegalTransition("failed");
    }

    /**
     * Test that illegal transitions out of ready throw an exception.
     * 
     * @throws InterruptedException
     *             when the test in interrupted unexpectedly.
     * @throws AlreadyExitedException
     *             when attempting a transition from a state that's already been
     *             exited.
     * @throws ForbiddenTransitionException
     *             when attempting a transition that is forbidden.
     */
    @Test
    @DisplayName("Illegal Transition out of Ready")
    public void testIllegalTransitionsOutOfReady() throws InterruptedException,
                                                   AlreadyExitedException,
                                                   ForbiddenTransitionException {
        testCancellation1();
        attemptIllegalTransition("executing");
        attemptIllegalTransition("processed");
        attemptIllegalTransition("failed");
    }

    /**
     * Test that illegal transitions out of submitted throw an exception.
     * 
     * @throws InterruptedException
     *             when the test in interrupted unexpectedly.
     * @throws AlreadyExitedException
     *             when attempting a transition from a state that's already been
     *             exited.
     * @throws ForbiddenTransitionException
     *             when attempting a transition that is forbidden.
     */
    @Test
    @DisplayName("Illegal Transition out of Submitted")
    public void testIllegalTransitionsOutOfSubmitted() throws InterruptedException,
                                                       AlreadyExitedException,
                                                       ForbiddenTransitionException {
        testSubmitted();
        attemptIllegalTransition("submit");
        attemptIllegalTransition("processed");
    }

    /**
     * Test that processing a item/process pairing works.
     * 
     * @throws InterruptedException
     *             when the test in interrupted unexpectedly.
     * @throws AlreadyExitedException
     *             when attempting a transition from a state that's already been
     *             exited.
     * @throws ForbiddenTransitionException
     *             when attempting a transition that is forbidden.
     */
    @Test
    @DisplayName("Processed")
    public void testProcessed() throws InterruptedException,
                                AlreadyExitedException,
                                ForbiddenTransitionException {
        getMockScheduler().targetState = State.PROCESSED;
        checkProcessed();
    }

    /**
     * Test that a failure of a item/process pairing can be resolved.
     * 
     * @throws InterruptedException
     *             when the test in interrupted unexpectedly.
     * @throws AlreadyExitedException
     *             when attempting a transition from a state that's already been
     *             exited.
     * @throws ForbiddenTransitionException
     *             when attempting a transition that is forbidden.
     */
    @Test
    @DisplayName("Resolved")
    public void testResolved() throws InterruptedException,
                               AlreadyExitedException,
                               ForbiddenTransitionException {
        getMockScheduler().targetState = State.FAILED;
        checkFailed();
        final String note = "Test resolution";
        testObject.exitState(ITEM_ID,
                             PROCESS,
                             "resolved",
                             note,
                             TEST_URI_HANDLER);
        assertEquals(State.READY,
                     testObject.getState(ITEM_ID,
                                         PROCESS),
                     "The pairing did not successfully resolved.");
    }

    // static member methods (alphabetic)

    /**
     * Test that submitting a item/process pairing for the first time works.
     * 
     * @throws InterruptedException
     *             when the test in interrupted unexpectedly.
     * @throws AlreadyExitedException
     *             when attempting a transition from a state that's already been
     *             exited.
     * @throws ForbiddenTransitionException
     *             when attempting a transition that is forbidden.
     */
    @Test
    @DisplayName("Submitted")
    public void testSubmitted() throws InterruptedException,
                                AlreadyExitedException,
                                ForbiddenTransitionException {
        getMockScheduler().targetState = State.SUBMITTED;
        checkSubmitted();
    }

    // Description of this object.
    // @Override
    // public String toString() {}
}
