package gov.lbl.nest.psquared;

import java.net.URI;
import java.util.concurrent.atomic.AtomicBoolean;

import gov.lbl.nest.psquared.ejb.SchedulerImpl;
import gov.lbl.nest.psquared.ejb.TransitionImpl;
import jakarta.enterprise.inject.Alternative;

/**
 * This class implements the {@link KnownScheduler} interface to be used in
 * testing.
 * 
 * @author patton
 */
@Alternative
public class MockScheduler extends
                           SchedulerImpl {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // instance member data

    /**
     * 
     */
    AtomicBoolean holding = new AtomicBoolean(false);

    /**
     * The first {@link Throwable}, if any caught by this object.
     */
    Throwable throwable;

    /**
     * The object used to hold executing jobs.
     */
    Object executingHold;

    /**
     * THe flag indicating that the process failed to execute properly.
     */
    boolean failedToExecute = false;

    /**
     * The object used to hold submitted jobs.
     */
    Object submittingHold;

    /**
     * The resulting state for the executing process.
     */
    public State targetState;

    /**
     * The {@link Thread} instance used to queue an item/process pairing.
     */
    private Thread queuing;

    /**
     * The {@link Thread} instance used to execute an item/process pairing.
     */
    private Thread executing;

    /**
     * The {@link UriHandler} used to create realized state URIs.
     */
    private final UriHandler uriHandler;

    // constructors

    /**
     * Creates an instance of this class.
     * 
     * @param uriHandler
     *            the {@link UriHandler} used to create realized state URIs.
     */
    public MockScheduler(UriHandler uriHandler) {
        this.uriHandler = uriHandler;
    }

    // instance member method (alphabetic)

    /**
     * Awaits for the an item/process pair to enter the executing state.
     * 
     * @throws InterruptedException
     *             when unexpectedly interrupted.
     */
    public void enteredExecuting() throws InterruptedException {
        if (null != queuing) {
            queuing.join();
        }
    }

    /**
     * Awaits for the an item/process pair to have exited the executing state.
     * 
     * @throws InterruptedException
     *             when unexpectedly interrupted.
     */
    public void exitedExecution() throws InterruptedException {
        if (null != executing) {
            executing.join();
        }
    }

    @Override
    public String submit(final String item,
                         final ProcessDefinition process,
                         final URI submissionUri,
                         final PSquared psquared) {
        if (State.SUBMITTED == targetState) {
            return null;
        }
        final Runnable runnable = new Runnable() {

            @SuppressWarnings("synthetic-access")
            @Override
            public void run() {
                if (null != submittingHold) {
                    try {
                        synchronized (submittingHold) {
                            holding.set(true);
                            submittingHold.wait();
                            holding.set(false);
                        }
                    } catch (InterruptedException e) {
                        if (null == throwable) {
                            throwable = e;
                        }
                        return;
                    }
                }
                final URI transitionUri;
                try {
                    transitionUri = uriHandler.buildFromIdentity(((TransitionImpl) psquared.exitState(submissionUri,
                                                                                                      "executing",
                                                                                                      null,
                                                                                                      uriHandler)).getIdentity());
                } catch (ForbiddenTransitionException e) {
                    e.printStackTrace();
                    return;
                } catch (AlreadyExitedException e1) {
                    failedToExecute = true;
                    return;
                }
                if (targetState == State.EXECUTING) {
                    return;
                }
                final Runnable nestedRunnable = new Runnable() {

                    @Override
                    public void run() {
                        if (null != executingHold) {
                            try {
                                synchronized (executingHold) {
                                    holding.set(true);
                                    executingHold.wait();
                                    holding.set(false);
                                }
                            } catch (InterruptedException e) {
                                if (null == throwable) {
                                    throwable = e;
                                }
                                return;
                            }
                        }
                        try {
                            if (targetState == State.PROCESSED) {
                                psquared.exitState(transitionUri,
                                                   "processed",
                                                   null,
                                                   uriHandler);
                                return;
                            }
                            if (targetState == State.FAILED) {
                                psquared.exitState(transitionUri,
                                                   "failed",
                                                   null,
                                                   uriHandler);
                                return;
                            }
                            psquared.exitState(transitionUri,
                                               "cancel",
                                               null,
                                               uriHandler);
                        } catch (ForbiddenTransitionException e) {
                            e.printStackTrace();
                        } catch (AlreadyExitedException e1) {
                            failedToExecute = true;
                            return;
                        }
                        if (targetState == State.EXECUTING) {
                            return;
                        }
                    }
                };
                executing = new Thread(nestedRunnable);
                executing.start();
            }
        };
        queuing = new Thread(runnable);
        queuing.start();
        return null;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
