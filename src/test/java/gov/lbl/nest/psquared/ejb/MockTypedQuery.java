package gov.lbl.nest.psquared.ejb;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jakarta.persistence.FlushModeType;
import jakarta.persistence.LockModeType;
import jakarta.persistence.NoResultException;
import jakarta.persistence.Parameter;
import jakarta.persistence.TemporalType;
import jakarta.persistence.TypedQuery;

/**
 * The {@link TypedQuery} implementation used in unit tests.
 * 
 * @author patton
 * 
 * @param <X>
 *            the class to which the query is typed.
 */
public class MockTypedQuery<X> implements
                           TypedQuery<X> {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The NameQuery to see if a definition within a family already exists with a
     * given name.
     */
    private static final String IS_EXISTING_DEFINITION = "isExistingDefinition";

    /**
     * The NameQuery to see if a family already exists with a given name.
     */
    private static final String IS_EXISTING_FAMILY = "isExistingFamily";

    /**
     * The NamedQuery to find all active {@link FamilyImpl} instances.
     */
    private static final String SELECT_ACTIVE_FAMILIES = "activeFamilies";

    /**
     * The NamedQuery to find a {@link ExitImpl}.
     */
    private static final String SELECT_EXIT = "getExit";

    /**
     * The NamedQuery to find a {@link ProcessDefinitionImpl}.
     */
    private static final String SELECT_PROCESS_DEFINITION_BY_FAMILY_NAME = "getProcessDefinitionByFamilyName";

    /**
     * The NamedQuery to find a {@link PairingImpl}.
     */
    private static final String SELECT_PAIRING = "getPairing";

    /**
     * The NamedQuery to find a {@link StateImpl}.
     */
    private static final String SELECT_STATE = "getState";

    // private static member data

    // private instance member data

    /**
     * The value of the process definition parameter if set.
     */
    private ProcessDefinitionImpl definition;

    /**
     * The {@link MockEntityManager} instance used by this object.
     */
    private MockEntityManager entityManager;

    /**
     * The value of the family parameter if set.
     */
    private FamilyImpl family;

    /**
     * The NamedQuery with which this object was created.
     */
    private String namedQuery;

    /**
     * The value of the String parameters if set.
     */
    private List<String> strings = new ArrayList<String>();

    /**
     * The value of the State parameter if set.
     */
    private StateImpl state;

    // constructors

    /**
     * Constructs an instance of this class.
     * 
     * @param query
     *            the NamedQuery defining this query.
     */
    MockTypedQuery(String query,
                   MockEntityManager manager) {
        entityManager = manager;
        namedQuery = query;
    }

    // instance member method (alphabetic)

    @Override
    public int executeUpdate() {

        return 0;
    }

    @Override
    public int getFirstResult() {

        return 0;
    }

    @Override
    public FlushModeType getFlushMode() {

        return null;
    }

    @Override
    public Map<String, Object> getHints() {

        return null;
    }

    @Override
    public LockModeType getLockMode() {

        return null;
    }

    @Override
    public int getMaxResults() {

        return 0;
    }

    @Override
    public Parameter<?> getParameter(int position) {

        return null;
    }

    @Override
    public <T> Parameter<T> getParameter(int position,
                                         Class<T> type) {

        return null;
    }

    @Override
    public Parameter<?> getParameter(String name) {

        return null;
    }

    @Override
    public <T> Parameter<T> getParameter(String name,
                                         Class<T> type) {

        return null;
    }

    @Override
    public Set<Parameter<?>> getParameters() {

        return null;
    }

    @Override
    public Object getParameterValue(int position) {

        return null;
    }

    @Override
    public <T> T getParameterValue(Parameter<T> param) {

        return null;
    }

    @Override
    public Object getParameterValue(String name) {

        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<X> getResultList() {
        return (List<X>) resolveResultsList(namedQuery);
    }

    @SuppressWarnings("unchecked")
    @Override
    public X getSingleResult() {
        return (X) resolveSingleResult(namedQuery,
                                       definition,
                                       family,
                                       state,
                                       strings);
    }

    @Override
    public boolean isBound(Parameter<?> param) {

        return false;
    }

    /**
     * Resolves {@link #getResultList()} is an untyped manner.
     * 
     * @param query
     *            the NamedQuery to resolve.
     * 
     * @return the {@link Object} that is the resolution of this query.
     */
    private Object resolveResultsList(String query) {
        if (SELECT_ACTIVE_FAMILIES.equals(query)) {
            final List<FamilyImpl> result = new ArrayList<FamilyImpl>();
            for (FamilyImpl entity : entityManager.FAMILIES) {
                if (null != entity && entity.isActive()) {
                    result.add(entity);
                }
            }
            return result;
        }
        return null;
    }

    /**
     * Resolves {@link #getSingleResult()} is an untyped manner.
     * 
     * @param query
     *            the NamedQuery with which this object was created.
     * @param definitionToSet
     *            the value of the process definition parameter if set.
     * @param familyToSet
     *            TODO
     * @param stateToSet
     *            the value of the State parameter to set, if any.
     * @param stringsToSet
     *            the value of the String parameters to set, if any.
     * 
     * @return the {@link Object} that is the resolution of this query.
     */
    private Object resolveSingleResult(String query,
                                       ProcessDefinitionImpl definitionToSet,
                                       FamilyImpl familyToSet,
                                       StateImpl stateToSet,
                                       List<String> stringsToSet) {
        if (IS_EXISTING_DEFINITION.equals(query)) {
            final String versionName = stringsToSet.get(0);
            for (ProcessDefinitionImpl entry : entityManager.PROCESS_DEFINITIONS) {
                if (versionName.equals(entry.getName()) && familyToSet.equals((entry.getFamily()))) {
                    return Boolean.TRUE;
                }
            }
            return Boolean.FALSE;
        }

        if (IS_EXISTING_FAMILY.equals(query)) {
            final String familyName = stringsToSet.get(0);
            for (FamilyImpl entity : entityManager.FAMILIES) {
                if (null != entity && familyName.equals(entity.getName())) {
                    return Boolean.TRUE;
                }
            }
            return Boolean.FALSE;
        }

        if (SELECT_EXIT.equals(query)) {
            for (ExitImpl entry : entityManager.EXITS) {
                if ((stringsToSet.get(0)).equals(entry.getName()) && (stateToSet == entry.getOrigin())) {
                    return entry;
                }
            }
            throw new NoResultException();
        }
        if (SELECT_PROCESS_DEFINITION_BY_FAMILY_NAME.equals(query)) {
            final String familyName = stringsToSet.get(0);
            final String versionName = stringsToSet.get(1);
            for (ProcessDefinitionImpl entry : entityManager.PROCESS_DEFINITIONS) {
                if (versionName.equals(entry.getName()) && (familyName.equals((entry.getFamily()
                                                                                    .getName())))) {
                    return entry;
                }
            }
            throw new NoResultException();
        }
        if (SELECT_PAIRING.equals(query)) {
            for (PairingImpl entry : entityManager.PAIRINGS) {
                if (stringsToSet.get(0)
                                .equals(entry.getItem())
                    && definitionToSet == entry.getProcessDefinition()) {
                    return entry;
                }
            }
            throw new NoResultException();
        }
        if (SELECT_STATE.equals(query)) {
            for (StateImpl entry : entityManager.STATES) {
                if (stringsToSet.get(0)
                                .equals(entry.getName())) {
                    return entry;
                }
            }
            throw new NoResultException();
        }
        throw new NoResultException();
    }

    @Override
    public TypedQuery<X> setFirstResult(int startPosition) {

        return null;
    }

    @Override
    public TypedQuery<X> setFlushMode(FlushModeType flushMode) {

        return null;
    }

    @Override
    public TypedQuery<X> setHint(String hintName,
                                 Object value) {

        return null;
    }

    @Override
    public TypedQuery<X> setLockMode(LockModeType lockMode) {

        return null;
    }

    @Override
    public TypedQuery<X> setMaxResults(int maxResult) {

        return null;
    }

    @Override
    public TypedQuery<X> setParameter(int position,
                                      Calendar value,
                                      TemporalType temporalType) {

        return null;
    }

    @Override
    public TypedQuery<X> setParameter(int position,
                                      Date value,
                                      TemporalType temporalType) {

        return null;
    }

    @Override
    public TypedQuery<X> setParameter(int position,
                                      Object value) {

        return null;
    }

    @Override
    public TypedQuery<X> setParameter(Parameter<Calendar> param,
                                      Calendar value,
                                      TemporalType temporalType) {

        return null;
    }

    @Override
    public TypedQuery<X> setParameter(Parameter<Date> param,
                                      Date value,
                                      TemporalType temporalType) {

        return null;
    }

    @Override
    public <T> TypedQuery<X> setParameter(Parameter<T> param,
                                          T value) {

        return null;
    }

    @Override
    public TypedQuery<X> setParameter(String name,
                                      Calendar value,
                                      TemporalType temporalType) {

        return null;
    }

    @Override
    public TypedQuery<X> setParameter(String name,
                                      Date value,
                                      TemporalType temporalType) {

        return null;
    }

    @Override
    public TypedQuery<X> setParameter(String parameterName,
                                      Object value) {
        if ("definition".equals(parameterName)) {
            definition = (ProcessDefinitionImpl) value;
        } else if ("family".equals(parameterName)) {
            family = (FamilyImpl) value;
        } else if ("familyName".equals(parameterName)) {
            strings.add((String) value);
        } else if ("item".equals(parameterName)) {
            strings.add((String) value);
        } else if ("name".equals(parameterName)) {
            strings.add((String) value);
        } else if ("origin".equals(parameterName)) {
            state = (StateImpl) value;
        }
        return this;
    }

    @Override
    public <T> T unwrap(Class<T> cls) {

        return null;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
