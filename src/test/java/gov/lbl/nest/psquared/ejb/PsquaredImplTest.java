package gov.lbl.nest.psquared.ejb;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

import gov.lbl.nest.psquared.AbstractPsquaredTest;
import gov.lbl.nest.psquared.StateMachine;

/**
 * This class test that a {@link PSquaredImpl} implementation fulfills its
 * requirements.
 * 
 * @author patton
 */
@DisplayName("PSquaredImpl class")
public class PsquaredImplTest extends
                              AbstractPsquaredTest {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The instance being tested.
     */
    private PSquaredImpl testObject;

    // constructors

    // instance member method (alphabetic)

    /**
     * @throws Exception
     *             when there is a problem.
     */
    @BeforeEach
    protected void setUp() throws Exception {
        final MockEntityManager mockEntityManager = new MockEntityManager(getMockScheduler());
        final StateMachine stateMachine = new StateMachine();
        testObject = new PSquaredImpl(mockEntityManager,
                                      stateMachine,
                                      new ExitManager(mockEntityManager));
        setTestObject(testObject);
        // Make sure test objects are loaded.
        testObject.getFamilies();
    }

    @Override
    protected void tearDown() throws Exception {
        // Does nothing.
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}
}
