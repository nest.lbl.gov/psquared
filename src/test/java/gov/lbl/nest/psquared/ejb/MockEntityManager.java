package gov.lbl.nest.psquared.ejb;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import gov.lbl.nest.psquared.MockScheduler;
import jakarta.persistence.EntityGraph;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.FlushModeType;
import jakarta.persistence.LockModeType;
import jakarta.persistence.Query;
import jakarta.persistence.StoredProcedureQuery;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaDelete;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.CriteriaUpdate;
import jakarta.persistence.metamodel.Metamodel;

/**
 * The {@link EntityManager} implementation used in unit tests.
 * 
 * @author patton
 */
public class MockEntityManager implements
                               EntityManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // instance member data

    /**
     * The array holding persistent {@link ExitImpl} instances.
     */
    final List<ExitImpl> EXITS = new ArrayList<ExitImpl>();

    /**
     * The array holding persistent {@link FamilyImpl} instances.
     */
    final List<FamilyImpl> FAMILIES = new ArrayList<FamilyImpl>();

    /**
     * The array holding persistent {@link ProcessDefinitionImpl} instances.
     */
    final List<ProcessDefinitionImpl> PROCESS_DEFINITIONS = new ArrayList<ProcessDefinitionImpl>();

    /**
     * The array holding persistent {@link PairingImpl} instances.
     */
    final List<PairingImpl> PAIRINGS = new ArrayList<PairingImpl>();

    /**
     * The array holding persistent {@link StateImpl} instances.
     */
    final List<StateImpl> STATES = new ArrayList<StateImpl>();

    /**
     * The array holding persistent {@link SchedulerImpl} instances.
     */
    final List<SchedulerImpl> SCHEDULERS = new ArrayList<SchedulerImpl>();

    /**
     * The array holding persistent {@link TransitionImpl} instances.
     */
    final List<TransitionImpl> TRANSITIONS = new ArrayList<TransitionImpl>();

    /**
     * The {@link MockScheduler} instance to use.
     */
    final private MockScheduler mockScheduler;

    // constructors

    MockEntityManager(MockScheduler scheduler) {
        mockScheduler = scheduler;
    }

    // instance member method (alphabetic)

    @Override
    public void clear() {
        // currently does nothing.
    }

    @Override
    public void close() {
        // currently does nothing.
    }

    @Override
    public boolean contains(Object entity) {
        return false;
    }

    @Override
    public <T> EntityGraph<T> createEntityGraph(Class<T> arg0) {
        return null;
    }

    @Override
    public EntityGraph<?> createEntityGraph(String arg0) {
        return null;
    }

    @Override
    public Query createNamedQuery(String name) {
        return null;
    }

    @Override
    public <T> TypedQuery<T> createNamedQuery(String name,
                                              Class<T> resultClass) {
        return new MockTypedQuery<T>(name,
                                     this);
    }

    @Override
    public StoredProcedureQuery createNamedStoredProcedureQuery(String arg0) {
        return null;
    }

    @Override
    public Query createNativeQuery(String sqlString) {
        return null;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Query createNativeQuery(String sqlString,
                                   Class resultClass) {
        return null;
    }

    @Override
    public Query createNativeQuery(String sqlString,
                                   String resultSetMapping) {
        return null;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Query createQuery(CriteriaDelete arg0) {
        return null;
    }

    @Override
    public <T> TypedQuery<T> createQuery(CriteriaQuery<T> criteriaQuery) {
        return null;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Query createQuery(CriteriaUpdate arg0) {
        return null;
    }

    @Override
    public Query createQuery(String qlString) {
        return null;
    }

    @Override
    public <T> TypedQuery<T> createQuery(String qlString,
                                         Class<T> resultClass) {
        return null;
    }

    @Override
    public StoredProcedureQuery createStoredProcedureQuery(String arg0) {
        return null;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public StoredProcedureQuery createStoredProcedureQuery(String arg0,
                                                           Class... arg1) {
        return null;
    }

    @Override
    public StoredProcedureQuery createStoredProcedureQuery(String arg0,
                                                           String... arg1) {
        return null;
    }

    @Override
    public void detach(Object entity) {
        // currently does nothing.
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T find(Class<T> entityClass,
                      Object primaryKey) {
        if (entityClass == TransitionImpl.class) {
            final int key = (Integer) primaryKey;
            for (TransitionImpl transition : TRANSITIONS) {
                if (key == transition.getTransitionKey()) {
                    return (T) transition;
                }
            }
        }
        return null;
    }

    @Override
    public <T> T find(Class<T> entityClass,
                      Object primaryKey,
                      LockModeType lockMode) {
        return null;
    }

    @Override
    public <T> T find(Class<T> entityClass,
                      Object primaryKey,
                      LockModeType lockMode,
                      Map<String, Object> properties) {
        return null;
    }

    @Override
    public <T> T find(Class<T> entityClass,
                      Object primaryKey,
                      Map<String, Object> properties) {
        return null;
    }

    @Override
    public void flush() {
        // currently does nothing.
    }

    @Override
    public CriteriaBuilder getCriteriaBuilder() {
        return null;
    }

    @Override
    public Object getDelegate() {
        return null;
    }

    @Override
    public EntityGraph<?> getEntityGraph(String arg0) {
        return null;
    }

    @Override
    public <T> List<EntityGraph<? super T>> getEntityGraphs(Class<T> arg0) {
        return null;
    }

    @Override
    public EntityManagerFactory getEntityManagerFactory() {
        return null;
    }

    @Override
    public FlushModeType getFlushMode() {

        return null;
    }

    @Override
    public LockModeType getLockMode(Object entity) {
        return null;
    }

    @Override
    public Metamodel getMetamodel() {
        return null;
    }

    @Override
    public Map<String, Object> getProperties() {
        return null;
    }

    @Override
    public <T> T getReference(Class<T> entityClass,
                              Object primaryKey) {
        return null;
    }

    @Override
    public EntityTransaction getTransaction() {
        return null;
    }

    @Override
    public boolean isJoinedToTransaction() {
        return false;
    }

    @Override
    public boolean isOpen() {
        return false;
    }

    @Override
    public void joinTransaction() {
        // currently does nothing.
    }

    @Override
    public void lock(Object entity,
                     LockModeType lockMode) {
        // currently does nothing.
    }

    @Override
    public void lock(Object entity,
                     LockModeType lockMode,
                     Map<String, Object> properties) {
        // currently does nothing.
    }

    @Override
    public <T> T merge(T entity) {
        return entity;
    }

    @Override
    public void persist(Object entity) {
        if (entity instanceof ExitImpl) {
            final int key = EXITS.size();
            final ExitImpl exit = (ExitImpl) entity;
            exit.setExitKey(key);
            EXITS.add(exit);
            return;
        }
        if (entity instanceof FamilyImpl) {
            final int key = FAMILIES.size();
            final FamilyImpl family = (FamilyImpl) entity;
            family.setFamilyKey(key);
            FAMILIES.add(family);
            return;
        }
        if (entity instanceof PairingImpl) {
            final int key = PAIRINGS.size();
            final PairingImpl pairing = (PairingImpl) entity;
            pairing.setPairingKey(key);
            PAIRINGS.add(pairing);
            return;
        }
        if (entity instanceof ProcessDefinitionImpl) {
            final int key = PROCESS_DEFINITIONS.size();
            final ProcessDefinitionImpl processDefinition = (ProcessDefinitionImpl) entity;
            processDefinition.setProcessDefinitionKey(key);
            final SchedulerImpl schedulerImpl = processDefinition.getDefaultScheduler();
            if (mockScheduler.getSchedulerKey() != schedulerImpl.getSchedulerKey()) {
                throw new IllegalStateException("A Scheduler is not the MockScheduler");
            }
            processDefinition.setDefaultScheduler(mockScheduler);
            PROCESS_DEFINITIONS.add(processDefinition);
            return;
        }
        if (entity instanceof StateImpl) {
            final int key = STATES.size();
            final StateImpl state = (StateImpl) entity;
            state.setStateKey(key);
            STATES.add(state);
            return;
        }
        if (entity instanceof SchedulerImpl) {
            final int key = SCHEDULERS.size();
            final SchedulerImpl scheduler = (SchedulerImpl) entity;
            if (0 == key) {
                scheduler.setSchedulerKey(key);
                mockScheduler.setDescription(scheduler.getDescription());
                mockScheduler.setName(scheduler.getName());
                mockScheduler.setSchedulerKey(scheduler.getSchedulerKey());
                SCHEDULERS.add(mockScheduler);
            }
            return;
        }
        if (entity instanceof TransitionImpl) {
            final int key = TRANSITIONS.size();
            final TransitionImpl transition = (TransitionImpl) entity;
            transition.setTransitionKey(key);
            TRANSITIONS.add(transition);
            return;
        }
    }

    @Override
    public void refresh(Object entity) {
        // currently does nothing.
    }

    @Override
    public void refresh(Object entity,
                        LockModeType lockMode) {
        // currently does nothing.
    }

    @Override
    public void refresh(Object entity,
                        LockModeType lockMode,
                        Map<String, Object> properties) {
        // currently does nothing.
    }

    @Override
    public void refresh(Object entity,
                        Map<String, Object> properties) {
        // currently does nothing.
    }

    @Override
    public void remove(Object entity) {
        // currently does nothing.
    }

    @Override
    public void setFlushMode(FlushModeType flushMode) {
        // currently does nothing.
    }

    @Override
    public void setProperty(String propertyName,
                            Object value) {
        // currently does nothing.
    }

    @Override
    public <T> T unwrap(Class<T> cls) {
        return null;
    }

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
