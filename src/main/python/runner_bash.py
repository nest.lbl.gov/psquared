#!/usr/bin/env python

from runner_utils import completeStateMachine, PSquaredFailure

import socket

if __name__ == '__main__':
    import sys
    from optparse import OptionParser, OptionGroup, IndentedHelpFormatter
    parser = OptionParser(usage = "pp_python_runner [options] submission_uri cmd [param ...]",
                          version = "%prog 1.0")
    parser.allow_interspersed_args = False
    parser.add_option('-?',
                      help = 'Prints out this help',
                      action = 'help')
    parser.add_option('-s',
                      '--success_cmd',
                      dest = 'SUCCESS_CMD',
                      help = 'The command to run after a successful processing',
                      default = None)
    parser.add_option('-f',
                      '--failure_cmd',
                      dest = 'FAILURE_CMD',
                      help = 'The command to run after a failed processing',
                      default = None)

    (options, args) = parser.parse_args()

    try:
        messageForExecute = 'Executing on ' + socket.gethostname()
        if 1 > len(args):
            print >> sys.stderr, 'must supply submission_uri'
            sys.exit(-1)
        if 3 > len(args):
            arguments = None
        else:
            arguments = args[2:]
        if 2 > len(args):
            cmd = None
        else:
            cmd = args[1]
        completeStateMachine(messageForExecute,
                             None,
                             None,
                             options.SUCCESS_CMD,
                             options.FAILURE_CMD,
                             args[0],
                             cmd,
                             arguments)
    except PSquaredFailure as e:
        print >> sys.stderr, e.message
        sys.exit(e.code)
