#!/usr/bin/env python

# This functions as the processor for the tutorial_xml.py file.

# Prepare environment
import sys
pyxml=None
index = 0
for p in sys.path:
    if -1 != p.find('pyxml'):
         pyxml = p
    index += 1

if None != pyxml:
    sys.path.remove(pyxml)

import requests
import xml.etree.ElementTree as ET
HEADERS = {'content-type': 'application/xml'}

# Simple pretty print for *IX
import os
def pretty_print(s):
    os.system("echo '" + str(s) + "' | xmllint -format -")

def process(uri):
    r = requests.get(uri)
    state = ET.fromstring(r.text)
    e=state.findall('exits/exit')
    for exit in e:
        if 'executing' == exit.find('name').text:
            executingUri = exit.find('uri').text
            break
    
    # Prepare attachment document
    import socket
    message = ET.Element('message')
    message.text = 'Executing on ' + socket.gethostname()
    attachment = ET.Element('attachment')
    attachment.append(message)
    pretty_print(ET.tostring(attachment))
    
    # Move into executing state
    r = requests.post(executingUri, data=ET.tostring(attachment), headers=HEADERS)
    state = ET.fromstring(r.text)
    pretty_print(ET.tostring(state))
    e=state.findall('exits/exit')
    for exit in e:
        if 'processed' == exit.find('name').text:
            processedUri = exit.find('uri').text
            break
    
    # Prepare attachment document
    attachment = ET.Element('attachment')
    
    # Complete processing
    r = requests.post(processedUri, data=ET.tostring(attachment), headers=HEADERS)
    state = ET.fromstring(r.text)
    pretty_print(ET.tostring(state))


if __name__ == '__main__':
    import sys
    if 1 == len(sys.argv):
        print 'URI was not provided'
        exit(1)
    if len(sys.argv) > 2:
        print 'Only the URI should be provided'
        exit(1)
    process(sys.argv[1])
