#!/usr/bin/env python

# To suppress SSL warnings to a https host
#import requests.packages.urllib3
#requests.packages.urllib3.disable_warnings()

# optional selection of scheduler
# Uncomment one, and only one, of the following lines to override the default scheduler.
#schedulerName = 'DoNothing'
#schedulerName = 'Internal'
#schedulerName = 'Background'
schedulerName = 'RabbitMQ'


# Prepare environment
import sys
pyxml=None
index = 0
for p in sys.path:
    if -1 != p.find('pyxml'):
         pyxml = p
    index += 1

if None != pyxml:
    sys.path.remove(pyxml)

import requests
import xml.etree.ElementTree as ET
HEADERS = {'content-type': 'application/xml'}

# Simple pretty print for *IX
import os
def pretty_print(s):
    os.system("echo '" + str(s) + "' | xmllint -format -")

# Read Application
r = requests.get('https://dayabay.lbl.gov:3238/psquared/tst/report/', verify=False)
print r.text
application = ET.fromstring(r.text)
pretty_print(ET.tostring(application))

# Read 'Test' Configuration's versions
c=application.findall('configurations/configuration')
for config in c:
    if 'Test' == config.find('name').text:
        configurationUri = config.find('uri').text
        break

r = requests.get(configurationUri, verify=False)
configuration = ET.fromstring(r.text)
pretty_print(ET.tostring(configuration))

# Prepare submission document for the 'ItemToTest' item
item = ET.Element('item')
item.text = 'ItemToTest'
items = ET.Element('items')
items.append(item)
submission = ET.Element('submission')
submission.append(items)
message = ET.Element('message')
message.text = 'Test Submission'
submission.append(message)
s=application.findall('schedulers/scheduler')
for scheduler in s:
    if schedulerName == scheduler.find('name').text:
        schedulerUri = scheduler.find('uri').text
        break

scheduler = ET.Element('scheduler')
scheduler.text = schedulerUri
submission.append(scheduler)
# end optional selection of scheduler
pretty_print(ET.tostring(submission))

# Find the submit URI for the '-1' version of the 'Test' configuration and use it
v=configuration.findall('known-versions/known-version')
for vers in v:
    if '-1' == vers.find('name').text:
        submitUri = vers.find('submit').text
        break

r = requests.post(submitUri, data=ET.tostring(submission), headers=HEADERS, verify=False)
current_states = ET.fromstring(r.text)
pretty_print(ET.tostring(current_states))

# Find the realized-state URI for the 'ItemToTest' item.
s=current_states.findall('realized-state')
for state in s:
    if 'ItemToTest' == state.find('item').text:
        realizedStateUri = state.find('uri').text
        break

# Run processor in separate thread
if 'Internal' != schedulerName:
    print ('Run external command such as "' +
           '${PP_PROJECT}/src/main/python/tutorial_processor_xml.py ' +
           realizedStateUri + '" in another session')

# Wait for processor to finish with item
import time
while True:
    r = requests.get(realizedStateUri, verify=False)
    state = ET.fromstring(r.text)
    s = state.find('entry/state')
    if 'SUBMITTED' != s.text and 'EXECUTING' != s.text:
        break
    e=state.find('exited')
    if None == e:
        time.sleep(1)
    else:
        realizedStateUri = e.find('uri').text

pretty_print(ET.tostring(state))

# Reset 'ItemToTest' with 'Test:-1' version so tutorial can be run again
e=state.findall('exits/exit')
for exit in e:
    if 'reset' == exit.find('name').text:
        resetUri = exit.find('uri').text
        break

message = ET.Element('message')
message.text = 'Reset "ItemToTest" with "Test:-1" version so tutorial can be run again'
attachment = ET.Element('attachment')
attachment.append(message)
pretty_print(ET.tostring(attachment))
r = requests.post(resetUri, data=ET.tostring(attachment), headers=HEADERS, verify=False)
state = ET.fromstring(r.text)
pretty_print(ET.tostring(state))
