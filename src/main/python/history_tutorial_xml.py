# Prepare environment
import sys
pyxml=None
index = 0
for p in sys.path:
    if -1 != p.find('pyxml'):
         pyxml = p
    index += 1

if None != pyxml:
    sys.path.remove(pyxml)

import requests
import xml.etree.ElementTree as ET
HEADERS = {'content-type': 'application/xml'}
PADDING = '                                        '

# Simple pretty print for *IX
import os
def pretty_print(s):
    os.system("echo '" + str(s) + "' | xmllint -format -")

# Read Application
r = requests.get('http://localhost:8080/psquared/local/report/')
application = ET.fromstring(r.text)
pretty_print(ET.tostring(application))

# Read 'Test' Configuration
c=application.findall('configurations/configuration')
for config in c:
    if 'Test' == config.find('name').text:
        configurationUri = config.find('uri').text
        break

r = requests.get(configurationUri)
configuration = ET.fromstring(r.text)
pretty_print(ET.tostring(configuration))

# Prepare selection document
item = ET.Element('item')
item.text = 'ItemToTest'
items = ET.Element('items')
items.append(item)
selection = ET.Element('selection')
selection.append(items)
pretty_print(ET.tostring(selection))

# Find the histories URI for the '-1' version of the 'Test' configuration and use it
v=configuration.findall('known-versions/known-version')
for vers in v:
    if '-1' == vers.find('name').text:
        historiesUri = vers.find('histories').text
        break

r = requests.get(historiesUri, data=ET.tostring(selection), headers=HEADERS)
histories = ET.fromstring(r.text)
entries=histories.findall('**/entry')
entries.reverse()
for entry in entries:
    header = entry.find('completed').text + ' ' + entry.find('state').text
    message = entry.find('message')
    if None == message or None == message.text:
        messageToUse = ''
    else:
        messageToUse = message.text.replace('\n', '\n' + PADDING)
    print header +  PADDING[len(header):] + messageToUse

