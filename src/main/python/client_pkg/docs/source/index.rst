.. psquared_client documentation master file, created by
   sphinx-quickstart on Wed Aug 21 15:14:15 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to psquared_client's documentation!
===========================================

Overview
========

The ``psqaured_client`` project provides client access to a **PSqaured** server.

It is made up of three components:

*   The ``pp-cli`` executable that provides command line access to request processing and receive reports from the **PSqaured** server.

*   The ``PSqaured`` python class that provides a python interface to request processing and receive reports from the **PSqaured** server.

*   The ``Display`` python module that can display to a terminal reports returned from the **PSqaured** server by the ``PSqaured`` class.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   PSquared
   Display

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
