from .PSquared import PSquared, FatalError

import pp_cli
def main():
    """Entry point for the application script"""
    pp_cli.main()
