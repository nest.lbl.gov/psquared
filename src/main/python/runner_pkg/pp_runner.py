def main():
    import argparse
    import sys
    import socket
    import logging

    parser = argparse.ArgumentParser(description='Executes a command with the PSquared State machine.')
    parser.add_argument('-d',
                      '--debug',
                      dest='DEBUG',
                      help='print out detail information to stdout.',
                      action='store_true',
                      default=False)
    parser.add_argument('-e',
                        dest='ERROR',
                        help='The file, as opposed to stdout, into which to write errors from the command')
    parser.add_argument('--log_file',
                        dest='LOG_FILE',
                        help='The file, as opposed to stdout, into which to write log messages from the state machine')
    parser.add_argument('-o',
                        dest='OUTPUT',
                        help='The file, as opposed to stdout, into which to write output from the command')
    parser.add_argument('-s',
                      '--success_cmd',
                      dest = 'SUCCESS_CMD',
                      help = 'The command to run after a successful processing',
                      default = None)
    parser.add_argument('-f',
                      '--failure_cmd',
                      dest = 'FAILURE_CMD',
                      help = 'The command to run after a failed processing',
                      default = None)
    parser.add_argument('-r',
                      '--resubmission',
                      dest='RESUBMISSION',
                      help='allows automatic re-submission to take place.',
                      action='store_true',
                      default=False)
    parser.add_argument('submission_url',
                        help='The submission URL used to create the processing request to be handled')
    parser.add_argument('cmd',
                        help='The command to be executed')
    parser.add_argument('args',
                        nargs=argparse.REMAINDER,
                        help='An argument to the command to be executed')
    options = parser.parse_args()

    if options.DEBUG:
        log_level = logging.INFO
        DEBUG = True
    else:
        log_level = logging.WARNING
    if options.LOG_FILE:
        logging.basicConfig(filename=options.LOG_FILE,
                            level=log_level)
    else:
        logging.basicConfig(Stream=sys.stdout,
                            level=log_level)

    print 'Beginning state machine execution'
    execute('Executing on ' + socket.gethostname() + ' using pp_engine.py',
            options.cmd,
            options.args,
            options.submission_url,
            options.OUTPUT,
            options.ERROR,
            options.SUCCESS_CMD,
            options.FAILURE_CMD,
            options.RESUBMISSION)
    print 'Completed state machine execution'
