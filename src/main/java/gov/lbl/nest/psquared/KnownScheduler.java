package gov.lbl.nest.psquared;

import java.net.URI;

/**
 * This interface defines how a item/process pair can be submitted for
 * processing.
 * 
 * @author patton
 */
public interface KnownScheduler {

    /**
     * Returns the description of this family.
     * 
     * @return the description of this family.
     */
    String getDescription();

    /**
     * Returns the identity of this object.
     * 
     * @return the identity of this object.
     */
    String getIdentity();

    /**
     * Returns the name of this object within the application.
     * 
     * @return the name of this object within the application.
     */
    String getName();

    /**
     * <code>true</code> when a call to
     * {@link #submit(String, ProcessDefinition, URI, PSquared)} is required when
     * re-submission of a jobs is happening automatically. If <code>false</code>
     * then the re-submission is done outside of this application.
     * 
     * @return <code>true</code> when a call to
     *         {@link #submit(String, ProcessDefinition, URI, PSquared)} is required
     */
    boolean getResubmit();

    /**
     * Submits the specified item/process pairing for processing, returning a
     * string, if any, that summarizes the result of submission.
     * 
     * @param item
     *            the identity of the item in this pairing.
     * @param process
     *            the name of the process definition associated in this pairing.
     * @param submissionUri
     *            the URI of the submmission's realized state.
     * @param psquared
     *            the {@link PSquared} instance that will receive any transition
     *            call-backs.
     * 
     * @return The String, if any, that summarizes the result of submission.
     * 
     * @throws AlreadyExitedException
     *             when the original specified transition that cause the submit is
     *             no long the current state.
     * @throws ForbiddenTransitionException
     *             when the {@link KnownScheduler} tries to transition into a state
     *             that is not allowed.
     * @throws SubmitException
     *             when the method needs a transition to occur following the submit
     *             transition that caused the invocation.
     */
    String submit(String item,
                  ProcessDefinition process,
                  URI submissionUri,
                  PSquared psquared) throws AlreadyExitedException,
                                     ForbiddenTransitionException,
                                     SubmitException;
}
