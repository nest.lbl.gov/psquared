package gov.lbl.nest.psquared.ejb;

import java.util.Date;

import gov.lbl.nest.psquared.ProcessDefinition;
import gov.lbl.nest.psquared.State;
import gov.lbl.nest.psquared.Transition;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;
import jakarta.persistence.SecondaryTable;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.persistence.Transient;

/**
 * This class implements the {@link Transition} interface using JPA.
 *
 * @author patton
 */
@Entity
@Table(name = "Transition2")
@SecondaryTable(name = "TransitionMessage2")
/**
 * Queries are order by `whenOccurred` *and* `transistionKey` in order to
 * guarantee ordering when, in the very rare case, the `whenOccured`values
 * match.
 */
@NamedQueries({ @NamedQuery(name = "latestTransitions",
                            query = "SELECT t" + " FROM TransitionImpl t"
                                    + " WHERE t.succeedingTransition IS NULL"
                                    + " AND t.pairingImpl.processDefinition IN ( :processes )"
                                    + " ORDER BY t.whenOccurred DESC, t.transitionKey"),
                @NamedQuery(name = "latestTransitionsExcludingStates",
                            query = "SELECT t" + " FROM TransitionImpl t"
                                    + " WHERE t.succeedingTransition IS NULL"
                                    + " AND t.exitImpl.target NOT IN ( :states )"
                                    + " AND t.pairingImpl.processDefinition = :process"
                                    + " ORDER BY t.whenOccurred ASC, t.transitionKey"),
                @NamedQuery(name = "latestTransitionsRequiredState",
                            query = "SELECT t" + " FROM TransitionImpl t"
                                    + " WHERE t.succeedingTransition IS NULL"
                                    + " AND t.exitImpl.target = :state"
                                    + " AND t.pairingImpl.processDefinition = :process"
                                    + " ORDER BY t.whenOccurred ASC, t.transitionKey"),
                @NamedQuery(name = "latestTransitionedExcludingStatesSince",
                            query = "SELECT t" + " FROM TransitionImpl t"
                                    + " WHERE t.whenOccurred >= :since"
                                    + " AND t.succeedingTransition IS NULL"
                                    + " AND t.exitImpl.target NOT IN ( :states )"
                                    + " AND t.pairingImpl.processDefinition = :process"
                                    + " ORDER BY t.whenOccurred ASC, t.transitionKey"),
                @NamedQuery(name = "latestTransitionedExcludingStatesBetween",
                            query = "SELECT t" + " FROM TransitionImpl t"
                                    + " WHERE t.whenOccurred >= :since"
                                    + " AND whenOccurred < :before"
                                    + " AND t.succeedingTransition IS NULL"
                                    + " AND t.exitImpl.target NOT IN ( :states )"
                                    + " AND t.pairingImpl.processDefinition = :process"
                                    + " ORDER BY t.whenOccurred ASC, t.transitionKey"),
                @NamedQuery(name = "selectedLatestTransitions",
                            query = "SELECT t" + " FROM TransitionImpl t"
                                    + " WHERE t.succeedingTransition IS NULL"
                                    + " AND t.pairingImpl.processDefinition = :process"
                                    + " AND t.pairingImpl.item IN ( :items )"
                                    + " ORDER BY t.whenOccurred ASC, t.transitionKey"),
                @NamedQuery(name = "selectedLatestTransitionsExcludingStates",
                            query = "SELECT t" + " FROM TransitionImpl t"
                                    + " WHERE t.succeedingTransition IS NULL"
                                    + " AND t.exitImpl.target NOT IN ( :states )"
                                    + " AND t.pairingImpl.processDefinition = :process"
                                    + " AND t.pairingImpl.item IN ( :items )"
                                    + " ORDER BY t.whenOccurred ASC, t.transitionKey"),
                @NamedQuery(name = "selectedLatestTransitionsRequiredState",
                            query = "SELECT t" + " FROM TransitionImpl t"
                                    + " WHERE t.succeedingTransition IS NULL"
                                    + " AND t.exitImpl.target = :state"
                                    + " AND t.pairingImpl.processDefinition = :process"
                                    + " AND t.pairingImpl.item IN ( :items )"
                                    + " ORDER BY t.whenOccurred ASC, t.transitionKey"),
                @NamedQuery(name = "transitionedSince",
                            query = "SELECT t" + " FROM TransitionImpl t"
                                    + " WHERE t.whenOccurred >= :since"
                                    + " AND t.succeedingTransition IS NULL"
                                    + " AND t.exitImpl.target IN :states"
                                    + " AND t.pairingImpl.processDefinition.familyImpl = :family"
                                    + " ORDER BY t.whenOccurred ASC, t.transitionKey"),
                @NamedQuery(name = "transitionedBetween",
                            query = "SELECT t" + " FROM TransitionImpl t"
                                    + " WHERE t.whenOccurred >= :since"
                                    + " AND t.whenOccurred < :before"
                                    + " AND t.succeedingTransition IS NULL"
                                    + " AND t.exitImpl.target IN :states"
                                    + " AND t.pairingImpl.processDefinition.familyImpl = :family"
                                    + " ORDER BY t.whenOccurred ASC, t.transitionKey"),
                @NamedQuery(name = "getLatestTransitionsByTime",
                            query = "SELECT t" + " FROM TransitionImpl t"
                                    + " WHERE t.whenOccurred = :when"
                                    + " AND t.succeedingTransition IS NULL"
                                    + " AND t.pairingImpl.processDefinition IN ( :processes )"),
                @NamedQuery(name = "whenLatestTransition",
                            query = "SELECT MAX(t.whenOccurred)" + " FROM TransitionImpl t"
                                    + " WHERE t.succeedingTransition IS NULL"
                                    + " AND t.pairingImpl.processDefinition IN ( :processes )") })
public class TransitionImpl implements
                            Transition {

    // public static final member data

    /**
     * The maximum number of character that should be stored in a message.
     */
    public static final int MESSAGE_LIMIT = 1024;

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The exit from the originating state.
     */
    private ExitImpl exit;

    /**
     * The message, if any, associated with this transition, <code>null</code>
     * otherwise.
     */
    private String message;

    /**
     * The item/process pairing involved in this transition.
     */
    private PairingImpl pairing;

    /**
     * The preceding {@link TransitionImpl}, if any, for this item/process pairing.
     */
    private TransitionImpl precedingTransition;

    /**
     * The succeeding {@link TransitionImpl}, if any, for this item/process pairing.
     */
    private TransitionImpl succeedingTransition;

    /**
     * The key, within the {@link TransitionImpl} class, that identifies this
     * instance.
     */
    private int transitionKey;

    /**
     * The date and time when this transition occurred.
     */
    private Date whenOccurred;

    // constructors

    /**
     * Constructs an instance of this class.
     */
    protected TransitionImpl() {
    }

    /**
     * Constructs an instance of this class.
     *
     * @param pairing
     *            the item/process pairing involved in this transition.
     * @param exit
     *            the exit from the originating state.
     * @param message
     *            the message, if any, associated with this transition,
     *            <code>null</code> otherwise.
     */
    protected TransitionImpl(PairingImpl pairing,
                             ExitImpl exit,
                             String message) {
        setExitImpl(exit);
        if (null == message || "".equals(message.trim())) {
            setMessage(null);
        } else {
            final String messageToUse;
            if (message.length() > MESSAGE_LIMIT) {
                messageToUse = message.substring(0,
                                                 MESSAGE_LIMIT);
            } else {
                messageToUse = message;
            }
            setMessage(messageToUse.trim());
        }
        setPairingImpl(pairing);
        setPrecedingTransition(null);
        setWhenOccurred(new Date());
    }

    /**
     * Constructs an instance of this class.
     *
     * @param transition
     *            the transition that precedes the one being created.
     * @param exit
     *            the exit taken that caused this transition to occur.
     * @param message
     *            the message, if any, associated with this transition,
     *            <code>null</code> otherwise.
     */
    protected TransitionImpl(TransitionImpl transition,
                             ExitImpl exit,
                             String message) {
        if (null == transition) {
            throw new NullPointerException("Transition can not be null");
        }
        transition.setSucceedingTransition(this);
        setPairingImpl(transition.getPairingImpl());
        setExitImpl(exit);
        if (null == message || "".equals(message.trim())) {
            setMessage(null);
        } else {
            final String messageToUse;
            if (message.length() > MESSAGE_LIMIT) {
                messageToUse = message.substring(0,
                                                 MESSAGE_LIMIT);
            } else {
                messageToUse = message;
            }
            setMessage(messageToUse.trim());
        }
        setPrecedingTransition(transition);
        setWhenOccurred(new Date());
    }

    // instance member method (alphabetic)

    @Override
    @Transient
    public String getExit() {
        if (null == exit) {
            return null;
        }
        return exit.getName();
    }

    /**
     * Returns the exit from the originating state.
     *
     * @return the exit from the originating state.
     */
    @ManyToOne
    protected ExitImpl getExitImpl() {
        return exit;
    }

    @Override
    @Transient
    public String getIdentity() {
        return Integer.toString(getTransitionKey());
    }

    @Override
    @Transient
    public String getItem() {
        return pairing.getItem();
    }

    @Override
    @Column(table = "TransitionMessage2")
    public String getMessage() {
        return message;
    }

    /**
     * Returns the item/process pairing involved in this transition.
     *
     * @return the item/process pairing involved in this transition.
     */
    @ManyToOne(optional = false)
    protected PairingImpl getPairingImpl() {
        return pairing;
    }

    @Override
    @OneToOne(targetEntity = TransitionImpl.class)
    public Transition getPrecedingTransition() {
        return precedingTransition;
    }

    @Override
    @Transient
    public ProcessDefinition getProcessDefinition() {
        return getPairingImpl().getProcessDefinition();
    }

    @Override
    @Transient
    public State getState() {
        return State.valueOf((getStateImpl()).getName());
    }

    @Transient
    StateImpl getStateImpl() {
        return exit.getTarget();
    }

    /**
     * Returns the succeeding {@link Transition}, if any, for this item/process
     * pairing.
     *
     * @return the succeeding {@link Transition}, if any, for this item/process
     *         pairing.
     */
    @Override
    @OneToOne(targetEntity = TransitionImpl.class)
    public Transition getSucceedingTransition() {
        return succeedingTransition;
    }

    /**
     * Returns a token identifying the resulting state instance of this transition.
     *
     * @return the token identifying the resulting state instance of this
     *         transition.
     */
    @Transient
    protected int getToken() {
        return getTransitionKey();
    }

    /**
     * Returns the key, within the {@link TransitionImpl} class, that identifies
     * this instance.
     *
     * @return the key, within the {@link TransitionImpl} class, that identifies
     *         this instance.
     */
    @Id
    @GeneratedValue
    protected int getTransitionKey() {
        return transitionKey;
    }

    @Override
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    public Date getWhenOccurred() {
        return whenOccurred;
    }

    @Override
    @Transient
    public boolean isFirst() {
        return null == precedingTransition;
    }

    @Override
    @Transient
    public boolean isLast() {
        return null == succeedingTransition;
    }

    /**
     * Sets the exit from the originating state.
     *
     * @param exit
     *            the exit from the originating state.
     */
    protected void setExitImpl(ExitImpl exit) {
        this.exit = exit;
    }

    /**
     * Sets the message, if any, associated with this transition.
     *
     * @param note
     *            the message, if any, associated with this transition.
     */
    protected void setMessage(String note) {
        this.message = note;
    }

    /**
     * Sets the item/process pairing involved in this transition.
     *
     * @param pairing
     *            the item/process pairing involved in this transition.
     *
     * @throws IllegalStateException
     *             when this {@link TransitionImpl} can not be added to its
     *             associated {@link PairingImpl}.
     */
    protected void setPairingImpl(PairingImpl pairing) {
        this.pairing = pairing;
    }

    /**
     * Sets the preceding {@link TransitionImpl}, if any, for this item/process
     * pairing.
     *
     * @param transition
     *            the preceding {@link TransitionImpl}, if any, for this
     *            item/process pairing.
     */
    protected void setPrecedingTransition(TransitionImpl transition) {
        this.precedingTransition = transition;
    }

    /**
     * Sets the succeeding {@link TransitionImpl}, if any, for this item/process
     * pairing.
     *
     * @param transition
     *            the succeeding {@link TransitionImpl}, if any, for this
     *            item/process pairing.
     */
    protected void setSucceedingTransition(TransitionImpl transition) {
        succeedingTransition = transition;
    }

    /**
     * Sets the key, within the {@link TransitionImpl} class, that identifies this
     * instance.
     *
     * @param key
     *            the value of the key to set.
     */
    protected void setTransitionKey(int key) {
        transitionKey = key;
    }

    /**
     * Sets the date and time when this transition occurred.
     *
     * @param dateTime
     *            the date and time when this transition occurred.
     */
    protected void setWhenOccurred(Date dateTime) {
        whenOccurred = dateTime;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
