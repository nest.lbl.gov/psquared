package gov.lbl.nest.psquared.ejb;

import java.text.MessageFormat;

import gov.lbl.nest.psquared.Family;
import gov.lbl.nest.psquared.ProcessDefinition;
import gov.lbl.nest.psquared.scheduler.TwoPhaseDefinition;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

/**
 * This class implement the {@link ProcessDefinition} interface using JPA.
 * 
 * @author patton
 */
@Entity
@Table(name = "ProcessDefinition2")
@NamedQueries({ @NamedQuery(name = "getProcessDefinitionByFamilyName",
                            query = "SELECT d " + " FROM ProcessDefinitionImpl d"
                                    + " WHERE d.familyImpl.name = :familyName"
                                    + " AND d.name = :name"
                                    + " AND d.active = true"),
                @NamedQuery(name = "getProcessDefinitionsByFamily",
                            query = "SELECT d " + " FROM ProcessDefinitionImpl d"
                                    + " WHERE d.familyImpl = :family"
                                    + " AND d.active = true"),
                @NamedQuery(name = "getLastOrdinalOfFamily",
                            query = "SELECT d.ordinal " + " FROM ProcessDefinitionImpl d"
                                    + " WHERE d.familyImpl = :family"
                                    + " ORDER BY d.ordinal"),
                @NamedQuery(name = "isExistingDefinition",
                            query = "SELECT 0 != count(d) " + " FROM ProcessDefinitionImpl d"
                                    + " WHERE d.name = :name"
                                    + " AND d.familyImpl = :family") })
public class ProcessDefinitionImpl implements
                                   TwoPhaseDefinition {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * True when items can be processed with this definition.
     */
    private boolean active;

    /**
     * The {@link MessageFormat} string that will be command's arguments.
     */
    private String args;

    /**
     * The scheduler to to use when executing this definition and none is specified.
     */
    private SchedulerImpl defaultScheduler;

    /**
     * The description of this object within the family of definitions.
     */
    private String description;

    /**
     * True when this definition is used during process development, which it to say
     * that it is mutable.
     * 
     * Note: For compatibility with v1.x.x, this is a {@link Boolean} not a boolean.
     * This can be changed later, once v1.x.x is not longer in use.
     */
    private Boolean development;

    /**
     * The command the will be run after processing has succeeded.
     */
    private String failureCmd;

    /**
     * The family of definitions to which this instance belongs.
     */
    private FamilyImpl family;

    /**
     * The name of this object within the family of definitions.
     */
    private String name;

    /**
     * The value used to order this object within the family of definitions.
     */
    private Integer ordinal;

    /**
     * The command that will be run to process an item.
     */
    private String processCmd;

    /**
     * The key, within the {@link ProcessDefinitionImpl} class, that identifies this
     * instance.
     */
    private int processDefinitionKey;

    /**
     * The command the will be run after processing has succeeded.
     */
    private String successCmd;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected ProcessDefinitionImpl() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param name
     *            the name of this object within the family of definitions.
     * @param description
     *            this object within the family of definitions.
     * @param family
     *            the family of definitions to which this instance belongs.
     * @param ordinal
     *            the value used to order this object within the family of
     *            definitions.
     * @param processCmd
     *            the command that will be run to process an item.
     * @param successCmd
     *            the command the will be run after processing has succeeded.
     * @param failureCmd
     *            the command the will be run after processing has failed.
     * @param args
     *            the {@link MessageFormat} string that will be command's arguments.
     * @param scheduler
     *            the scheduler to to use when executing this definition and none is
     *            specified.
     * @param development
     *            True when this definition is used during process development,
     *            which it to say that it is mutable.
     */
    protected ProcessDefinitionImpl(final String name,
                                    final String description,
                                    final FamilyImpl family,
                                    final Integer ordinal,
                                    final String processCmd,
                                    final String successCmd,
                                    final String failureCmd,
                                    final String args,
                                    final SchedulerImpl scheduler,
                                    final boolean development) {
        setActive(true);
        setArgs(args);
        setDescription(description);
        setDevelopment(development);
        setFailureCmd(failureCmd);
        setFamilyImpl(family);
        setName(name);
        setOrdinal(ordinal);
        setProcessCmd(processCmd);
        setSuccessCmd(successCmd);
        setDefaultScheduler(scheduler);
    }

    // instance member method (alphabetic)

    @Override
    public String getArgs() {
        return args;
    }

    /**
     * Returns the scheduler to to use when executing this definition and none is
     * specified.
     * 
     * @return the scheduler to to use when executing this definition and none is
     *         specified.
     */
    @Override
    @ManyToOne(optional = false,
               targetEntity = SchedulerImpl.class)
    public SchedulerImpl getDefaultScheduler() {
        return defaultScheduler;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getFailureCmd() {
        return failureCmd;
    }

    @Override
    @Transient
    public Family getFamily() {
        return getFamilyImpl();
    }

    /**
     * Returns the {@link FamilyImpl} of definitions to which this instance belongs.
     * 
     * @return the {@link FamilyImpl} of definitions to which this instance belongs.
     */
    @ManyToOne(optional = false,
               targetEntity = FamilyImpl.class)
    // Included for backwards compatibility with 1.2.0 and below
    @JoinColumn(name = "family_familykey")
    protected FamilyImpl getFamilyImpl() {
        return family;
    }

    @Override
    @Transient
    public String getIdentity() {
        return Integer.toString(getProcessDefinitionKey());
    }

    @Override
    @Column(nullable = false)
    public String getName() {
        return name;
    }

    /**
     * Returns the value used to order this object within the family of definitions.
     * 
     * @return the value used to order this object within the family of definitions.
     */
    protected Integer getOrdinal() {
        return ordinal;
    }

    @Override
    @Column(nullable = false)
    public String getProcessCmd() {
        return processCmd;
    }

    /**
     * Returns the key, within the {@link ProcessDefinitionImpl} class, that
     * identifies this instance.
     * 
     * @return the key, within the {@link ProcessDefinitionImpl} class, that
     *         identifies this instance.
     */
    @Id
    @GeneratedValue
    protected int getProcessDefinitionKey() {
        return processDefinitionKey;
    }

    @Override
    public String getSuccessCmd() {
        return successCmd;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    // @Column(nullable = false)
    public Boolean isDevelopment() {
        if (null == development) {
            return Boolean.FALSE;
        }
        return development;
    }

    /**
     * Sets whether items can be processed with this definition or not.
     * 
     * @param active
     *            true when items can be processed with this definition.
     */
    protected void setActive(final boolean active) {
        this.active = active;
    }

    /**
     * Sets the {@link MessageFormat} string that will be command's arguments.
     * 
     * @param string
     *            the {@link MessageFormat} string that will be command's arguments.
     */
    protected void setArgs(final String string) {
        args = string;
    }

    /**
     * Sets the scheduler to to use when executing this definition and none is
     * specified.
     * 
     * @param scheduler
     *            the scheduler to to use when executing this definition and none is
     *            specified.
     */
    protected void setDefaultScheduler(final SchedulerImpl scheduler) {
        defaultScheduler = scheduler;
    }

    /**
     * Sets the description of this family.
     * 
     * @param description
     *            the description of this family.
     */
    protected void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Sets whether this definition is used during process development, which it to
     * say that it is mutable, nor not.
     * 
     * @param development
     *            True when this definition is used during process development
     */
    protected void setDevelopment(Boolean development) {
        this.development = development;
    }

    /**
     * Sets the command the will be run after processing has failed.
     * 
     * @param cmd
     *            the command the will be run after processing has failed.
     */
    protected void setFailureCmd(final String cmd) {
        failureCmd = cmd;
    }

    /**
     * Sets the family of definitions to which this instance belongs.
     * 
     * @param family
     *            the family of definitions to which this instance belongs.
     */
    protected void setFamilyImpl(final FamilyImpl family) {
        this.family = family;
    }

    /**
     * Sets the name of the set of definitions to which this instance belongs.
     * 
     * @param name
     *            the name of the set of definitions to which this instance belongs.
     */
    protected void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets the value used to order this object within the family of definitions.
     * 
     * @param ordinal
     *            the value used to order this object within the family of
     *            definitions.
     */
    protected void setOrdinal(final Integer ordinal) {
        this.ordinal = ordinal;
    }

    /**
     * Sets the command that will be run to process a file.
     * 
     * @param cmd
     *            the command that will be run to process a file.
     */
    protected void setProcessCmd(final String cmd) {
        processCmd = cmd;
    }

    /**
     * Sets the key, within the {@link ProcessDefinitionImpl} class, that identifies
     * this instance.
     * 
     * @param key
     *            the value of the key to set.
     */
    protected void setProcessDefinitionKey(final int key) {
        processDefinitionKey = key;
    }

    /**
     * Sets the command the will be run after processing has succeeded.
     * 
     * @param cmd
     *            the command the will be run after processing has succeeded.
     */
    protected void setSuccessCmd(final String cmd) {
        successCmd = cmd;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
