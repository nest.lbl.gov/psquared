package gov.lbl.nest.psquared.ejb;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import gov.lbl.nest.psquared.AlreadyExitedException;
import gov.lbl.nest.psquared.KnownScheduler;
import gov.lbl.nest.psquared.State;
import gov.lbl.nest.psquared.StateMachineError;
import gov.lbl.nest.psquared.Transition;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

/**
 * This class tracks the path of an item/process pairing through the
 * application's state machine.
 * 
 * @author patton
 */
@Entity
@Table(name = "Pairing2")
@NamedQueries({ @NamedQuery(name = "getPairing",
                            query = "SELECT p " + " FROM PairingImpl p"
                                    + " WHERE p.processDefinition = :definition"
                                    + " AND p.item = :item"),
                @NamedQuery(name = "getPairings",
                            query = "SELECT p " + " FROM PairingImpl p"
                                    + " WHERE p.processDefinition = :definition"
                                    + " AND p.item IN ( :items )"),
                @NamedQuery(name = "getActiveFamilyPairings",
                            query = "SELECT t.pairingImpl" + " FROM TransitionImpl t"
                                    + " WHERE t.succeedingTransition IS NULL"
                                    + " AND t.pairingImpl.item = :item"
                                    + " AND t.pairingImpl.processDefinition IN ( :processes )"
                                    + " AND t.exitImpl.target NOT IN ( :states )") })
public class PairingImpl {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The initial {@link TransitionImpl} of this pairing.
     */
    private TransitionImpl initialTransition;

    /**
     * The identity of the item in this pairing.
     */
    private String item;

    /**
     * The key, within the {@link PairingImpl} class, that identifies this instance.
     */
    private int pairingKey;

    /**
     * The {@link ProcessDefinitionImpl} instance in this pairing.
     */
    private ProcessDefinitionImpl processDefinition;

    /**
     * The {@link KnownScheduler} instance that was last used to schedule this pair.
     */
    private SchedulerImpl scheduler;

    /**
     * The ordered list of {@link TransitionImpl} instance that this pairing has
     * been through.
     */
    private List<TransitionImpl> transitions;

    /**
     * The {@link UUID} for this ticket.
     */
    private UUID uuid;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected PairingImpl() {
        setTransitions(new ArrayList<TransitionImpl>());
    }

    /**
     * Creates an instance of this class.
     * 
     * @param item
     *            the identity of the item in this pairing.
     * @param definition
     *            the {@link ProcessDefinitionImpl} instance in this pairing.
     * @param scheduler
     *            TODO
     */
    PairingImpl(final String item,
                final ProcessDefinitionImpl definition,
                final SchedulerImpl scheduler) {
        setItem(item);
        setProcessDefinition(definition);
        setScheduler(scheduler);
        setTransitions(new ArrayList<TransitionImpl>());
        setUuid(UUID.randomUUID());
    }

    // instance member method (alphabetic)

    /**
     * Constructs a new {@link Transition} for this pairing.
     * 
     * @param transition
     *            the transition that precedes the one being created.
     * @param target
     *            the target {@link State} of this transition.
     * @param message
     *            the message, if any, associated with this transition,
     *            <code>null</code> otherwise.
     * 
     * @return the created {@link Transition}.
     * 
     * @throws AlreadyExitedException
     *             when the specified transition has already created a successor.
     * @throws StateMachineError
     *             when the mechanic of the state machine are inconsistent.
     */
    TransitionImpl createTransition(final TransitionImpl transition,
                                    final ExitImpl exit,
                                    final String message) throws AlreadyExitedException {
        final List<TransitionImpl> transactions = getTransitions();
        final TransitionImpl result;
        if (null == transition) {
            if (null == exit.getOrigin()) {
                if (transactions.isEmpty()) {
                    result = new TransitionImpl(this,
                                                exit,
                                                message);
                    setInitialTransition(result);
                } else {
                    throw new StateMachineError("Initial Transition has already been created");
                }
            } else {
                throw new StateMachineError("Initial transition must be from a null state");
            }
        } else {
            if (this != transition.getPairingImpl()) {
                throw new StateMachineError("Preceding Transaction does not belong to this Pairing");
            }
            if (!transition.isLast()) {
                throw new AlreadyExitedException("Preceding Transaction is not the last transaction for this Pairing",
                                                 transactions.get(0));
            }
            result = new TransitionImpl(transition,
                                        exit,
                                        message);
        }
        transactions.add(0,
                         result);
        return result;
    }

    /**
     * Returns the initial {@link Transition} of this pairing.
     * 
     * @return the initial {@link Transition} of this pairing.
     */
    @OneToOne
    protected TransitionImpl getInitialTransition() {
        return initialTransition;
    }

    /**
     * Returns the identity of the item in this pairing.
     * 
     * @return the identity of the item in this pairing.
     */
    @Column(nullable = false)
    protected String getItem() {
        return item;
    }

    /**
     * Returns the most resent {@link Transition} of this pairing.
     * 
     * @return the most resent {@link Transition} of this pairing.
     */
    @Transient
    protected TransitionImpl getLastTransition() {
        final List<TransitionImpl> allTransitions = getTransitions();
        if (null == allTransitions || allTransitions.isEmpty()) {
            return null;
        }
        return allTransitions.get(0);
    }

    /**
     * Returns the key, within the {@link PairingImpl} class, that identifies this
     * instance.
     * 
     * @return the key, within the {@link PairingImpl} class, that identifies this
     *         instance.
     */
    @Id
    @GeneratedValue
    protected int getPairingKey() {
        return pairingKey;
    }

    /**
     * Returns the {@link ProcessDefinitionImpl} instance in this pairing.
     *
     * @return the {@link ProcessDefinitionImpl} instance in this pairing.
     */
    @ManyToOne(optional = false)
    protected ProcessDefinitionImpl getProcessDefinition() {
        return processDefinition;
    }

    /**
     * Returns the {@link KnownScheduler} instance that was last used to schedule
     * this pair.
     *
     * @return the {@link KnownScheduler} instance that was last used to schedule
     *         this pair.
     */
    @ManyToOne(optional = false)
    protected SchedulerImpl getScheduler() {
        return scheduler;
    }

    /**
     * Returns the collection of {@link Transition} instance that this pairing has
     * been through.
     * 
     * @return the collection of {@link Transition} instance that this pairing has
     *         been through.
     */
    @OneToMany(mappedBy = "pairingImpl")
    @OrderBy("whenOccurred DESC")
    protected List<TransitionImpl> getTransitions() {
        return transitions;
    }

    /**
     * Returns the {@link UUID} for this ticket.
     * 
     * @return the {@link UUID} for this ticket.
     */
    protected UUID getUuid() {
        return uuid;
    }

    /**
     * Sets the initial {@link Transition} of this pairing.
     * 
     * @param transition
     *            the initial {@link Transition} of this pairing.
     */
    protected void setInitialTransition(final TransitionImpl transition) {
        initialTransition = transition;
    }

    /**
     * Sets the identity of the item in this pairing.
     * 
     * @param identity
     *            the identity of the item in this pairing.
     */
    protected void setItem(final String identity) {
        item = identity;
    }

    /**
     * Sets the key, within the {@link PairingImpl} class, that identifies this
     * instance.
     * 
     * @param key
     *            the value of the key to set.
     */
    protected void setPairingKey(final int key) {
        pairingKey = key;
    }

    /**
     * Sets the {@link ProcessDefinitionImpl} instance in this pairing.
     * 
     * @param definition
     *            the {@link ProcessDefinitionImpl} instance in this pairing.
     */
    protected void setProcessDefinition(final ProcessDefinitionImpl definition) {
        processDefinition = definition;
    }

    /**
     * Sets the {@link KnownScheduler} instance that was last used to schedule this
     * pair.
     *
     * @param scheduler
     *            the {@link KnownScheduler} instance that was last used to schedule
     *            this pair.
     */
    protected void setScheduler(final SchedulerImpl scheduler) {
        this.scheduler = scheduler;
    }

    /**
     * Sets the list of {@link Transition} instance that this pairing has been
     * through.
     * 
     * @param transactions
     *            the list of {@link Transition} instance that this pairing has been
     *            through.
     */
    protected void setTransitions(final List<TransitionImpl> transactions) {
        this.transitions = transactions;
    }

    /**
     * Sets the {@link UUID} for this ticket.
     *
     * @param uuid
     *            the {@link UUID} for this ticket.
     */
    protected void setUuid(final UUID uuid) {
        this.uuid = uuid;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
