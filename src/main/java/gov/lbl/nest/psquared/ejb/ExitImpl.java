package gov.lbl.nest.psquared.ejb;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

/**
 * This class enumerates the known exits for all states.
 * 
 * @author patton
 */
@Entity
@Table(name = "Exit2")
@NamedQueries({ @NamedQuery(name = "getExit",
                            query = "SELECT e " + " FROM ExitImpl e"
                                    + " WHERE name = :name"
                                    + " AND origin = :origin"),
                @NamedQuery(name = "getExitFromNull",
                            query = "SELECT e " + " FROM ExitImpl e"
                                    + " WHERE name = :name"
                                    + " AND origin is NULL") })
public class ExitImpl {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The key, within the {@link ExitImpl} class, that identifies this instance.
     */
    private int exitKey;

    /**
     * The name of this object within the context of its originating state.
     */
    private String name;

    /**
     * The {@link StateImpl} instance from which this object is an exit.
     */
    private StateImpl origin;

    /**
     * The {@link StateImpl} instance that this object targets.
     */
    private StateImpl target;

    // constructors

    /**
     * Creates and instance of this class.
     */
    protected ExitImpl() {
    }

    /**
     * Creates and instance of this class.
     * 
     * @param name
     *            the name of this object within the context of its originating
     *            state.
     * @param origin
     *            the {@link StateImpl} instance from which this object is an exit.
     * @param target
     *            the {@link StateImpl} instance that this object targets.
     */
    ExitImpl(String name,
             StateImpl origin,
             StateImpl target) {
        setName(name);
        setOrigin(origin);
        setTarget(target);
    }

    // instance member method (alphabetic)

    /**
     * Returns the key, within the {@link ExitImpl} class, that identifies this
     * instance.
     * 
     * @return the key, within the {@link ExitImpl} class, that identifies this
     *         instance.
     */
    @Id
    @GeneratedValue
    protected int getExitKey() {
        return exitKey;
    }

    /**
     * Returns the name of this object within the context of its originating state.
     * 
     * @return the name of this object within the context of its originating state.
     */
    protected String getName() {
        return name;
    }

    /**
     * Returns the {@link StateImpl} instance from which this object is an exit.
     * 
     * @return the {@link StateImpl} instance from which this object is an exit.
     */
    @ManyToOne
    protected StateImpl getOrigin() {
        return origin;
    }

    /**
     * Returns the {@link StateImpl} instance that this object targets.
     * 
     * @return the {@link StateImpl} instance that this object targets.
     */
    @ManyToOne(optional = false)
    protected StateImpl getTarget() {
        return target;
    }

    /**
     * Sets the key, within the {@link TransitionImpl} class, that identifies this
     * instance.
     * 
     * @param key
     *            the value of the key to set.
     */
    protected void setExitKey(int key) {
        this.exitKey = key;
    }

    /**
     * Sets the name of this object within the context of its originating state.
     * 
     * @param name
     *            the name of this object within the context of its originating
     *            state.
     */
    protected void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the {@link StateImpl} instance from which this object is an exit.
     * 
     * @param start
     *            the {@link StateImpl} instance from which this object is an exit.
     */
    protected void setOrigin(StateImpl start) {
        this.origin = start;
    }

    /**
     * Sets the {@link StateImpl} instance that this object targets.
     * 
     * @param state
     *            the {@link StateImpl} instance that this object targets.
     */
    protected void setTarget(StateImpl state) {
        this.target = state;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
