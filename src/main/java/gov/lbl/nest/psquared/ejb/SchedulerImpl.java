package gov.lbl.nest.psquared.ejb;

import java.net.URI;

import gov.lbl.nest.psquared.KnownScheduler;
import gov.lbl.nest.psquared.PSquared;
import gov.lbl.nest.psquared.ProcessDefinition;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

/**
 * This class captures the instruction used to submit a item/process pair for
 * processing.
 * 
 * @author patton
 */
@Entity
@Table(name = "Scheduler2")
@Inheritance(strategy = InheritanceType.JOINED)
@NamedQueries({ @NamedQuery(name = "activeSchedulers",
                            query = "SELECT s " + " FROM SchedulerImpl s"
                                    + " WHERE active = true"),
                @NamedQuery(name = "getScheduler",
                            query = "SELECT s " + " FROM SchedulerImpl s"
                                    + " WHERE s.name = :name"
                                    + " AND active = true") })
public abstract class SchedulerImpl implements
                                    KnownScheduler {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * True when items can be processed with this definition.
     */
    private boolean active;

    /**
     * The description of this scheduler.
     */
    private String description;

    /**
     * <code>true</code> when a call to
     * {@link #submit(String, ProcessDefinition, URI, PSquared)} is required when
     * re-submission of a jobs is happening automatically.
     */
    private boolean resubmit;

    /**
     * The key, within the {@link SchedulerImpl} class, that identifies this
     * instance.
     */
    private int schedulerKey;

    /**
     * The name by which this submission instruction is referenced.
     */
    private String name;

    // constructors

    /**
     * Create instance of this class.
     */
    protected SchedulerImpl() {
    }

    /**
     * Create instance of this class.
     * 
     * @param name
     *            the submission instruction defined by this object.
     * @param description
     *            the description of this scheduler.
     */
    protected SchedulerImpl(String name,
                            String description) {
        setActive(true);
        setName(name);
        setDescription(description);
    }

    // instance member method (alphabetic)

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    @Transient
    public String getIdentity() {
        return Integer.toString(getSchedulerKey());
    }

    /**
     * Returns the name by which this submission instruction is referenced.
     * 
     * @return the name by which this submission instruction is referenced.
     */
    @Override
    @Column(nullable = false,
            unique = true)
    public String getName() {
        return name;
    }

    @Override
    public boolean getResubmit() {
        return resubmit;
    }

    /**
     * Returns the key, within the {@link SchedulerImpl} class, that identifies this
     * instance.
     * 
     * @return the key, within the {@link SchedulerImpl} class, that identifies this
     *         instance.
     */
    @Id
    @GeneratedValue
    protected int getSchedulerKey() {
        return schedulerKey;
    }

    /**
     * Returns true when items can be processed with this definition.
     * 
     * @return true when items can be processed with this definition.
     */
    protected boolean isActive() {
        return active;
    }

    /**
     * Sets whether items can be processed with this definition or not.
     * 
     * @param active
     *            true when items can be processed with this definition.
     */
    protected void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Sets the description of this family.
     * 
     * @param description
     *            the description of this family.
     */
    protected void setDescription(String description) {
        this.description = description;
    }

    /**
     * Sets the name by which this submission instruction is referenced.
     * 
     * @param name
     *            the name by which this submission instruction is referenced.
     */
    protected void setName(String name) {
        this.name = name;
    }

    /**
     * Sets whether a call to
     * {@link #submit(String, ProcessDefinition, URI, PSquared)} is required when
     * re-submission of a jobs is happening automatically or not.
     * 
     * @param resubmit
     *            <code>true</code> when a call to
     *            {@link #submit(String, ProcessDefinition, URI, PSquared)} is
     *            required when re-submission of a jobs is happening automatically.
     */
    protected void setResubmit(boolean resubmit) {
        this.resubmit = resubmit;
    }

    /**
     * Sets the key, within the {@link SchedulerImpl} class, that identifies this
     * instance.
     * 
     * @param key
     *            the value of the key to set.
     */
    protected void setSchedulerKey(int key) {
        schedulerKey = key;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
