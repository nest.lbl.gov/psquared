/**
 * This package provides the EJB implementations needed to PSquared persistence.
 * 
 * @author patton
 */
package gov.lbl.nest.psquared.ejb;