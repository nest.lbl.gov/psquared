package gov.lbl.nest.psquared.ejb;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

/**
 * This class enumerates the known states.
 * 
 * @author patton
 */
@Entity
@Table(name = "State2")
@NamedQueries({ @NamedQuery(name = "getState",
                            query = "SELECT s " + " FROM StateImpl s"
                                    + " WHERE name = :name"),
                @NamedQuery(name = "getStates",
                            query = "SELECT s " + " FROM StateImpl s"
                                    + " WHERE name IN  :names") })
public class StateImpl {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The key, within the {@link StateImpl} class, that identifies this instance.
     */
    private int stateKey;

    /**
     * The name of this object.
     */
    private String name;

    // constructors

    /**
     * Creates and instance of this class.
     */
    protected StateImpl() {
    }

    /**
     * Creates and instance of this class.
     * 
     * @param name
     *            the name of this object within the context of its originating
     *            state.
     */
    StateImpl(String name) {
        setName(name);
    }

    // instance member method (alphabetic)

    /**
     * Returns the name of this object within the context of its originating state.
     * 
     * @return the name of this object within the context of its originating state.
     */
    protected String getName() {
        return name;
    }

    /**
     * Returns the key, within the {@link StateImpl} class, that identifies this
     * instance.
     * 
     * @return the key, within the {@link StateImpl} class, that identifies this
     *         instance.
     */
    @Id
    @GeneratedValue
    protected int getStateKey() {
        return stateKey;
    }

    /**
     * Sets the name of this object within the context of its originating state.
     * 
     * @param name
     *            the name of this object within the context of its originating
     *            state.
     */
    protected void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the key, within the {@link TransitionImpl} class, that identifies this
     * instance.
     * 
     * @param key
     *            the value of the key to set.
     */
    protected void setStateKey(int key) {
        this.stateKey = key;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
