package gov.lbl.nest.psquared.ejb;

import java.util.Comparator;
import java.util.List;

/**
 * This class is used to sort the list returned by the JQL query into the order
 * in which the items where originally specified.
 * 
 * @author patton
 */
class PairingComparator implements
                        Comparator<PairingImpl> {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The collection of items with which to order the list.
     */
    private final List<String> items;

    // constructors

    PairingComparator(final List<String> items) {
        this.items = items;
    }

    // instance member method (alphabetic)

    @Override
    public int compare(PairingImpl lhs,
                       PairingImpl rhs) {
        int lhsIndex = items.indexOf(lhs.getItem());
        int rhsIndex = items.indexOf(rhs.getItem());
        if (-1 == lhsIndex) {
            if (-1 == rhsIndex) {
                return (lhs.getPairingKey() - rhs.getPairingKey());
            }
            return 1;
        }
        if (-1 == rhsIndex) {
            return -1;
        }
        return lhsIndex - rhsIndex;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
