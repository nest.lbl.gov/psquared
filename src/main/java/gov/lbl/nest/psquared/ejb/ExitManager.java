package gov.lbl.nest.psquared.ejb;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.psquared.AlreadyExitedException;
import gov.lbl.nest.psquared.ForbiddenTransitionException;
import gov.lbl.nest.psquared.KnownScheduler;
import gov.lbl.nest.psquared.State;
import gov.lbl.nest.psquared.StateMachine;
import jakarta.ejb.AccessTimeout;
import jakarta.ejb.Lock;
import jakarta.ejb.LockType;
import jakarta.ejb.Singleton;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.TypedQuery;

/**
 * The class manages the exit from one state into another.
 * 
 * @author patton
 */
@Singleton
@AccessTimeout(value = 10,
               unit = TimeUnit.MINUTES)
public class ExitManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} instance used bu this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ExitManager.class);

    // private static member data

    // private instance member data

    /**
     * The {@link EntityManager} instance used by this object.
     */
    @Inject
    @PSquaredDB
    private EntityManager entityManager;

    // constructors

    /**
     * Create an instance of this class.
     */
    public ExitManager() {
    }

    /**
     * Create an instance of this class for test purposes.
     */
    ExitManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    // instance member method (alphabetic)

    /**
     * Creates a new pairing in its own transaction.
     * 
     * @param item
     *            the identity of the item in this pairing.
     * @param process
     *            the process definition associated in this pairing.
     * @param scheduler
     *            TODO
     * 
     * @return the created {@link PairingImpl} instance.
     */
    @Lock(LockType.WRITE)
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public PairingImpl createPairing(String item,
                                     ProcessDefinitionImpl process,
                                     KnownScheduler scheduler) {
        LOG.debug("Entering \"submitPairing\" for \"" + item
                  + "\"");
        final ProcessDefinitionImpl localDefinition = entityManager.merge(process);
        final SchedulerImpl localScheduler;
        if (null == scheduler) {
            localScheduler = localDefinition.getDefaultScheduler();
        } else {
            localScheduler = entityManager.merge((SchedulerImpl) scheduler);
        }
        final PairingImpl result = new PairingImpl(item,
                                                   localDefinition,
                                                   localScheduler);
        entityManager.persist(result);
        LOG.debug("Exiting \"createPairing\" for \"" + item
                  + "\"");
        return result;
    }

    /**
     * Attempts the transition the pairing of a item and process definition from one
     * state to another.
     * 
     * @param pairing
     *            the item/configuration pairing whose transition
     * @param exit
     *            the name of the exit to take out of the state that resulted from
     *            the previously completed transition.
     * @param message
     *            a message, if any, to associate with the new transition if it
     *            completed.
     * 
     * @return the completed transaction, <code>null</code> otherwise.
     * 
     * @throws AlreadyExitedException
     *             when the current state was exited by a different thread before
     *             this one could exit.
     * @throws ForbiddenTransitionException
     *             when the current state does not allow the requested exit.
     */
    @Lock(LockType.WRITE)
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public TransitionImpl exitState(PairingImpl pairing,
                                    String exit,
                                    String message) throws AlreadyExitedException,
                                                    ForbiddenTransitionException {
        return exitState(pairing,
                         pairing.getLastTransition(),
                         exit,
                         message);
    }

    /**
     * Attempts the transition the pairing of a item and process definition from one
     * state to another.
     * 
     * @param pairing
     *            the item/configuration pairing whose transition
     * @param localTransition
     *            the {@link TransitionImpl} instance that was the entry to the
     *            state from which to exit.
     * @param exit
     *            the name of the exit to take out of the state that resulted from
     *            the previously completed transition.
     * @param message
     *            a message, if any, to associate with the new transition if it
     *            completed.
     * 
     * @return the completed transaction, <code>null</code> otherwise.
     * 
     * @throws AlreadyExitedException
     *             when the specified transition did not result in current state.
     * @throws ForbiddenTransitionException
     *             when the state resulting from the specified transition does not
     *             allow the requested exit.
     */
    private TransitionImpl exitState(PairingImpl pairing,
                                     TransitionImpl transition,
                                     String exit,
                                     String message) throws AlreadyExitedException,
                                                     ForbiddenTransitionException {
        if (null == transition) {
            LOG.debug("Entering \"exitState\"");
        } else {
            LOG.debug("Entering \"exitState\" for " + transition.getTransitionKey());
        }
        final PairingImpl localPairing = entityManager.merge(pairing);
        final TransitionImpl localTransition;
        if (null == transition) {
            localTransition = null;
        } else {
            localTransition = entityManager.merge(transition);
        }
        final ExitImpl exitToUse;
        if (null == localTransition) {
            final StateImpl submitted = getStateImpl(State.SUBMITTED);
            exitToUse = getInitialExit(exit,
                                       submitted);
        } else {
            final State currentState = localTransition.getState();
            final StateImpl stateImpl = getStateImpl(currentState);
            final ExitImpl foundExit = findExit(exit,
                                                stateImpl);
            if (null == foundExit) {
                final State target = StateMachine.getTarget(currentState,
                                                            exit);
                if (null == target) {
                    LOG.debug("Exiting \"exitState\" for " + transition.getTransitionKey());
                    throw new ForbiddenTransitionException("Can not exit the current " + currentState
                                                           + " via the \""
                                                           + exit
                                                           + "\" exit",
                                                           localTransition);
                }
                exitToUse = new ExitImpl(exit,
                                         stateImpl,
                                         getStateImpl(target));
                entityManager.persist(exitToUse);
            } else {
                exitToUse = foundExit;
            }
        }
        final String messageToUse;
        if (null == message || "".equals(message.trim())) {
            messageToUse = null;
        } else {
            messageToUse = message.trim();
        }
        final TransitionImpl resultingTransition = localPairing.createTransition(localTransition,
                                                                                 exitToUse,
                                                                                 messageToUse);
        entityManager.persist(resultingTransition);
        LOG.debug("Exiting \"exitState\" having created " + resultingTransition.getTransitionKey());
        return resultingTransition;
    }

    /**
     * Attempts the transition the pairing of a item and process definition from one
     * state to another.
     * 
     * @param transition
     *            the {@link TransitionImpl} instance that was the entry to the
     *            current state.
     * @param exit
     *            the name of the exit to take out of the state that resulted from
     *            the previously completed transition.
     * @param message
     *            a message, if any, to associate with the new transition if it
     *            completed.
     * 
     * @return the completed transaction, <code>null</code> otherwise.
     * 
     * @throws AlreadyExitedException
     *             when the specified transition did not result in current state.
     * @throws ForbiddenTransitionException
     *             when the state resulting from the specified transition does not
     *             allow the requested exit.
     */
    @Lock(LockType.WRITE)
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public TransitionImpl exitState(TransitionImpl transition,
                                    String exit,
                                    String message) throws AlreadyExitedException,
                                                    ForbiddenTransitionException {
        return exitState(transition.getPairingImpl(),
                         transition,
                         exit,
                         message);
    }

    /**
     * Returns the {@link ExitImpl} instance that matches the specified exit.
     * 
     * @param name
     *            the name of the exit within the context of its originating state.
     * @param origin
     *            the {@link StateImpl} instance from the returned {@link ExitImpl}
     *            instance is an exit.
     */
    private ExitImpl findExit(String exit,
                              StateImpl origin) {
        final TypedQuery<ExitImpl> query;
        if (null == origin) {
            query = entityManager.createNamedQuery("getExitFromNull",
                                                   ExitImpl.class);
        } else {
            query = entityManager.createNamedQuery("getExit",
                                                   ExitImpl.class);
            query.setParameter("origin",
                               origin);
        }
        query.setParameter("name",
                           exit);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Returns the {@link ExitImpl} from the initial state.
     * 
     * @param name
     */
    private ExitImpl getInitialExit(String name,
                                    StateImpl target) {
        LOG.debug("Entering \"getInitialExit\"");
        final ExitImpl existingExit = findExit(name,
                                               null);
        if (null != existingExit) {
            if (target != existingExit.getTarget()) {
                throw new IllegalArgumentException("Mismatch with previous declaration of initial transiation");
            }
            return existingExit;
        }
        final ExitImpl createdExit = new ExitImpl(name,
                                                  null,
                                                  target);
        entityManager.persist(createdExit);
        LOG.debug("Exiting \"getInitialExit\"");
        return createdExit;
    }

    /**
     * Returns the {@link StateImpl} with the specified name.
     * 
     * @param state
     *            the {@link State} whose {@link StateImpl} instance should be
     *            returned.
     */
    private StateImpl getStateImpl(State state) {
        final TypedQuery<StateImpl> query = entityManager.createNamedQuery("getState",
                                                                           StateImpl.class);
        query.setParameter("name",
                           state.toString());
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            final StateImpl createdState = new StateImpl(state.toString());
            entityManager.persist(createdState);
            return createdState;
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
