package gov.lbl.nest.psquared.ejb;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import jakarta.inject.Qualifier;

/**
 * The annotation is used to qualify the PSquared Database access.
 * 
 * @author patton
 */
@Qualifier
@Retention(RUNTIME)
@Target({ TYPE,
          METHOD,
          FIELD,
          PARAMETER })
public @interface PSquaredDB {
    // Does nothing, just a label.
}