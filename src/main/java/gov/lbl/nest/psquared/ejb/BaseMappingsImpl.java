package gov.lbl.nest.psquared.ejb;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

/**
 * This class maps the URI that forms that base of a request onto the URI that
 * forms that base of the response.
 * 
 * @author patton
 */
@Entity
@Table(name = "BaseMapping2")
@NamedQueries({ @NamedQuery(name = "getMappedBase",
                            query = "SELECT b.value " + " FROM BaseMappingsImpl b"
                                    + " WHERE key = :key") })
public class BaseMappingsImpl {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The base URI of the request.
     */
    private String key;

    /**
     * The key, within the {@link BaseMappingsImpl} class, that identifies this
     * instance.
     */
    private int mappingKey;

    /**
     * The base URI of the response.
     */
    private String value;

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the base URI of the request.
     * 
     * @return the base URI of the request.
     */
    protected String getKey() {
        return key;
    }

    /**
     * Returns the key, within the {@link BaseMappingsImpl} class, that identifies
     * this instance.
     * 
     * @return the key, within the {@link BaseMappingsImpl} class, that identifies
     *         this instance.
     */
    @Id
    @GeneratedValue
    protected int getMappingKey() {
        return mappingKey;
    }

    /**
     * Returns the base URI of the response.
     * 
     * @return the base URI of the response.
     */
    protected String getValue() {
        return value;
    }

    /**
     * Sets the base URI of the request.
     * 
     * @param key
     *            the base URI of the request.
     */
    protected void setKey(String key) {
        this.key = key;
    }

    /**
     * Sets the key, within the {@link BaseMappingsImpl} class, that identifies this
     * instance.
     * 
     * @param key
     *            the key, within the {@link BaseMappingsImpl} class, that
     *            identifies this instance.
     */
    protected void setMappingKey(int key) {
        mappingKey = key;
    }

    /**
     * Sets the base URI of the response.
     * 
     * @param value
     *            the base URI of the response.
     */
    protected void setValue(String value) {
        this.value = value;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
