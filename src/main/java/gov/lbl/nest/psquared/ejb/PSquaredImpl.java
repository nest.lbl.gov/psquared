package gov.lbl.nest.psquared.ejb;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.monitor.Instrumentation;
import gov.lbl.nest.psquared.AlreadyExitedException;
import gov.lbl.nest.psquared.ExistingEntityException;
import gov.lbl.nest.psquared.Family;
import gov.lbl.nest.psquared.ForbiddenTransitionException;
import gov.lbl.nest.psquared.KnownScheduler;
import gov.lbl.nest.psquared.PSquared;
import gov.lbl.nest.psquared.ProcessDefinition;
import gov.lbl.nest.psquared.State;
import gov.lbl.nest.psquared.StateMachine;
import gov.lbl.nest.psquared.SubmitException;
import gov.lbl.nest.psquared.Transition;
import gov.lbl.nest.psquared.UriHandler;
import gov.lbl.nest.psquared.VetoedException;
import gov.lbl.nest.psquared.scheduler.BashScheduler;
import gov.lbl.nest.psquared.scheduler.NopScheduler;
import gov.lbl.nest.psquared.scheduler.RabbitMQScheduler;
import gov.lbl.nest.psquared.scheduler.Utilities;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.NonUniqueResultException;
import jakarta.persistence.TypedQuery;

/**
 * This class implements the {@link PSquared} interface using JPA.
 *
 * @author patton
 */
@Stateless
public class PSquaredImpl implements
                          PSquared {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} instance used bu this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(PSquaredImpl.class);

    /**
     * The default location of executables in the psquared user area.
     */
    private static final String DEFAULT_BIN_DIRECTORY = "bin";

    /**
     * The default psquared user area.
     */
    private static final String DEFAULT_PSQRD_DIRECTORY = "psquared";

    /**
     * The string used for the family veto.
     */
    private static final String FAMILY_VETO = "family";

    /**
     * The list of states to ignore when active pairing.
     */
    private static final List<String> NON_ACTIVE_STATES = Arrays.asList(new String[] { State.READY.toString(),
                                                                                       State.ABANDONED.toString() });

    /**
     * The empty list to return when no states are found.
     */
    private static final List<StateImpl> NO_STATES = new ArrayList<>();

    /**
     * The empty list to return when no transitions are found.
     */
    private static final List<Transition> NO_TRANSITIONS = new ArrayList<>();

    // private static member data

    // private instance member data

    /**
     * Returns the path to the local directory containing psquared's executables.
     *
     * @return the path to the local directory containing psquared's executables.
     */
    private static String getBinDir() {
        final String home = System.getProperty("user.home");
        return home + File.separatorChar
               + DEFAULT_PSQRD_DIRECTORY
               + File.separatorChar
               + DEFAULT_BIN_DIRECTORY;
    }

    private static void instrumentEntry(final TransitionImpl result,
                                        final Map<String, String> attributes,
                                        Instrumentation instrumentation) {
        if (Instrumentation.NULL_INSTRUMENTATION != instrumentation) {
            attributes.remove("transition");
            attributes.put("state",
                           (result.getState()).name());
            instrumentation.pointOfInterst("state",
                                           (result.getPairingImpl()).getUuid(),
                                           attributes);
        }
    }

    private static Map<String, String> instrumentExit(PairingImpl pairing,
                                                      String exit,
                                                      Instrumentation instrumentation) {
        final Map<String, String> attributes;
        if (Instrumentation.NULL_INSTRUMENTATION != instrumentation) {
            attributes = prepareInstrumentation(pairing,
                                                instrumentation);
            attributes.put("transition",
                           exit);
            instrumentation.pointOfInterst("exit",
                                           pairing.getUuid(),
                                           attributes);
        } else {
            attributes = null;
        }
        return attributes;
    }

    private static Map<String, String> prepareInstrumentation(PairingImpl pairing,
                                                              Instrumentation instrumentation) {
        final Map<String, String> attributes;
        if (Instrumentation.NULL_INSTRUMENTATION != instrumentation) {
            attributes = new HashMap<>(1);
            attributes.put("item",
                           pairing.getItem());
            final ProcessDefinitionImpl processDefinition = pairing.getProcessDefinition();
            attributes.put("configuration.family",
                           (processDefinition.getFamily()).getName());
            attributes.put("configuration.name",
                           processDefinition.getName());
        } else {
            attributes = null;
        }
        return attributes;
    }

    // constructors

    /**
     * True when the default files have been installed.
     */
    private boolean defaultsInstalled = false;

    /**
     * The {@link EntityManager} instance used by this object.
     */
    @Inject
    @PSquaredDB
    private EntityManager entityManager;

    // instance member method (alphabetic)

    /**
     * The {@link ExitManager} instance used by this object.
     */
    @Inject
    private ExitManager exitManager;

    /**
     * The {@link Instrumentation} instance, if any, used by this object.
     */
    private Instrumentation instrumentation;

    /**
     * Create an instance of this class.
     */
    public PSquaredImpl() {
    }

    /**
     * Create an instance of this class for test purposes.
     */
    PSquaredImpl(final EntityManager entityManager,
                 final StateMachine stateMachine,
                 final ExitManager exitManager) {
        this.exitManager = exitManager;
        this.entityManager = entityManager;
    }

    private ProcessDefinitionImpl bindProcessDefinitionImpl(final ProcessDefinition process) {
        final ProcessDefinitionImpl processDefinition = findProcessDefinition(process);
        if (null == processDefinition) {
            throw new IllegalArgumentException("There is no version \"" + process.getName()
                                               + "\" of process definition  \""
                                               + (process.getFamily()).getName()
                                               + "\"");
        }
        return processDefinition;
    }

    @Override
    public FamilyImpl createFamily(final String name,
                                   final String description) throws ExistingEntityException {
        final TypedQuery<Boolean> query = entityManager.createNamedQuery("isExistingFamily",
                                                                         Boolean.class);
        query.setParameter("name",
                           name);
        if (query.getSingleResult()) {
            throw new ExistingEntityException("There is already a family called \"" + name
                                              + "\".");
        }
        final FamilyImpl family = new FamilyImpl(name,
                                                 description);
        entityManager.persist(family);
        return family;
    }

    private ProcessDefinitionImpl createProcessDefinition(final String name,
                                                          final String description,
                                                          final FamilyImpl family,
                                                          final String processCmd,
                                                          final String successCmd,
                                                          final String failureCmd,
                                                          final String args,
                                                          final SchedulerImpl scheduler,
                                                          final boolean development) throws ExistingEntityException {
        final TypedQuery<Boolean> query = entityManager.createNamedQuery("isExistingDefinition",
                                                                         Boolean.class);
        query.setParameter("family",
                           family);
        query.setParameter("name",
                           name);
        if (query.getSingleResult()) {
            throw new ExistingEntityException("There is already a process definition for this family called \"" + name
                                              + "\".");
        }

        final TypedQuery<Integer> query2 = entityManager.createNamedQuery("getLastOrdinalOfFamily",
                                                                          Integer.class);
        query2.setParameter("family",
                            family);
        query2.setMaxResults(1);
        int lastOrdinal;
        try {
            lastOrdinal = query2.getSingleResult();
        } catch (NoResultException e) {
            lastOrdinal = 0;
        }
        final int ordinal = lastOrdinal + 1;
        final ProcessDefinitionImpl definition = new ProcessDefinitionImpl(name,
                                                                           description,
                                                                           family,
                                                                           ordinal,
                                                                           processCmd,
                                                                           successCmd,
                                                                           failureCmd,
                                                                           args,
                                                                           scheduler,
                                                                           development);
        entityManager.persist(definition);
        if (null == family.getDefaultDefinition()) {
            family.setDefaultDefinition(definition);
        }
        return definition;
    }

    @Override
    public ProcessDefinitionImpl createProcessDefinition(final String name,
                                                         final String description,
                                                         final URI familyUri,
                                                         final String processCmd,
                                                         final String successCmd,
                                                         final String failureCmd,
                                                         final String args,
                                                         final URI schedulerUri,
                                                         final boolean development,
                                                         final UriHandler uriHandler) throws ExistingEntityException {
        final FamilyImpl family = getFamily(uriHandler.getIdentity(familyUri));

        final SchedulerImpl scheduler;
        if (null == schedulerUri) {
            scheduler = null;
        } else {
            String identity = uriHandler.getIdentity(schedulerUri);
            scheduler = getScheduler(identity);
            if (null == scheduler) {
                throw new IllegalArgumentException("No Scheduler with URI\"" + identity
                                                   + "\" exists");
            }
        }
        return createProcessDefinition(name,
                                       description,
                                       family,
                                       processCmd,
                                       successCmd,
                                       failureCmd,
                                       args,
                                       scheduler,
                                       development);
    }

    /**
     * Runs any entry action associated with a successful transition.
     *
     * @param transition2
     *            the successful transition.
     * @param uriHandler
     *            the {@link UriHandler} used to create realized state URIs.
     * @param reschedule
     *            true if the transition is a re-submission transition.
     *
     * @return the successful transition.
     *
     * @throws AlreadyExitedException
     * @throws ForbiddenTransitionException
     */
    private TransitionImpl entryAction(final TransitionImpl transition2,
                                       final UriHandler uriHandler,
                                       final boolean reschedule) throws AlreadyExitedException,
                                                                 ForbiddenTransitionException {
        final TransitionImpl transition = entityManager.merge(transition2);
        if (State.SUBMITTED == transition.getState()) {
            final PairingImpl pairing = transition.getPairingImpl();
            final ProcessDefinitionImpl processDefinition = pairing.getProcessDefinition();
            final URI submissionUri = uriHandler.buildFromIdentity(transition.getIdentity());
            final SchedulerImpl scheduler = pairing.getScheduler();
            if (null == scheduler) {
                throw new IllegalArgumentException("No scheduler was assigned to this pair");
            }
            try {
                final String description;
                if (reschedule) {
                    if (scheduler.getResubmit()) {
                        scheduler.submit(pairing.getItem(),
                                         processDefinition,
                                         submissionUri,
                                         this);
                    }
                } else {
                    description = scheduler.submit(pairing.getItem(),
                                                   processDefinition,
                                                   submissionUri,
                                                   this);
                    final String message = transition.getMessage();
                    final StringBuilder builder;
                    if (null == message || "".equals(message.trim())) {
                        builder = new StringBuilder();
                    } else {
                        builder = new StringBuilder(message.trim());
                        builder.append('\n');
                    }
                    if (null != description && !"".equals(description.trim())) {
                        builder.append(description);
                    }
                    final String messageToUse;
                    if (builder.length() > TransitionImpl.MESSAGE_LIMIT) {
                        messageToUse = builder.substring(0,
                                                         TransitionImpl.MESSAGE_LIMIT);
                    } else {
                        messageToUse = builder.toString();
                    }
                    transition.setMessage(messageToUse.trim());
                }
            } catch (SubmitException e) {
                return exitState(submissionUri,
                                 "failed",
                                 e.getMessage(),
                                 uriHandler);
            } catch (Throwable e) {
                return exitState(submissionUri,
                                 "failed",
                                 "Submission failed using Scheduler \"" + scheduler.getName()
                                           + "\"",
                                 uriHandler);
            }
        }
        return transition;

    }

    @Override
    public TransitionImpl exitState(final String item,
                                    final ProcessDefinition process,
                                    final String exit,
                                    final String message,
                                    final UriHandler uriHandler) throws AlreadyExitedException,
                                                                 ForbiddenTransitionException {
        final ProcessDefinitionImpl processDefinition = findProcessDefinition(process);
        if (null == processDefinition) {
            throw new IllegalArgumentException("There is no process definition named \"" + process
                                               + "\"");
        }
        final PairingImpl pairing = findPairing(item,
                                                processDefinition);
        final PairingImpl pairingToUse;
        if (null == pairing) {
            pairingToUse = exitManager.createPairing(item,
                                                     processDefinition,
                                                     processDefinition.getDefaultScheduler());
        } else {
            pairingToUse = pairing;
        }
        final TransitionImpl exitTransition = exitManager.exitState(pairingToUse,
                                                                    exit,
                                                                    message);
        Instrumentation instrumentationToUse = getInstrumentation();
        final Map<String, String> attributes = instrumentExit(pairingToUse,
                                                              exit,
                                                              instrumentationToUse);
        final TransitionImpl result = entryAction(exitTransition,
                                                  uriHandler,
                                                  false);
        instrumentEntry(result,
                        attributes,
                        instrumentationToUse);
        return result;
    }

    @Override
    public TransitionImpl exitState(final URI transitionUri,
                                    final String exit,
                                    final String message,
                                    final UriHandler uriHandler) throws AlreadyExitedException,
                                                                 ForbiddenTransitionException {
        LOG.debug("Entering \"exitState\" for " + transitionUri);
        final TransitionImpl lastTransition = entityManager.find(TransitionImpl.class,
                                                                 Integer.valueOf(uriHandler.getIdentity(transitionUri)));
        if (null == lastTransition) {
            throw new IllegalArgumentException("Unknown transition identity");
        }
        final TransitionImpl exitTransition = exitManager.exitState(lastTransition,
                                                                    exit,
                                                                    message);
        Instrumentation instrumentationToUse = getInstrumentation();
        final Map<String, String> attributes = instrumentExit(lastTransition.getPairingImpl(),
                                                              exit,
                                                              instrumentationToUse);
        final TransitionImpl result = entryAction(exitTransition,
                                                  uriHandler,
                                                  true);
        instrumentEntry(result,
                        attributes,
                        instrumentationToUse);
        LOG.debug("Exit \"exitState\" for " + transitionUri);
        return result;
    }

    /**
     * Returns the last transition take by the specified item/process pairing.
     *
     * @param item
     *            the identity of the item in this pairing.
     * @param process
     *            the process definition associated in this pairing.
     *
     * @return the last transition take by the specified item/process pairing.
     *
     * @throws IllegalArgumentException
     *             when the specified process is unknown.
     */
    private TransitionImpl findLastTransition(final String item,
                                              final ProcessDefinition process) throws IllegalArgumentException {
        final ProcessDefinitionImpl processDefinition = findProcessDefinition(process);
        if (null == processDefinition) {
            throw new IllegalArgumentException("There is no process definition named \"" + process
                                               + "\"");
        }
        PairingImpl pairing = findPairing(item,
                                          processDefinition);
        if (null == pairing) {
            return null;
        }
        return pairing.getLastTransition();
    }

    /**
     * Returns the {@link PairingImpl}, if any, that matches the supplied item and
     * process definition.
     *
     * @param item
     *            the identity of the item in this pairing.
     * @param process
     *            the process definition associated in this pairing.
     *
     * @return
     */
    private PairingImpl findPairing(final String item,
                                    final ProcessDefinition process) {
        final ProcessDefinitionImpl processDefinition = bindProcessDefinitionImpl(process);
        return findPairing(item,
                           processDefinition);
    }

    private PairingImpl findPairing(final String item,
                                    final ProcessDefinitionImpl processDefinition) {
        final TypedQuery<PairingImpl> query = entityManager.createNamedQuery("getPairing",
                                                                             PairingImpl.class);
        query.setParameter("item",
                           item);
        query.setParameter("definition",
                           processDefinition);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Returns the {@link PairingImpl}, if any, that matches the supplied item and
     * process definition.
     *
     * @param item
     *            the identity of the item in this pairing.
     * @param process
     *            the process definition associated in this pairing.
     *
     * @return
     */
    private List<PairingImpl> findPairings(final List<String> items,
                                           final ProcessDefinition process) {
        final ProcessDefinitionImpl processDefinition = bindProcessDefinitionImpl(process);
        return findPairings(items,
                            processDefinition);
    }

    private List<PairingImpl> findPairings(final List<String> items,
                                           final ProcessDefinitionImpl processDefinition) {
        if (null == items || items.isEmpty()) {
            return null;
        }
        final TypedQuery<PairingImpl> query = entityManager.createNamedQuery("getPairings",
                                                                             PairingImpl.class);
        query.setParameter("items",
                           items);
        query.setParameter("definition",
                           processDefinition);
        List<PairingImpl> result = query.getResultList();
        final Comparator<PairingImpl> comparator = new PairingComparator(items);
        result.sort(comparator);
        return result;
    }

    private ProcessDefinitionImpl findProcessDefinition(final ProcessDefinition process) throws NonUniqueResultException {
        final TypedQuery<ProcessDefinitionImpl> query = entityManager.createNamedQuery("getProcessDefinitionByFamilyName",
                                                                                       ProcessDefinitionImpl.class);
        query.setParameter("familyName",
                           (process.getFamily()).getName());
        query.setParameter("name",
                           process.getName());
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            final String familyName = (process.getFamily()).getName();
            final String versionName = process.getName();
            if ("Test".equals(familyName) && "-1".equals(versionName)) {
                // Loaded Test scheduler, family, and process definition.
                getFamilies();
                return findProcessDefinition(process);
            }
            return null;
        }
    }

    @Override
    public Set<String> getAllowedExits(final State state) {
        return StateMachine.getAllowedTransitions(state);
    }

    @Override
    public List<? extends Family> getFamilies() {
        final TypedQuery<FamilyImpl> query = entityManager.createNamedQuery("activeFamilies",
                                                                            FamilyImpl.class);
        final List<FamilyImpl> families = query.getResultList();
        if (!defaultsInstalled) {
            try {
                Utilities.defaultInstall();
                RabbitMQScheduler.defaultInstall();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (families.isEmpty()) {
            try {
                final FamilyImpl family = createFamily("Test",
                                                       "Test configurations");

                if (!defaultsInstalled) {
                    final SchedulerImpl scheduler = new NopScheduler("DoNothing",
                                                                     "Prints submission information to application log file");
                    entityManager.persist(scheduler);
                    final ProcessDefinition processDefinition = createProcessDefinition("-1",
                                                                                        "Executes the echo command",
                                                                                        family,
                                                                                        "echo",
                                                                                        null,
                                                                                        null,
                                                                                        "one two \"three four\"",
                                                                                        scheduler,
                                                                                        false);
                    family.setDefaultDefinition(processDefinition);
                    final SchedulerImpl scheduler2 = new BashScheduler("Internal",
                                                                       "Runs the command inside the server",
                                                                       "{0}",
                                                                       getBinDir() + File.separatorChar
                                                                              + "pp_python_runner");
                    entityManager.persist(scheduler2);
                    createProcessDefinition("-2",
                                            "Executes the test_* scripts",
                                            family,
                                            getBinDir() + File.separatorChar
                                                    + "test_processing.sh",
                                            getBinDir() + File.separatorChar
                                                                            + "test_success.sh",
                                            getBinDir() + File.separatorChar
                                                                                                 + "test_failure.sh",
                                            "one two \"three four\"",
                                            scheduler2,
                                            false);
                    final SchedulerImpl scheduler3 = new BashScheduler("Background",
                                                                       "Runs the command in background outside the server",
                                                                       getBinDir() + File.separatorChar
                                                                                                                            + "test_background.sh -o {1} -e {2} -m {3} {0}",
                                                                       getBinDir() + File.separatorChar
                                                                                                                                                                             + "pp_python_runner");
                    entityManager.persist(scheduler3);
                    final Path configPath = RabbitMQScheduler.getConfigFileName();
                    final File propertiesFile;
                    if (null == configPath) {
                        final String home = System.getProperty("user.home");
                        propertiesFile = new File(new File(new File(home,
                                                                    Utilities.DEFAULT_PSQUARED_DIRECTORY),
                                                           Utilities.DEFAULT_SERVER_DIRECTORY),
                                                  RabbitMQScheduler.DEFAULT_PROPERTIES_NAME);
                    } else {
                        propertiesFile = configPath.toFile();
                    }
                    final SchedulerImpl scheduler4 = new RabbitMQScheduler("RabbitMQ",
                                                                           "Publishes command to test RabbitMQ queue",
                                                                           propertiesFile.toString(),
                                                                           "psquared_test");
                    entityManager.persist(scheduler4);
                    defaultsInstalled = true;
                }

            } catch (ExistingEntityException e) {
                // If you get here there is a deactivate Test family so do
                // nothing
            }
            return getFamilies();
        }
        return families;
    }

    @Override
    public FamilyImpl getFamily(final String identity) {
        final FamilyImpl family = entityManager.find(FamilyImpl.class,
                                                     Integer.parseInt(identity));
        return family;
    }

    private Instrumentation getInstrumentation() {
        if (null == instrumentation) {
            instrumentation = Instrumentation.getInstrumentation(getClass().getSimpleName());
        }
        return instrumentation;
    }

    @Override
    public Transition getLatestTransition(Family family) {
        final List<ProcessDefinitionImpl> familyDefinitions = getProcessDefinitionImpls((FamilyImpl) family);

        if (null == familyDefinitions || familyDefinitions.isEmpty()) {
            return null;
        }
        return getLatestTransition(familyDefinitions);
    }

    private Transition getLatestTransition(List<? extends ProcessDefinition> deinitions) {
        final TypedQuery<Date> query = entityManager.createNamedQuery("whenLatestTransition",
                                                                      Date.class);
        query.setParameter("processes",
                           deinitions);
        try {
            final Date when = query.getSingleResult();
            final TypedQuery<TransitionImpl> query2 = entityManager.createNamedQuery("getLatestTransitionsByTime",
                                                                                     TransitionImpl.class);
            query2.setParameter("when",
                                when);
            query2.setParameter("processes",
                                deinitions);
            final List<? extends Transition> transitions = query2.getResultList();
            if (null == transitions || transitions.isEmpty()) {
                return null;
            }
            return transitions.get(0);
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Transition getLatestTransition(ProcessDefinition processDefinition) {
        if (null == processDefinition) {
            return null;
        }
        List<ProcessDefinition> definition = new ArrayList<ProcessDefinition>(1);
        definition.add(processDefinition);
        return getLatestTransition(definition);
    }

    @Override
    public List<? extends Transition> getLatestTransitions(List<String> items,
                                                           ProcessDefinition process) {
        final List<PairingImpl> pairings = findPairings(items,
                                                        process);
        if (null == pairings || 0 == pairings.size()) {
            return NO_TRANSITIONS;
        }
        final List<TransitionImpl> results = new ArrayList<>();
        for (PairingImpl pairing : pairings) {
            final List<TransitionImpl> transitions = pairing.getTransitions();
            if (transitions.isEmpty()) {
                throw new IllegalStateException("Pairing# \"" + pairing.getPairingKey()
                                                + "\" for item \""
                                                + pairing.getItem()
                                                + "\" has no current transition!");
            }
            results.add(transitions.get(0));
        }
        return results;
    }

    @Override
    public List<? extends Transition> getMatchLatestTransitions(List<String> items,
                                                                ProcessDefinition process,
                                                                String state) {
        final StateImpl stateImpl = getState(state);
        if (null == stateImpl) {
            return NO_TRANSITIONS;
        }
        final TypedQuery<TransitionImpl> query;
        query = entityManager.createNamedQuery("selectedLatestTransitionsRequiredState",
                                               TransitionImpl.class);
        query.setParameter("state",
                           stateImpl);
        query.setParameter("process",
                           process);
        query.setParameter("items",
                           items);
        return query.getResultList();
    }

    @Override
    public List<? extends Transition> getMatchLatestTransitions(final ProcessDefinition process,
                                                                final String state,
                                                                final Integer page,
                                                                final Integer length) {
        final int begin;
        final boolean paged;
        if (null == page || 0 > page
            || null == length
            || 0 >= length) {
            begin = 0;
            paged = false;
        } else {
            begin = page * length;
            paged = true;
        }

        final StateImpl stateImpl = getState(state);
        if (null == stateImpl) {
            return NO_TRANSITIONS;
        }
        final TypedQuery<TransitionImpl> query;
        query = entityManager.createNamedQuery("latestTransitionsRequiredState",
                                               TransitionImpl.class);
        query.setParameter("state",
                           stateImpl);
        query.setParameter("process",
                           process);
        query.setFirstResult(begin);
        if (paged) {
            query.setMaxResults(length);
        }
        return query.getResultList();
    }

    @Override
    public ProcessDefinition getProcessDefinition(final String identity) {
        final ProcessDefinitionImpl definition = entityManager.find(ProcessDefinitionImpl.class,
                                                                    Integer.parseInt(identity));
        return definition;
    }

    private List<ProcessDefinitionImpl> getProcessDefinitionImpls(FamilyImpl family) {
        final TypedQuery<ProcessDefinitionImpl> query = entityManager.createNamedQuery("getProcessDefinitionsByFamily",
                                                                                       ProcessDefinitionImpl.class);
        query.setParameter("family",
                           family);
        return query.getResultList();
    }

    @Override
    public SchedulerImpl getScheduler(final String identity) {
        final SchedulerImpl scheduler = entityManager.find(SchedulerImpl.class,
                                                           Integer.parseInt(identity));
        return scheduler;
    }

    @Override
    public List<? extends KnownScheduler> getSchedulers() {
        final TypedQuery<SchedulerImpl> query = entityManager.createNamedQuery("activeSchedulers",
                                                                               SchedulerImpl.class);
        final List<SchedulerImpl> schedulers = query.getResultList();
        if (schedulers.isEmpty()) {
            // Loaded Test scheduler, family, and process definition.
            getFamilies();
            return getSchedulers();
        }
        return schedulers;
    }

    private StateImpl getState(final String name) {
        final TypedQuery<StateImpl> query = entityManager.createNamedQuery("getState",
                                                                           StateImpl.class);
        query.setParameter("name",
                           name);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public State getState(final String item,
                          final ProcessDefinition process) throws IllegalArgumentException {
        final TransitionImpl lastTransition = findLastTransition(item,
                                                                 process);
        if (null == lastTransition) {
            return State.READY;
        }
        return lastTransition.getState();
    }

    private List<StateImpl> getStates(final List<String> names) {
        if (null == names) {
            return NO_STATES;
        }
        final TypedQuery<StateImpl> query = entityManager.createNamedQuery("getStates",
                                                                           StateImpl.class);
        query.setParameter("names",
                           names);
        return query.getResultList();
    }

    @Override
    public List<? extends Transition> getStateSince(final String family,
                                                    final List<String> states,
                                                    final Date since,
                                                    final Date before,
                                                    final int maxCount) {
        final List<StateImpl> statesImpl = getStates(states);
        final TypedQuery<TransitionImpl> query;
        if (null == before) {
            query = entityManager.createNamedQuery("transitionedSince",
                                                   TransitionImpl.class);
        } else {
            query = entityManager.createNamedQuery("transitionedBetween",
                                                   TransitionImpl.class);
            query.setParameter("before",
                               before);
        }
        query.setParameter("since",
                           since);
        query.setParameter("states",
                           statesImpl);
        query.setParameter("family",
                           family);
        if (maxCount > 0) {
            query.setMaxResults(maxCount);
        }
        return query.getResultList();
    }

    @Override
    public Transition getTransition(final String identity) {
        return entityManager.find(TransitionImpl.class,
                                  Integer.parseInt(identity));
    }

    @Override
    public List<? extends Transition> getTransitions(final String identity) {
        final TransitionImpl transition = entityManager.find(TransitionImpl.class,
                                                             Integer.parseInt(identity));
        return (transition.getPairingImpl()).getTransitions();
    }

    @Override
    public List<? extends Transition> getTransitions(final String item,
                                                     final ProcessDefinition process) {
        final PairingImpl pairing = findPairing(item,
                                                process);
        if (null == pairing) {
            return new ArrayList<>();
        }
        return pairing.getTransitions();
    }

    @Override
    public List<? extends Transition> getUnmatchLatestTransitions(final List<String> items,
                                                                  final ProcessDefinition process,
                                                                  final List<String> states) {
        final List<StateImpl> unprocessedStates = getStates(states);
        final TypedQuery<TransitionImpl> query;
        if (unprocessedStates.isEmpty()) {
            query = entityManager.createNamedQuery("selectedLatestTransitions",
                                                   TransitionImpl.class);
        } else {
            query = entityManager.createNamedQuery("selectedLatestTransitionsExcludingStates",
                                                   TransitionImpl.class);
            query.setParameter("states",
                               unprocessedStates);
        }
        query.setParameter("process",
                           process);
        query.setParameter("items",
                           items);
        return query.getResultList();
    }

    @Override
    public List<? extends Transition> getUnmatchLatestTransitions(ProcessDefinition process,
                                                                  List<String> states,
                                                                  Integer page,
                                                                  Integer length) {
        final int begin;
        if (null == page || null == length) {
            begin = 0;
        } else {
            begin = page * length;
        }
        final int end;
        if (null == page || null == length) {
            end = -1;
        } else {
            end = begin + length;
        }

        final List<StateImpl> unprocessedStates = getStates(states);
        final TypedQuery<TransitionImpl> query;
        if (unprocessedStates.isEmpty()) {
            query = entityManager.createNamedQuery("latestTransitions",
                                                   TransitionImpl.class);
        } else {
            query = entityManager.createNamedQuery("latestTransitionsExcludingStates",
                                                   TransitionImpl.class);
            query.setParameter("states",
                               unprocessedStates);
        }
        query.setParameter("process",
                           process);
        query.setFirstResult(begin);
        if (-1 != end) {
            query.setMaxResults(end);
        }
        return query.getResultList();
    }

    /**
     * Returns true if there already exists one or more active {@link PairingImpl}
     * instances for the specified item paired with any
     * {@link ProcessDefinitionImpl} instance that belongs to the {@link FamilyImpl}
     * of the supplied {@link ProcessDefinitionImpl} but is not that definition.
     *
     * @param item
     *            the item whose pairings should be searched
     * @param processDefinition
     *            the {@link ProcessDefinitionImpl} that specifies the
     *            {@link FamilyImpl} to be matched.
     */
    private boolean isFamilyVetoed(final String item,
                                   final ProcessDefinitionImpl processDefinition) {
        final List<StateImpl> nonActiveStates = getStates(NON_ACTIVE_STATES);
        final List<ProcessDefinitionImpl> familyDefinitions = getProcessDefinitionImpls(processDefinition.getFamilyImpl());

        final TypedQuery<PairingImpl> query = entityManager.createNamedQuery("getActiveFamilyPairings",
                                                                             PairingImpl.class);
        query.setParameter("item",
                           item);
        query.setParameter("processes",
                           familyDefinitions);
        query.setParameter("states",
                           nonActiveStates);
        List<PairingImpl> pairings = query.getResultList();
        if (null == pairings || pairings.isEmpty()) {
            return false;
        }
        for (PairingImpl pairing : pairings) {
            if (!(processDefinition.getProcessDefinitionKey() == (pairing.getProcessDefinition()).getProcessDefinitionKey())) {
                return true;
            }
        }
        return false;
    }

    // static member methods (alphabetic)

    @Override
    public URI mapBaseUri(final URI uri) {
        LOG.info("Entering \"mapBaseUri\" for " + uri);
        final TypedQuery<String> query = entityManager.createNamedQuery("getMappedBase",
                                                                        String.class);
        query.setParameter("key",
                           uri.toString());
        try {
            return new URI(query.getSingleResult());
        } catch (NoResultException e) {
            // No problem, just return null.
            return null;
        } catch (NonUniqueResultException e) {
            LOG.error("Multiple matches to base URI \"" + uri
                      + "\", so all are ignored");
            return null;
        } catch (URISyntaxException e) {
            LOG.error("URI mapped to \"" + uri
                      + "\" is syntactically incorrect.");
            return null;
        } finally {
            LOG.debug("Exiting \"mapBaseUri\" for " + uri);
        }
    }

    @Override
    public Transition submitPairing(final String item,
                                    final ProcessDefinition process,
                                    final String message,
                                    final String veto,
                                    final KnownScheduler scheduler,
                                    final UriHandler uriHandler) throws AlreadyExitedException,
                                                                 ForbiddenTransitionException,
                                                                 VetoedException {
        LOG.debug("Entering \"submitPairing\" for \"" + item
                  + "\"");
        final ProcessDefinitionImpl processDefinition = findProcessDefinition(process);
        if (null == processDefinition) {
            throw new IllegalArgumentException("There is no version \"" + process.getName()
                                               + "\" of process definition  \""
                                               + (process.getFamily()).getName()
                                               + "\"");
        }
        if (FAMILY_VETO.equals(veto) && isFamilyVetoed(item,
                                                       processDefinition)) {
            throw new VetoedException("Vetoed by 'family' veto");
        }
        final PairingImpl pairing = findPairing(item,
                                                processDefinition);
        final PairingImpl pairingToUse;
        if (null == pairing) {
            pairingToUse = exitManager.createPairing(item,
                                                     processDefinition,
                                                     scheduler);
        } else {
            if (null != scheduler) {
                pairing.setScheduler((SchedulerImpl) scheduler);
            }
            pairingToUse = pairing;
        }
        final TransitionImpl result = entryAction(exitManager.exitState(pairingToUse,
                                                                        "submit",
                                                                        message),
                                                  uriHandler,
                                                  false);
        Instrumentation instrumentationToUse = getInstrumentation();
        instrumentEntry(result,
                        prepareInstrumentation(pairingToUse,
                                               instrumentationToUse),
                        instrumentationToUse);
        LOG.debug("Exiting \"submitPairing\" for \"" + item
                  + "\"");
        return result;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
