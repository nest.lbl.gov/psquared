package gov.lbl.nest.psquared.ejb;

import java.util.List;

import gov.lbl.nest.psquared.Family;
import gov.lbl.nest.psquared.ProcessDefinition;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

/**
 * This class implements the {@link Family} interface using JPA.
 * 
 * @author patton
 */
@Entity
@Table(name = "Family2")
@NamedQueries({ @NamedQuery(name = "activeFamilies",
                            query = "SELECT f " + " FROM FamilyImpl f"
                                    + " WHERE active = true"
                                    + " ORDER BY name"),
                @NamedQuery(name = "getActiveFamilyByName",
                            query = "SELECT f " + " FROM FamilyImpl f"
                                    + " WHERE name = :name"
                                    + " AND active = true"),
                @NamedQuery(name = "isExistingFamily",
                            query = "SELECT 0 != count(f) " + " FROM FamilyImpl f"
                                    + " WHERE f.name = :name") })
public class FamilyImpl implements
                        Family {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * True when items can be processed with this definition.
     */
    private boolean active;

    /**
     * The {@link ProcessDefinition} instance to use is none is explicitly
     * requested.
     */
    private ProcessDefinition defaultDefinition;

    /**
     * The description of this family.
     */
    private String description;

    /**
     * The key, within the {@link FamilyImpl} class, that identifies this instance.
     */
    private int familyImplKey;

    /**
     * The name of this object within the application.
     */
    private String name;

    /**
     * The {@link ProcessDefinition} instances that belong to this family.
     */
    private List<ProcessDefinitionImpl> processDefinitions;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected FamilyImpl() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param name
     *            the name of this object within the application.
     * @param description
     *            the description of this family.
     */
    FamilyImpl(String name,
               String description) {
        setActive(true);
        setDescription(description);
        setName(name);
    }

    // instance member method (alphabetic)

    @Override
    @OneToOne(targetEntity = ProcessDefinitionImpl.class)
    public ProcessDefinition getDefaultDefinition() {
        return defaultDefinition;
    }

    @Override
    public String getDescription() {
        return description;
    }

    /**
     * Returns the key, within the {@link FamilyImpl} class, that identifies this
     * instance.
     * 
     * @return the key, within the {@link FamilyImpl} class, that identifies this
     *         instance.
     */
    @Id
    @GeneratedValue
    protected int getFamilyKey() {
        return familyImplKey;
    }

    @Override
    @Transient
    public String getIdentity() {
        return Integer.toString(getFamilyKey());
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    @Transient
    public List<? extends ProcessDefinition> getProcessDefinitions() {
        return getProcessDefinitionsImpl();
    }

    /**
     * Returns the {@link ProcessDefinition} instances that belong to this family.
     * 
     * @return the {@link ProcessDefinition} instances that belong to this family.
     */
    @OneToMany(mappedBy = "familyImpl")
    @OrderBy("ordinal ASC")
    protected List<ProcessDefinitionImpl> getProcessDefinitionsImpl() {
        return processDefinitions;
    }

    /**
     * Returns true when items can be processed with this definition.
     * 
     * @return true when items can be processed with this definition.
     */
    @Override
    public boolean isActive() {
        return active;
    }

    /**
     * Sets whether items can be processed with this definition or not.
     * 
     * @param active
     *            true when items can be processed with this definition.
     */
    protected void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Sets the {@link ProcessDefinition} instance to use is none is explicitly
     * requested.
     * 
     * @param definition
     *            the {@link ProcessDefinition} instance to use is none is
     *            explicitly requested.
     */
    public void setDefaultDefinition(ProcessDefinition definition) {
        defaultDefinition = definition;
    }

    /**
     * Sets the description of this family.
     * 
     * @param description
     *            the description of this family.
     */
    protected void setDescription(String description) {
        this.description = description;
    }

    /**
     * Sets the key, within the {@link ProcessDefinitionImpl} class, that identifies
     * this instance.
     * 
     * @param key
     *            the value of the key to set.
     */
    protected void setFamilyKey(int key) {
        familyImplKey = key;
    }

    /**
     * Sets the name of this object within the application.
     * 
     * @param name
     *            the name of this object within the application.
     */
    protected void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the {@link ProcessDefinition} instances that belong to this family.
     * 
     * @param definitions
     *            the {@link ProcessDefinition} instances that belong to this
     *            family.
     */
    protected void setProcessDefinitionsImpl(List<ProcessDefinitionImpl> definitions) {
        processDefinitions = definitions;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
