/**
 * The package provides the main classes that implements PSquare's
 * responsibilities.
 * 
 * @author patton
 */
package gov.lbl.nest.psquared;