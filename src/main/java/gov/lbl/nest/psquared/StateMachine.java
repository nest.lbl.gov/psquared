package gov.lbl.nest.psquared;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * This class defines the state machine that this application is implementing.
 * 
 * @author patton
 */
public class StateMachine {

    /**
     * The class captures the allowed transitions out of a state.
     * 
     * @author patton
     */
    public class AllowedTransition {

        /**
         * The name of this transition.
         */
        public final String name;

        /**
         * The target state this transition.
         */
        public final State target;

        /**
         * Creates an instance of this class.
         * 
         * @param name
         *            the name of this transition.
         * @param target
         *            the target state this transition.
         */
        private AllowedTransition(String name,
                                  State target) {
            this.name = name;
            this.target = target;
        }
    }

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    /**
     * The sets of allowed transitions of a each state.
     */
    private static final Map<State, Map<String, State>> STATE_MACHINE = new HashMap<State, Map<String, State>>();

    static {
        Map<String, State> exits = new HashMap<String, State>();
        exits.put("submit",
                  State.SUBMITTED);
        STATE_MACHINE.put(State.READY,
                          exits);

        exits = new HashMap<String, State>();
        exits.put("cancel",
                  State.READY);
        exits.put("executing",
                  State.EXECUTING);
        exits.put("failed",
                  State.FAILED);
        STATE_MACHINE.put(State.SUBMITTED,
                          exits);

        exits = new HashMap<String, State>();
        exits.put("cancel",
                  State.READY);
        exits.put("processed",
                  State.PROCESSED);
        exits.put("failed",
                  State.FAILED);
        exits.put("reschedule",
                  State.SUBMITTED);
        STATE_MACHINE.put(State.EXECUTING,
                          exits);

        exits = new HashMap<String, State>();
        exits.put("reset",
                  State.READY);
        STATE_MACHINE.put(State.PROCESSED,
                          exits);

        exits = new HashMap<String, State>();
        exits.put("resolved",
                  State.READY);
        exits.put("abandon",
                  State.ABANDONED);
        STATE_MACHINE.put(State.FAILED,
                          exits);

        exits = new HashMap<String, State>();
        exits.put("recover",
                  State.READY);
        STATE_MACHINE.put(State.ABANDONED,
                          exits);
    }

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    /**
     * Returns the set of allowed transitions out of the specified state.
     * 
     * @param state
     *            the state whose outbound transitions should be returned.
     * 
     * @return the set of allowed transitions out of the specified state.
     */
    public static Set<String> getAllowedTransitions(State state) {
        return (STATE_MACHINE.get(state)).keySet();
    }

    /**
     * Returns the target state for the specified exit of the supplied {@link State}
     * instance.
     * 
     * @param origin
     *            the {@link State} instance from which the target state should be
     *            returned.
     * @param name
     *            the name of the exit transition.
     * 
     * @return the target state for the specified exit of the supplied {@link State}
     *         instance.
     */
    public static State getTarget(State origin,
                                  String name) {
        final Map<String, State> exits = STATE_MACHINE.get(origin);
        return exits.get(name);
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
