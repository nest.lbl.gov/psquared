package gov.lbl.nest.psquared;

import java.net.URI;

/**
 * This interface is used to handle URIs for a RESTful interface
 * 
 * @author patton
 */
public interface UriHandler {

    /**
     * Builds the URI to used for the identified realized state.
     * 
     * @param identity
     *            the identity of realized state whose URI should be returned.
     * 
     * @return the URI to used for the identified realized state.
     */
    URI buildFromIdentity(String identity);

    /**
     * Returns the identity of the realized state reference by the supplied URI.
     * 
     * @param uri
     *            the URI of realized state whose identity should be returned.
     * 
     * @return the identity of the realized state reference by the supplied URI.
     */
    String getIdentity(URI uri);

    /**
     * Constructs a new URI by parsing the given string and then resolving it
     * against this objects base.
     * 
     * @param str
     *            the string to be parsed into a URI
     * 
     * @return the resulting URI.
     */
    URI resolve(String str);
}
