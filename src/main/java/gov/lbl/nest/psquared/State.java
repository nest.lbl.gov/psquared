package gov.lbl.nest.psquared;

import jakarta.xml.bind.annotation.XmlEnum;

/**
 * This enumerates all possible states of a item/process pairings.
 * 
 * @author patton
 */
@XmlEnum
public enum State {

                   /**
                    * The object is ready of processing.
                    */
                   READY,

                   /**
                    * A portion of the object is {@link #PROCESSING}, but another portion is
                    * neither {@link #PROCESSING} nor {@link #PROCESSED} so the entire object will
                    * eventually become incomplete unless circumstances change..
                    */
                   FALTERING,

                   /**
                    * The object has been submitted for processing, but is not yet executing.
                    */
                   SUBMITTED,

                   /**
                    * The object is currently being executing.
                    */
                   EXECUTING,

                   /**
                    * The object is currently being processed. This is a super-state of
                    * {@link #SUBMITTED} and {@link #EXECUTING}
                    */
                   PROCESSING,

                   /**
                    * The object has been successfully processed.
                    */
                   PROCESSED,

                   /**
                    * The object is {@link #PROCESSING}, but it will not complete successfully as a
                    * portion of that object has already {@link #FAILED}.
                    */
                   FAILING,

                   /**
                    * The object is no longer processing but a portion or all of it has failed to
                    * be processed successfully.
                    */
                   FAILED,

                   /**
                    * The object is not currently being processed but a portion of that is
                    * {@link #READY} and not {@link #PROCESSED}.
                    */
                   INCOMPLETE,

                   /**
                    * Processing of the object has been abandoned.
                    */
                   ABANDONED,

                   /**
                    * Submission of the object was been vetoed.
                    */
                   VETOED;
}
