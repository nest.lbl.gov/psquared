package gov.lbl.nest.psquared.execute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.execution.DeployedFiles;
import gov.lbl.nest.common.execution.DeployedFiles.DeployedFile;
import gov.lbl.nest.jee.monitor.InstrumentationDB;
import gov.lbl.nest.jee.watching.WatcherDB;
import gov.lbl.nest.psquared.ImplementationString;
import gov.lbl.nest.psquared.ejb.PSquaredDB;
import jakarta.ejb.Singleton;
import jakarta.enterprise.inject.Produces;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

/**
 * This class provides resources to be injected into the PSquared application.
 * 
 * @author patton
 */
@Singleton
public class ResourcesForPSquared {

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ResourcesForPSquared.class);

    /**
     * Expose an entity manager using the resource producer pattern
     */
    @PersistenceContext(unitName = "psquared")
    @Produces
    @InstrumentationDB
    @PSquaredDB
    @WatcherDB
    private static EntityManager entityManager;

    // private static member data

    // private instance member data

    /**
     * The version String containing this project's release.
     */
    private static String version;

    /**
     * Returns the version String containing this project's release.
     * 
     * @return the version String containing this project's release.
     */
    private static synchronized String getDeployedVersion() {
        if (null == version) {
            DeployedFile deployedFile = DeployedFiles.getDeployedWarFile();
            LOG.info("Using version \"" + deployedFile.version
                     + "\" of artifact \""
                     + deployedFile.project
                     + "\"");
            version = deployedFile.version;
        }
        return version;
    }

    /**
     * Creates an instance of this class.
     */
    public ResourcesForPSquared() {
        getDeployedVersion();
    }

    /**
     * Returns the implementation version.
     * 
     * @return the implementation version.
     */
    @Produces
    @ImplementationString
    public String getImplementationString() {
        getDeployedVersion();
        return version;
    }
}
