package gov.lbl.nest.psquared;

/**
 * This exception reports when an requested transition is not allowed because of
 * the current state of an item/processing pair.
 * 
 * @author patton
 */
public class ForbiddenTransitionException extends
                                          PSquaredException {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by the Serialized mechanism.
     */
    private static final long serialVersionUID = 6258011321200490925L;

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     */
    public ForbiddenTransitionException() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param message
     *            the detailed message.
     */
    public ForbiddenTransitionException(String message) {
        super(message);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param message
     *            the detailed message.
     * @param transition
     *            the {@link Transition} that created the state whose requested exit
     *            transition is forbidden.
     */
    public ForbiddenTransitionException(String message,
                                        Transition transition) {
        super(message,
              transition);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param transition
     *            the {@link Transition} that created the state whose requested exit
     *            transition is forbidden.
     */
    public ForbiddenTransitionException(Transition transition) {
        super(transition);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
