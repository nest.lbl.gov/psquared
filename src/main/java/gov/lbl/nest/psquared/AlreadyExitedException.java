package gov.lbl.nest.psquared;

/**
 * This exception reports a request to transition out of a realized state that
 * has already been exited.
 * 
 * @author patton
 */
public class AlreadyExitedException extends
                                    PSquaredException {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by the Serialized mechanism.
     */
    private static final long serialVersionUID = 5979514347091878903L;

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     */
    public AlreadyExitedException() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param message
     *            the detailed message.
     */
    public AlreadyExitedException(String message) {
        super(message);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param message
     *            the detailed message.
     * @param transition
     *            the latest {@link Transition} from this item/process pairing.
     */
    public AlreadyExitedException(String message,
                                  Transition transition) {
        super(message,
              transition);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param transition
     *            the latest {@link Transition} from this item/process pairing.
     */
    public AlreadyExitedException(Transition transition) {
        super(transition);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
