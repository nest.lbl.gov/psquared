package gov.lbl.nest.psquared;

/**
 * This exception reports when an requested transition is not allowed because of
 * the current state of an item/processing pair.
 * 
 * @author patton
 */
public class PSquaredException extends
                               Exception {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by the Serialized mechanism.
     */
    private static final long serialVersionUID = 4197927362663720315L;

    // private static member data

    // private instance member data

    /**
     * The {@link Transition} instance to return from this object.
     */
    private Transition transition;

    // constructors

    /**
     * Creates an instance of this class.
     */
    public PSquaredException() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param message
     *            the detailed message.
     */
    public PSquaredException(String message) {
        super(message);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param message
     *            the detailed message.
     * @param transition
     *            the {@link Transition} instance to return from this object.
     */
    public PSquaredException(String message,
                             Transition transition) {
        super(message);
        this.transition = transition;
    }

    /**
     * Creates an instance of this class.
     * 
     * @param transition
     *            the {@link Transition} instance to return from this object.
     */
    public PSquaredException(Transition transition) {
        this.transition = transition;
    }

    // instance member method (alphabetic)

    /**
     * Returns the {@link Transition} instance to return from this object.
     * 
     * @return the {@link Transition} instance to return from this object.
     */
    public Transition getTransition() {
        return transition;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
