package gov.lbl.nest.psquared;

/**
 * This error reports that the state machine configuration is inconsistent.
 * 
 * @author patton
 */
public class StateMachineError extends
                               Error {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by the Serialized mechanism.
     */
    private static final long serialVersionUID = -3711925301395518025L;

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     */
    public StateMachineError() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param message
     *            the detailed message.
     */
    public StateMachineError(String message) {
        super(message);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
