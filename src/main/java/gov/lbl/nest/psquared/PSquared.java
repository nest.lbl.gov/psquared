package gov.lbl.nest.psquared;

import java.net.URI;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * This interface defines the methods that a {@link PSquared} application needs
 * in implement.
 *
 * @author patton
 *
 */
public interface PSquared {

    /**
     * The version of the specification that this library implements.
     */
    static final String SPECIFICATION = "0.2";

    /**
     * Creates a new {@link Family} instance.
     *
     * @param name
     *            the name of this family within the application.
     * @param description
     *            the description of this family.
     *
     * @return the created {@link Family} instance.
     *
     * @throws ExistingEntityException
     *             when the family already exists.
     */
    Family createFamily(final String name,
                        final String description) throws ExistingEntityException;

    /**
     * Creates a new {@link ProcessDefinition} instance.
     *
     * @param name
     *            the name of this object within the family of definitions.
     * @param description
     *            this object within the family of definitions.
     * @param familyUri
     *            the URI of the family of definition to which this instance
     *            belongs.
     * @param processCmd
     *            the command that will be run to process an item.
     * @param successCmd
     *            the command the will be run after processing has succeeded.
     * @param failureCmd
     *            the command the will be run after processing has failed.
     * @param args
     *            the {@link MessageFormat} string that will be command's arguments.
     * @param schedulerUri
     *            the URI, if any, of the scheduler to to use when executing this
     *            definition and none is specified.
     * @param development
     *            True when this definition is used during process development,
     *            which it to say that it is mutable.
     * @param uriBuilder
     *            the {@link UriHandler} used to create realized state URIs.
     *
     * @return the created {@link ProcessDefinition} instance.
     *
     * @throws ExistingEntityException
     *             when the process definition already exists in that supplied
     *             family
     */
    ProcessDefinition createProcessDefinition(final String name,
                                              final String description,
                                              final URI familyUri,
                                              final String processCmd,
                                              final String successCmd,
                                              final String failureCmd,
                                              final String args,
                                              final URI schedulerUri,
                                              final boolean development,
                                              final UriHandler uriBuilder) throws ExistingEntityException;

    /**
     * Attempts the transition the pairing of a item and process definition from one
     * state to another.
     *
     * @param item
     *            the identity of the item in this pairing.
     * @param process
     *            the process definition associated in this pairing.
     * @param exit
     *            the name of the exit to take out of the state that resulted from
     *            the previously completed transition.
     * @param message
     *            a message, if any, to associate with the new transition if it
     *            completes.
     * @param uriBuilder
     *            the {@link UriHandler} used to create realized state URIs.
     *
     * @return the completed transaction, <code>null</code> otherwise.
     *
     * @throws AlreadyExitedException
     *             when the current state was exited by a different thread before
     *             this one could exit.
     * @throws ForbiddenTransitionException
     *             when the current state does not allow the requested exit.
     */
    Transition exitState(String item,
                         ProcessDefinition process,
                         String exit,
                         String message,
                         UriHandler uriBuilder) throws AlreadyExitedException,
                                                ForbiddenTransitionException;

    /**
     * Attempts the transition the pairing of a item and process definition from one
     * state to another.
     *
     * @param transitionUri
     *            the URI of the previously completed transition for the
     *            item/configuration pairing.
     * @param exit
     *            the name of the exit to take out of the state that resulted from
     *            the previously completed transition.
     * @param message
     *            a message, if any, to associate with the new transition if it
     *            completes.
     * @param uriBuilder
     *            the {@link UriHandler} used to create realized state URIs.
     *
     * @return the completed transaction, <code>null</code> otherwise.
     *
     * @throws AlreadyExitedException
     *             when the specified transition did not result in current state.
     * @throws ForbiddenTransitionException
     *             when the state resulting from the specified transition does not
     *             allow the requested exit.
     */
    Transition exitState(URI transitionUri,
                         String exit,
                         String message,
                         UriHandler uriBuilder) throws AlreadyExitedException,
                                                ForbiddenTransitionException;

    /**
     * Returns the set of allowed exits from the specified state.
     *
     * @param state
     *            the state whose exits should be returned.
     *
     * @return the set of allowed exits from the specified state.
     */
    Set<String> getAllowedExits(State state);

    /**
     * Returns the collection of all {@link Family} instances contained in this
     * application.
     *
     * @return the collection of all {@link Family} instances contained in this
     *         application.
     */
    List<? extends Family> getFamilies();

    /**
     * Returns the {@link Family} instance identified by the supplied identity.
     *
     * @param identity
     *            the identity of the {@link Family} instance to return.
     *
     * @return the {@link Family} instance identified by the supplied identity.
     */
    Family getFamily(String identity);

    /**
     * Returns the most recent {@link Transition} instance associated with the
     * supplied {@link Family} instance.
     *
     * @param family
     *            the {@link Family} instance whose most resent transition should be
     *            returned.
     *
     * @return the most recent {@link Transition} instance associated with the
     *         supplied {@link Family} instance.
     */
    Transition getLatestTransition(Family family);

    /**
     * Returns the most recent {@link Transition} instance associated with the
     * supplied {@link ProcessDefinition} instance.
     *
     * @param processDefinition
     *            the {@link ProcessDefinition} instance whose most resent
     *            transition should be returned.
     *
     * @return the most recent {@link Transition} instance associated with the
     *         supplied {@link ProcessDefinition} instance.
     */
    Transition getLatestTransition(ProcessDefinition processDefinition);

    /**
     * Returns the most recent transition, if any, of the selected items/process
     * pairing.
     *
     * @param items
     *            the identity of the items whose latest transition should be
     *            returned.
     * @param process
     *            the process definition for which latest transition should be
     *            returned.
     *
     * @return the most recent transition, if any, of a item/process pairing.
     */
    List<? extends Transition> getLatestTransitions(List<String> items,
                                                    ProcessDefinition process);

    /**
     * Returns the collection of the more recent {@link Transition} instances for
     * all items whose latest state, for the supplied process, matches the specified
     * one.
     *
     * @param items
     *            the identity of the items whose matched latest transition should
     *            be returned.
     * @param process
     *            the process definition whose items should be returned.
     * @param state
     *            the state that the latest state of an item should match to be
     *            included in the result.
     *
     * @return the collection of the more recent {@link Transition} instances for
     *         all items whose latest state, for the supplied process, matches the
     *         specified one.
     */
    List<? extends Transition> getMatchLatestTransitions(List<String> items,
                                                         ProcessDefinition process,
                                                         String state);

    /**
     * Returns the collection of the more recent {@link Transition} instances for
     * all items whose latest state, for the supplied process, matches the specified
     * one.
     *
     * @param process
     *            the process definition whose items should be returned.
     * @param state
     *            the state that the latest state of an item should match to be
     *            included in the result.
     * @param page
     *            the page, of specified length, from the total collection, to
     *            return.
     * @param length
     *            the length of a page, in number of items.
     *
     * @return the collection of the more recent {@link Transition} instances for
     *         all items whose latest state, for the supplied process, matches the
     *         specified one.
     */
    List<? extends Transition> getMatchLatestTransitions(ProcessDefinition process,
                                                         String state,
                                                         Integer page,
                                                         Integer length);

    /**
     * Returns the {@link ProcessDefinition} instance identified by the supplied
     * identity.
     *
     * @param identity
     *            the identity of the {@link ProcessDefinition} instance to return.
     *
     * @return the {@link ProcessDefinition} instance identified by the supplied
     *         identity.
     */
    ProcessDefinition getProcessDefinition(String identity);

    /**
     * Returns the {@link KnownScheduler} instance identified by the supplied
     * identity.
     *
     * @param identity
     *            the identity of the {@link KnownScheduler} instance to return.
     *
     * @return the {@link KnownScheduler} instance identified by the supplied
     *         identity.
     */
    KnownScheduler getScheduler(String identity);

    /**
     * Returns the collection of all {@link KnownScheduler} instances contained in
     * this application.
     *
     * @return the collection of all {@link KnownScheduler} instances contained in
     *         this application.
     */
    List<? extends KnownScheduler> getSchedulers();

    /**
     * Returns the current state of the supplied item/process pairing.
     *
     * @param item
     *            the identity of the item in this pairing.
     * @param process
     *            the process definition associated in this pairing.
     *
     * @return the current state of the supplied ite/process pairing.
     *
     * @throws IllegalArgumentException
     *             when the specified process is unknown.
     */
    State getState(String item,
                   ProcessDefinition process) throws IllegalArgumentException;

    /**
     * Returns the list of items that are in supplied state and reached in on or
     * since the specified time.
     *
     * @param family
     *            the name of the family whose files should be returned.
     * @param states
     *            the collection of states in which the returned items should be in.
     * @param since
     *            the time on or after which the supplied state was reached.
     * @param before
     *            the time before which the supplied state was reached.
     * @param maxCount
     *            the maximum number of files to return, 0 is unlimited.
     *
     * @return the list of items that are in supplied state and reached in on or
     *         since the specified time.
     */
    List<? extends Transition> getStateSince(String family,
                                             List<String> states,
                                             Date since,
                                             Date before,
                                             int maxCount);

    /**
     * Returns the {@link Transition} instance identified by the supplied identity.
     *
     * @param identity
     *            the identity of the {@link Transition} instance to return.
     *
     * @return the {@link Transition} instance identified by the supplied identity.
     */
    Transition getTransition(String identity);

    /**
     * Returns the sequence of {@link Transition} instances that make up the history
     * of the specified item and process definition.
     *
     * @param identity
     *            the identity of the one of the transitions of the item and process
     *            definition pairing whose results should be returned.
     *
     * @return the sequence of {@link Transition} instances that make up the history
     *         of the specified item and process definition.
     */
    List<? extends Transition> getTransitions(String identity);

    /**
     * Returns the sequence of {@link Transition} instances that make up the history
     * of the specified item and process definition.
     *
     * @param item
     *            the identity of the item in this pairing.
     * @param process
     *            the process definition associated in this pairing.
     *
     * @return the sequence of {@link Transition} instances that make up the history
     *         of the specified item and process definition.
     */
    List<? extends Transition> getTransitions(String item,
                                              ProcessDefinition process);

    /**
     * Returns the sequence of the most recent {@link Transition} instances for all
     * items whose latest state, for the supplied process, does not match the
     * specified ones. The sequence is ordered oldest to newest.
     *
     * @param items
     *            the identity of the items whose matched latest transition should
     *            be returned.
     * @param process
     *            the process definition whose unprocessed items should be returned.
     * @param states
     *            the state in which the latest state of an item should be to be
     *            included in the result.
     *
     * @return the collection of the more recent {@link Transition} instances for
     *         all items whose latest state, for the supplied process, does not
     *         match the specified ones.
     */
    List<? extends Transition> getUnmatchLatestTransitions(List<String> items,
                                                           ProcessDefinition process,
                                                           List<String> states);

    /**
     * Returns the sequence of the most recent {@link Transition} instances for all
     * items whose latest state, for the supplied process, does not match the
     * specified ones. The sequence is ordered oldest to newest.
     *
     * @param process
     *            the process definition whose unprocessed items should be returned.
     * @param states
     *            the state in which the latest state of an item should be to be
     *            included in the result.
     * @param page
     *            the page, of specified length, from the total collection, to
     *            return.
     * @param length
     *            the length of a page, in number of items.
     *
     * @return the collection of the more recent {@link Transition} instances for
     *         all items whose latest state, for the supplied process, does not
     *         match the specified ones.
     */
    List<? extends Transition> getUnmatchLatestTransitions(ProcessDefinition process,
                                                           List<String> states,
                                                           Integer page,
                                                           Integer length);

    /**
     * Maps the URI that forms that base of a request onto the URI that forms that
     * base of the response.
     *
     * @param uri
     *            the base URI of the request.
     *
     * @return the base URI of the response.
     */
    URI mapBaseUri(URI uri);

    /**
     * Attempts the transition the pairing of a item and process definition from one
     * state to another.
     *
     * @param item
     *            the identity of the item in this pairing.
     * @param process
     *            the process definition associated in this pairing.
     * @param note
     *            a note, if any, to attach to the transition if it completes.
     * @param veto
     *            the name of the veto, is any, to apply to the submission.
     * @param scheduler
     *            the scheduler, if the default should not be used, to use when
     *            submitting the specified pairing.
     * @param uriBuilder
     *            the {@link UriHandler} used to create realized state URIs.
     *
     * @return the completed transaction, <code>null</code> otherwise.
     *
     * @throws AlreadyExitedException
     *             when the current state was exited by a different thread before
     *             this one could exit.
     * @throws ForbiddenTransitionException
     *             when the current state does not allow the requested exit.
     * @throws VetoedException
     *             when this submission was vetoed because the item had already been
     *             used with the specified {@link ProcessDefinition} instance's
     *             {@link Family}.
     */
    Transition submitPairing(String item,
                             ProcessDefinition process,
                             String note,
                             String veto,
                             KnownScheduler scheduler,
                             UriHandler uriBuilder) throws AlreadyExitedException,
                                                    ForbiddenTransitionException,
                                                    VetoedException;

}
