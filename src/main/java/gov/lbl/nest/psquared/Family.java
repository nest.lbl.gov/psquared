package gov.lbl.nest.psquared;

import java.util.List;

/**
 * This interface is used to define a family of process definitions.
 * 
 * @author patton
 * 
 */
public interface Family {

    /**
     * Returns the {@link ProcessDefinition} instance to use is none is explicitly
     * requested.
     * 
     * @return the {@link ProcessDefinition} instance to use is none is explicitly
     *         requested.
     */
    ProcessDefinition getDefaultDefinition();

    /**
     * Returns the description of this family.
     * 
     * @return the description of this family.
     */
    String getDescription();

    /**
     * Returns the identity of this object.
     * 
     * @return the identity of this object.
     */
    String getIdentity();

    /**
     * Returns the name of this object within the application.
     * 
     * @return the name of this object within the application.
     */
    String getName();

    /**
     * Returns the {@link ProcessDefinition} instances that belong to this family.
     * 
     * @return the {@link ProcessDefinition} instances that belong to this family.
     */
    List<? extends ProcessDefinition> getProcessDefinitions();

    /**
     * Returns true when items can be processed with this definition.
     * 
     * @return true when items can be processed with this definition.
     */
    boolean isActive();
}
