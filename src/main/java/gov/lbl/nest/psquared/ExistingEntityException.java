package gov.lbl.nest.psquared;

/**
 * This exception is thrown when a request to create an entity fails as one
 * already exists that matches the creation criteria.
 * 
 * @author patton
 */
public class ExistingEntityException extends
                                     Exception {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by the Serialized mechanism.
     */
    private static final long serialVersionUID = 0L;

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     * 
     * @param message
     *            a message, if any, explaining the reason for the failure.
     */
    public ExistingEntityException(String message) {
        super(message);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param message
     *            a message, if any, explaining the reason for the failure.
     * @param cause
     *            the {@link Throwable} instance that cause the failure.
     */
    public ExistingEntityException(String message,
                                   Throwable cause) {
        super(message,
              cause);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param cause
     *            the {@link Throwable} instance that cause the failure.
     */
    public ExistingEntityException(Throwable cause) {
        super(cause);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
