package gov.lbl.nest.psquared;

import java.util.Date;

/**
 * This interface captures the transition of an item/process pairing from one
 * state to another.
 * 
 * @author patton
 */
public interface Transition {

    /**
     * Returns the exit taken that cause this transition.
     * 
     * @return the exit taken that cause this transition.
     */
    String getExit();

    /**
     * Returns the identity of this transition.
     * 
     * @return the identity of this transition.
     */
    String getIdentity();

    /**
     * Returns the item involved in this transition.
     * 
     * @return the item involved in this transition.
     */
    String getItem();

    /**
     * Returns the message, if any, associated with this transition,
     * <code>null</code> otherwise.
     * 
     * @return the message, if any, associated with this transition,
     *         <code>null</code> otherwise.
     */
    String getMessage();

    /**
     * Returns the preceding {@link Transition}, if any, for this item/process
     * pairing.
     * 
     * @return the preceding {@link Transition}, if any, for this item/process
     *         pairing.
     */
    Transition getPrecedingTransition();

    /**
     * Returns the process definition involved in this transition.
     * 
     * @return the process definition involved in this transition.
     */
    ProcessDefinition getProcessDefinition();

    /**
     * Returns the resulting {@link State} of this transition.
     * 
     * @return the resulting {@link State} of this transition.
     */
    State getState();

    /**
     * Returns the succeeding {@link Transition}, if any, for this item/process
     * pairing.
     * 
     * @return the succeeding {@link Transition}, if any, for this item/process
     *         pairing.
     */
    Transition getSucceedingTransition();

    /**
     * Returns the date and time when this transition occurred.
     * 
     * @return the date and time when this transition occurred.
     */
    Date getWhenOccurred();

    /**
     * Returns true if this is the first transition for the item/process pairing.
     * 
     * @return true if this is the first transition for the item/process pairing.
     */
    boolean isFirst();

    /**
     * Returns true if this is currently the last transition for the item/process
     * pairing.
     * 
     * @return true if this is currently the last transition for the item/process
     *         pairing.
     */
    boolean isLast();

}
