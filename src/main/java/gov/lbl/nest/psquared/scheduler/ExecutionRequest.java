package gov.lbl.nest.psquared.scheduler;

import java.net.URI;
import java.util.List;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * The class contains the information needed by some schedulers in order to be
 * able to execute a process.
 * 
 * @author patton
 */
@XmlRootElement(name = "execution_request")
@XmlType(propOrder = { "submissionUri",
                       "command",
                       "success",
                       "failure",
                       "arguments" })
public class ExecutionRequest {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The action, if any, to take once this request has been completed.
     */
    private String action;

    /**
     * The sequence of arguments to be passed to the commands.
     */
    private List<String> arguments;

    /**
     * The command to be executed in order to execute this request.
     */
    private String command;

    /**
     * The command, if any, to execute if this request fails.
     */
    private String failure;

    /**
     * The path, if any, to use at the standard error when executing the request.
     */
    private String stderr;

    /**
     * The path, if any, to use at the standard input when executing the request.
     */
    private String stdin;

    /**
     * The path, if any, to use at the standard output when executing the request.
     */
    private String stdout;

    /**
     * The URI of the submission request for this execution.
     */
    private URI submissionUri;

    /**
     * The command, if any, to execute if this request succeeds.
     */
    private String success;

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the action, if any, to take once this request has been completed.
     * 
     * @return the action, if any, to take once this request has been completed.
     */
    @XmlAttribute
    protected String getAction() {
        return action;
    }

    /**
     * Returns the sequence of arguments to be passed to the commands.
     * 
     * @return the sequence of arguments to be passed to the commands.
     */
    @XmlElement(name = "argument")
    protected List<String> getArguments() {
        return arguments;
    }

    /**
     * Returns the command to be executed in order to execute this request.
     * 
     * @return the command to be executed in order to execute this request.
     */
    @XmlElement
    protected String getCommand() {
        return command;
    }

    /**
     * Returns the command, if any, to execute if this request fails.
     * 
     * @return the command, if any, to execute if this request fails.
     */
    @XmlElement(name = "failure_cmd")
    protected String getFailure() {
        return failure;
    }

    /**
     * Returns the path, if any, to use at the standard error when executing the
     * request.
     * 
     * @return the path, if any, to use at the standard error when executing the
     *         request.
     */
    @XmlAttribute
    protected String getStderr() {
        return stderr;
    }

    /**
     * Returns the path, if any, to use at the standard input when executing the
     * request.
     * 
     * @return the path, if any, to use at the standard input when executing the
     *         request.
     */
    @XmlAttribute
    protected String getStdin() {
        return stdin;
    }

    /**
     * Returns the path, if any, to use at the standard output when executing the
     * request.
     * 
     * @return the path, if any, to use at the standard output when executing the
     *         request.
     */
    @XmlAttribute
    protected String getStdout() {
        return stdout;
    }

    /**
     * Returns the URI of the submission request for this execution.
     * 
     * @return the URI of the submission request for this execution.
     */
    @XmlElement(name = "submission_uri")
    protected URI getSubmissionUri() {
        return submissionUri;
    }

    /**
     * Returns the command, if any, to execute if this request succeeds.
     * 
     * @return the command, if any, to execute if this request succeeds.
     */
    @XmlElement(name = "success_cmd")
    protected String getSuccess() {
        return success;
    }

    /**
     * Sets the action, if any, to take once this request has been completed.
     * 
     * @param action
     *            the action, if any, to take once this request has been completed.
     */
    protected void setAction(String action) {
        this.action = action;
    }

    /**
     * Sets the sequence of arguments to be passed to the commands.
     * 
     * @param arguments
     *            the sequence of arguments to be passed to the commands.
     */
    protected void setArguments(List<String> arguments) {
        this.arguments = arguments;
    }

    /**
     * Sets the command to be executed in order to execute this request.
     * 
     * @param command
     *            the command to be executed in order to execute this request.
     */
    protected void setCommand(String command) {
        this.command = command;
    }

    /**
     * Sets the command, if any, to execute if this request fails.
     * 
     * @param failure
     *            the command, if any, to execute if this request fails.
     */
    protected void setFailure(String failure) {
        this.failure = failure;
    }

    /**
     * Sets the path, if any, to use at the standard error when executing the
     * request.
     * 
     * @param path
     *            the path, if any, to use at the standard error when executing the
     *            request.
     */
    protected void setStderr(String path) {
        stderr = path;
    }

    /**
     * Sets the path, if any, to use at the standard input when executing the
     * request.
     * 
     * @param path
     *            the path, if any, to use at the standard input when executing the
     *            request.
     */
    protected void setStdin(String path) {
        stdin = path;
    }

    /**
     * Sets the path, if any, to use at the standard output when executing the
     * request.
     * 
     * @param path
     *            the path, if any, to use at the standard output when executing the
     *            request.
     */
    protected void setStdout(String path) {
        stdout = path;
    }

    /**
     * Sets the URI of the submission request for this execution.
     * 
     * @param uri
     *            the URI of the submission request for this execution.
     */
    protected void setSubmissionUri(URI uri) {
        submissionUri = uri;
    }

    /**
     * Sets the command, if any, to execute if this request succeeds.
     * 
     * @param success
     *            the command, if any, to execute if this request succeeds.
     */
    protected void setSuccess(String success) {
        this.success = success;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
