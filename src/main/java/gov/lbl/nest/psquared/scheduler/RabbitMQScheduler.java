package gov.lbl.nest.psquared.scheduler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import gov.lbl.nest.common.configure.CustomConfigPath;
import gov.lbl.nest.common.external.Command;
import gov.lbl.nest.common.external.ExecutionFailedException;
import gov.lbl.nest.psquared.PSquared;
import gov.lbl.nest.psquared.ProcessDefinition;
import gov.lbl.nest.psquared.SubmitException;
import gov.lbl.nest.psquared.ejb.SchedulerImpl;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

/**
 * This class submits item/processing pairs by executing the 'process command'
 * with a bash shell.
 * 
 * @author patton
 */
@Entity
@Table(name = "RabbitMQSubmitter2")
public class RabbitMQScheduler extends
                               SchedulerImpl {

    // public static final member data

    /**
     * The default name to use for the RabbitMQ properties file.
     */
    public static final String DEFAULT_PROPERTIES_NAME = "rabbitMQ.properties";

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} instance used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(RabbitMQScheduler.class);

    /**
     * The list of file to install by default.
     */
    private static final String[] RMQ_FILES = new String[] { "parameter_utils.py",
                                                             "inject_pika.py",
                                                             "Consumption.py",
                                                             "consume.py",
                                                             "NullConsumption.py",
                                                             "PSquaredConsumption.py" };

    /**
     * The name of the resource that may contain the path to an alternate default
     * configuration file for RMQ.
     */
    private static final String RMQ_CONFIG_RESOURCE = "rabbitmq_config";

    /**
     * The list of default files that should in executable.
     */
    private static final List<String> EXECUTABLES = Arrays.asList(new String[] { "inject_pika.py",
                                                                                 "consume.py" });

    // private static member data

    /**
     * True if the {@link #configFileName} value has been loaded.
     */
    private static CustomConfigPath customConfigPath = new CustomConfigPath(RabbitMQScheduler.class,
                                                                            RMQ_CONFIG_RESOURCE);

    // private instance member data

    /**
     * Creates the appropriate {@link ExecutionRequest} instance to publish.
     * 
     * @param item
     *            the identity of the item in this pairing.
     * @param process
     *            the name of the process definition associated in this pairing.
     * @param submissionUri
     *            the URI of the submmission's realized state.
     * 
     * @return the {@link ExecutionRequest} instance to be published.
     * 
     * @throws ParseException
     *             when the requests arguments can not be parsed.
     */
    private static ExecutionRequest createExecutionRequest(final String item,
                                                           final ProcessDefinition process,
                                                           final URI submissionUri) throws ParseException {
        final TwoPhaseDefinition definition = (TwoPhaseDefinition) process;
        final String family = (definition.getFamily()).getName();
        final String name = definition.getName();
        final Date dateTime = new Date();
        final ExecutionRequest request = new ExecutionRequest();
        final String args = definition.getArgs();
        if (null != args && !"".equals(args)) {
            request.setArguments(Utilities.parseArgs(args,
                                                     item,
                                                     family,
                                                     definition.getName()));
        }
        request.setCommand(definition.getProcessCmd());
        final String failureCmd = definition.getFailureCmd();
        if (null != failureCmd && !"".equals(failureCmd)) {
            request.setFailure(failureCmd);
        }
        request.setStderr((Utilities.getError(item,
                                              family,
                                              name,
                                              dateTime)).getPath());
        request.setStdout((Utilities.getOutput(item,
                                               family,
                                               name,
                                               dateTime)).getPath());
        request.setSubmissionUri(submissionUri);
        final String successCmd = definition.getSuccessCmd();
        if (null != successCmd && !"".equals(successCmd)) {
            request.setSuccess(successCmd);
        }
        return request;
    }

    /**
     * Installs the file needed for the default RabbitMQ scheduler.
     * 
     * @throws IOException
     *             the one of the file can not be installed.
     */
    public static void defaultInstall() throws IOException {
        final String home = System.getProperty("user.home");
        final File serverDir = new File(new File(home,
                                                 Utilities.DEFAULT_PSQUARED_DIRECTORY),
                                        Utilities.DEFAULT_RUNNER_DIRECTORY);
        for (String file : RMQ_FILES) {
            final File rmqFile = new File(serverDir,
                                          file);
            if (!rmqFile.exists()) {
                Utilities.createFileFromResource(Utilities.class,
                                                 rmqFile);
                if (EXECUTABLES.contains(file)) {
                    rmqFile.setExecutable(true);
                }
            }
        }
    }

    /**
     * Returns the alternate default configuration {@link Path} for this class.
     * 
     * @return the alternate default configuration {@link Path} for this class.
     */
    public static Path getConfigFileName() {
        return customConfigPath.getConfigPath();
    }

    /**
     * The RabbitMQ channel used to publish submissions.
     */
    private Channel channel;

    /**
     * The RabbitMQ connection used to publish submissions.
     */
    private Connection connection;

    /**
     * The path, if any, to the script that manages the workers.
     */
    private String managementScript;

    // constructors

    /**
     * The {@link Marshaller} instance used to publish XML as text.
     */
    private Marshaller marshaller;

    /**
     * The path to the file containing the RabbitMQ connection properties.
     */
    private String properties;

    // instance member method (alphabetic)

    /**
     * The name of the queue to which submissions are published.
     */
    private String queue;

    /**
     * Creates instance of this class.
     */
    protected RabbitMQScheduler() {
    }

    /**
     * Creates instance of this class.
     * 
     * @param name
     *            the submission instruction defined by this object.
     * @param description
     *            the description of this scheduler.
     * @param properties
     *            the path to the file containing the RabbitMQ connection
     *            properties.
     * @param queue
     *            the name of the queue to which submissions are published.
     */
    public RabbitMQScheduler(String name,
                             String description,
                             String properties,
                             String queue) {
        super(name,
              description);
        setProperties(properties);
        setQueue(queue);
    }

    /**
     * Returns the RabbitMQ channel used to publish submissions.
     * 
     * @return the RabbitMQ channel used to publish submissions.
     * 
     * @throws IOException
     *             if the channel can not be created.
     */
    @Transient
    private Channel getChannel() throws IOException {
        if (null == channel) {
            channel = getConnection().createChannel();
            channel.queueDeclare(queue,
                                 true,
                                 false,
                                 false,
                                 null);
        }
        return channel;
    }

    /**
     * Returns the RabbitMQ connection used to publish submissions.
     * 
     * @return the RabbitMQ connection used to publish submissions.
     * 
     * @throws IOException
     *             if the connection can not be made.
     */
    @Transient
    private Connection getConnection() throws IOException {
        if (null == connection) {
            ConnectionFactory factory = new ConnectionFactory();
            final String propsPath = getProperties();
            final File propertiesFile;
            if (propsPath.startsWith(Utilities.USER_HOME_STRING + File.separator)) {
                final String userHome = System.getProperty(Utilities.USER_HOME_PROPERTY);
                propertiesFile = new File(userHome + propsPath.substring(Utilities.USER_HOME_STRING.length()));
            } else {
                propertiesFile = new File(propsPath);
            }

            final Properties props = new Properties();
            props.load(new FileInputStream(propertiesFile));
            factory.setUsername(props.getProperty("gov.lbl.nest.psquared.submission.rabbitmq.user"));
            factory.setPassword(props.getProperty("gov.lbl.nest.psquared.submission.rabbitmq.password"));
            factory.setVirtualHost(props.getProperty("gov.lbl.nest.psquared.submission.rabbitmq.vhost"));
            factory.setHost(props.getProperty("gov.lbl.nest.psquared.submission.rabbitmq.host"));
            final String port = props.getProperty("gov.lbl.nest.psquared.submission.rabbitmq.port");
            if (null != port) {
                factory.setPort(Integer.parseInt(port));
            }
            try {
                LOG.debug("Attempting to connect to RMQ server on " + factory.getHost()
                          + " as user "
                          + factory.getUsername());
                connection = factory.newConnection();
                LOG.debug("Successfully connected to RMQ");
            } catch (TimeoutException e) {
                throw new IOException(e);
            }
        }
        return connection;
    }

    /**
     * Returns the path, if any, to the script that manages the workers.
     * 
     * @return the path, if any, to the script that manages the workers.
     */
    protected String getManagementScript() {
        return managementScript;
    }

    /**
     * Returns the {@link Marshaller} instance used by this class.
     * 
     * @return the {@link Marshaller} instance used by this class.
     */
    @Transient
    private Marshaller getMarshaller() {
        if (null == marshaller) {
            try {
                final JAXBContext jc = JAXBContext.newInstance(ExecutionRequest.class);
                marshaller = jc.createMarshaller();
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        }
        return marshaller;
    }

    /**
     * Returns the path to the file containing the RabbitMQ connection properties.
     * 
     * @return the path to the file containing the RabbitMQ connection properties.
     */
    protected String getProperties() {
        return properties;
    }

    /**
     * Returns the name of the queue to which submissions are published.
     * 
     * @return the name of the queue to which submissions are published.
     */
    @Column(nullable = false)
    protected String getQueue() {
        return queue;
    }

    /**
     * Sets the path, if any, to the script that manages the workers.
     * 
     * @param script
     *            the path, if any, to the script that manages the workers.
     */
    protected void setManagementScript(String script) {
        managementScript = script;
    }

    // static member methods (alphabetic)

    /**
     * Sets the path to the file containing the RabbitMQ connection properties.
     * 
     * @param path
     *            the path to the file containing the RabbitMQ connection
     *            properties.
     */
    protected void setProperties(String path) {
        properties = path;
    }

    /**
     * Sets the name of the queue to which submissions are published.
     * 
     * @param queue
     *            the name of the queue to which submissions are published.
     */
    protected void setQueue(String queue) {
        this.queue = queue;
    }

    @Override
    public String submit(String item,
                         ProcessDefinition process,
                         URI submissionUri,
                         PSquared psquared) throws SubmitException {
        final Marshaller marshallerToUse = getMarshaller();
        if (null == marshallerToUse) {
            throw new SubmitException("No Marshaller could be created");
        }
        try {
            final ExecutionRequest request = createExecutionRequest(item,
                                                                    process,
                                                                    submissionUri);
            final StringWriter writer = new StringWriter();
            marshallerToUse.marshal(request,
                                    writer);
            final String message = writer.toString();
            getChannel().basicPublish("",
                                      queue,
                                      MessageProperties.PERSISTENT_TEXT_PLAIN,
                                      message.getBytes());
            final Channel channelToClose = channel;
            channel = null;
            try {
                channelToClose.close();
            } catch (TimeoutException e1) {
                throw new SubmitException(e1);
            }
            final Connection connectionToClose = connection;
            connection = null;
            connectionToClose.close();
            final String script = getManagementScript();
            if (null != script) {
                final String[] cmdArray = new String[] { "bash",
                                                         "-c",
                                                         script + " "
                                                               + getProperties()
                                                               + " "
                                                               + getQueue() };
                final Command command = new Command(cmdArray);
                try {
                    command.execute();
                    final ExecutionFailedException cmdException = command.getCmdException();
                    if (null != cmdException) {
                        throw cmdException;
                    }
                } catch (IOException e) {
                    throw new SubmitException("Management execution failed",
                                              e);
                } catch (InterruptedException e) {
                    throw new SubmitException("Management execution interrupted",
                                              e);
                } catch (ExecutionFailedException e) {
                    throw new SubmitException("Management execution failed",
                                              e);
                }
            }
        } catch (JAXBException e) {
            e.printStackTrace();
            throw new SubmitException("Could not serialized ExecutionRequest XML",
                                      e);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new SubmitException("Failed access file",
                                      e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new SubmitException("Could not publish ExecutionRequest",
                                      e);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new SubmitException("Could not parse arguments",
                                      e);
        }
        return "RabbitMQ queue " + getQueue();
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
