package gov.lbl.nest.psquared.scheduler;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.psquared.Family;
import gov.lbl.nest.psquared.PSquared;
import gov.lbl.nest.psquared.ProcessDefinition;
import gov.lbl.nest.psquared.ejb.SchedulerImpl;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

/**
 * This class does not submit item/processing pairs for execution, but expects
 * external processes to take care of that. The Submission URI is written to the
 * application's log file.
 * 
 * @author patton
 */
@Entity
@Table(name = "NopSubmitter2")
public class NopScheduler extends
                          SchedulerImpl {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} instance used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(NopScheduler.class);

    // private static member data

    // private instance member data

    // constructors

    /**
     * Create instance of this class.
     */
    protected NopScheduler() {
    }

    /**
     * Create instance of this class.
     * 
     * @param name
     *            the submission instruction defined by this object.
     * @param description
     *            the description of this scheduler.
     */
    public NopScheduler(String name,
                        String description) {
        super(name,
              description);
    }

    // instance member method (alphabetic)

    @Override
    public String submit(String item,
                         ProcessDefinition process,
                         URI submissionUri,
                         PSquared psquared) {
        final Family family = process.getFamily();
        LOG.info("Item \'" + item
                 + "\' needs to be processed with version "
                 + process.getName()
                 + " of \'"
                 + family.getName()
                 + "\'.\n"
                 + "\tThis can be done by using following command:\n"
                 + "\t    pp_engine.py '"
                 + submissionUri.toString()
                 + "\' cmd [arg ...]");
        return null;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
