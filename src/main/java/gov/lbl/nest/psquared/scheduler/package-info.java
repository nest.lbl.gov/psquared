/**
 * This package provides various implementation of the
 * {@link gov.lbl.nest.psquared.KnownScheduler} interface.
 *
 * @author patton
 */
package gov.lbl.nest.psquared.scheduler;