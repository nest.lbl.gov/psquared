package gov.lbl.nest.psquared.scheduler;

import java.text.MessageFormat;

import gov.lbl.nest.psquared.KnownScheduler;
import gov.lbl.nest.psquared.ProcessDefinition;

/**
 * This interface used by two phase command based {@link KnownScheduler}
 * instances. The first phase is to execute the process command. The second
 * phase is to execute either the success or failure command, depending on the
 * result of the process command. All three commands are executed with the same
 * argument string.
 * 
 * @author patton
 */
public interface TwoPhaseDefinition extends
                                    ProcessDefinition {

    /**
     * Returns the {@link MessageFormat} string that will be command's arguments.
     * 
     * @return the {@link MessageFormat} string that will be command's arguments.
     */
    String getArgs();

    /**
     * Returns the command the will be run after processing has failed.
     * 
     * @return the command the will be run after processing has failed.
     */
    String getFailureCmd();

    /**
     * Returns the command that will be run to process an item.
     * 
     * @return the command that will be run to process an item.
     */
    String getProcessCmd();

    /**
     * Returns the command the will be run after processing has succeeded.
     * 
     * @return the command the will be run after processing has succeeded.
     */
    String getSuccessCmd();
}
