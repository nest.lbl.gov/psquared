package gov.lbl.nest.psquared.scheduler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.attribute.FileAttribute;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.external.Command;
import gov.lbl.nest.common.external.ExecutionFailedException;
import gov.lbl.nest.psquared.KnownScheduler;

/**
 * The class contains methods to help {@link KnownScheduler} instance fulfill
 * their responsibilities.
 * 
 * @author patton
 */
public class Utilities {

    // public static final member data

    /**
     * The default suffix to use on batch error files.
     */
    public static final String DEFAULT_ERROR_SUFFIX = ".err";

    /**
     * The default suffix to use on batch output files.
     */
    public static final String DEFAULT_OUTPUT_SUFFIX = ".out";

    /**
     * The default location of files used by the PSquared server.
     */
    public static final String DEFAULT_SERVER_DIRECTORY = "server";

    /**
     * The default location of files used by the PSquared runners.
     */
    public static final String DEFAULT_RUNNER_DIRECTORY = "runner";

    /**
     * The default psquared user area.
     */
    public static final String DEFAULT_PSQUARED_DIRECTORY = ".psquared";

    /**
     * The name of the system property that contains the users home directory.
     */
    public static final String USER_HOME_PROPERTY = "user.home";

    /**
     * The string used to signify the users home directory.
     */
    public static final String USER_HOME_STRING = "~";

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} instance used bu this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(Utilities.class);

    /**
     * The list of server file to install by default.
     */
    private static final String[] SERVER_FILES = new String[] { "parse_args.sh" };

    /**
     * The list of runner file to install by default.
     */
    private static final String[] RUNNER_FILES = new String[] { "pp_engine.py" };

    /**
     * The list of default files that should in executable.
     */
    private static final List<String> EXECUTABLES = Arrays.asList(new String[] { "parse_args.sh",
                                                                                 "pp_engine.py" });

    /**
     * The default location of log files in the psquared user area.
     */
    private static final String DEFAULT_LOG_DIRECTORY = "log";

    /**
     * The name of the script used to parse arguments for a bash script.
     */
    private static final String PARSE_ARGS_SCRIPT = "parse_args.sh";

    /**
     * The UTC {@link TimeZone}, used to build log file hierarchy.
     */
    private static final TimeZone UTC_TIMEZONE = TimeZone.getTimeZone("Etc/UTC");

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    /**
     * Creates a file from a resource.
     * 
     * @param clazz
     *            the class that is acting as the "parent" of this resource.
     * @param file
     *            the file to create.
     * 
     * @throws IOException
     *             when the file can not be created.
     */
    static void createFileFromResource(Class<?> clazz,
                                       File file) throws IOException {
        final File directory = file.getParentFile();
        if (null != directory && !directory.exists()) {
            Files.createDirectories(directory.toPath(),
                                    new FileAttribute<?>[] {});
        }
        final String resource = file.getName();
        try (final InputStream is = clazz.getResourceAsStream(resource)) {
            if (null == is) {
                throw new IOException("Can not find the resource \"" + resource
                                      + "\"");
            }
            Files.copy(is,
                       file.toPath(),
                       new CopyOption[] {});
        }
    }

    /**
     * Installs the file needed for the default RabbitMQ scheduler.
     * 
     * @throws IOException
     *             the one of the file can not be installed.
     */
    public static void defaultInstall() throws IOException {
        final String home = System.getProperty("user.home");
        final File serverDir = new File(new File(home,
                                                 DEFAULT_PSQUARED_DIRECTORY),
                                        DEFAULT_SERVER_DIRECTORY);
        for (String file : SERVER_FILES) {
            final File serverFile = new File(serverDir,
                                             file);
            if (!serverFile.exists()) {
                createFileFromResource(Utilities.class,
                                       serverFile);
                if (EXECUTABLES.contains(file)) {
                    serverFile.setExecutable(true);
                }
            }
        }

        final File runnerDir = new File(new File(home,
                                                 DEFAULT_PSQUARED_DIRECTORY),
                                        DEFAULT_RUNNER_DIRECTORY);
        for (String file : RUNNER_FILES) {
            final File runnerFile = new File(runnerDir,
                                             file);
            if (!runnerFile.exists()) {
                createFileFromResource(Utilities.class,
                                       runnerFile);
                if (EXECUTABLES.contains(file)) {
                    runnerFile.setExecutable(true);
                }
            }
        }

    }

    /**
     * Returns the {@link File} that will hold the errors from the submission
     * attempt.
     * 
     * @param item
     *            the identity of the item in this pairing.
     * @param family
     *            the family to which the process definition belongs.
     * @param name
     *            the name of the process definition associated in this pairing.
     * @param dateTime
     *            the date and time of when the job is being prepared for
     *            submission.
     * 
     * @return the {@link File} that will hold the errors from the submission
     *         attempt.
     */
    public static File getError(String item,
                                String family,
                                String name,
                                Date dateTime) {
        final File version = getLogDir(family,
                                       name,
                                       dateTime);
        return new File(version,
                        item + DEFAULT_ERROR_SUFFIX);
    }

    /**
     * Returns the directory into which the log files will be written when the
     * command executes.
     * 
     * @param family
     *            the family to which the process definition belongs.
     * @param name
     *            the name of the process definition associated in this pairing.
     * @param dateTime
     *            the date and time of when the job is being prepared for
     *            submission.
     * 
     * @return the directory into which the log files will be written when the
     *         command executes.
     */
    public static File getLocalLogDir(String family,
                                      String name,
                                      Date dateTime) {
        return getLogDir(family,
                         name,
                         dateTime,
                         true);
    }

    /**
     * Returns the directory into which the log files will be written when the
     * command executes.
     * 
     * @param family
     *            the family to which the process definition belongs.
     * @param name
     *            the name of the process definition associated in this pairing.
     * @param dateTime
     *            the date and time of when the job is being prepared for
     *            submission.
     * 
     * @return the directory into which the log files will be written when the
     *         command executes.
     */
    public static File getLogDir(String family,
                                 String name,
                                 Date dateTime) {
        return getLogDir(family,
                         name,
                         dateTime,
                         false);
    }

    /**
     * Returns the directory into which the log files will be written when the
     * command executes.
     * 
     * @param family
     *            the family to which the process definition belongs.
     * @param name
     *            the name of the process definition associated in this pairing.
     * @param dateTime
     *            the date and time of when the job is being prepared for
     *            submission.
     * @param local
     *            true when the log directory should be with respect the users home
     *            directory.
     * 
     * @return the directory into which the log files will be written when the
     *         command executes.
     */
    private static File getLogDir(String family,
                                  String name,
                                  Date dateTime,
                                  boolean local) {
        final Calendar calendar = new GregorianCalendar(UTC_TIMEZONE);
        calendar.setTime(dateTime);
        final File psqrdHome;
        if (local) {
            final String home = System.getProperty("user.home");
            psqrdHome = new File(home,
                                 DEFAULT_PSQUARED_DIRECTORY);
        } else {
            psqrdHome = new File(DEFAULT_PSQUARED_DIRECTORY);
        }
        final File logHome = new File(psqrdHome,
                                      DEFAULT_LOG_DIRECTORY);
        if (!logHome.exists()) {
            LOG.info("Creating base log directory \"" + logHome.toString()
                     + "\"");
            logHome.mkdir();
        }
        final File year = new File(logHome,
                                   String.format("%04d",
                                                 Integer.valueOf(calendar.get(Calendar.YEAR))));
        if (!year.exists()) {
            LOG.info("Creating 'year' log directory \"" + year.toString()
                     + "\"");
            year.mkdir();
        }
        final File month = new File(year,
                                    String.format("%02d",
                                                  Integer.valueOf(calendar.get(Calendar.MONTH) + 1)));
        if (!month.exists()) {
            if (!year.exists()) {
                LOG.info("Creating 'month' log directory \"" + month.toString()
                         + "\"");
                year.mkdir();
            }
            month.mkdir();
        }
        final File day = new File(month,
                                  String.format("%02d",
                                                Integer.valueOf(calendar.get(Calendar.DAY_OF_MONTH))));
        if (!day.exists()) {
            if (!year.exists()) {
                LOG.info("Creating 'day' log directory \"" + day.toString()
                         + "\"");
                year.mkdir();
            }
            day.mkdir();
        }
        final File configuration = new File(day,
                                            family);
        if (!configuration.exists()) {
            configuration.mkdir();
        }
        final File version = new File(configuration,
                                      name);
        if (!version.exists()) {
            version.mkdir();
        }
        return version;
    }

    /**
     * Returns the {@link File} that will hold the output from the submission
     * attempt.
     * 
     * @param item
     *            the identity of the item in this pairing.
     * @param family
     *            the family to which the process definition belongs.
     * @param name
     *            the name of the process definition associated in this pairing.
     * @param dateTime
     *            the date and time of when the job is being prepared for
     *            submission.
     * 
     * @return the {@link File} that will hold the output from the submission
     *         attempt.
     */
    public static File getOutput(String item,
                                 String family,
                                 String name,
                                 Date dateTime) {
        final File version = getLogDir(family,
                                       name,
                                       dateTime);
        return new File(version,
                        item + DEFAULT_OUTPUT_SUFFIX);
    }

    /**
     * Parses, and resolves any substitutions, for a single argument line returning
     * a sequence of arguments.
     * 
     * @param args
     *            the argument line to parse.
     * @param item
     *            the item for which the arguments will be used.
     * @param family
     *            the family for which the arguments will be used.
     * @param process
     *            the process for which the arguments will be used.
     * 
     * @return the sequence of arguments.
     * 
     * @throws ParseException
     *             when the arguments can not be parsed.
     */
    public static List<String> parseArgs(String args,
                                         String item,
                                         String family,
                                         String process) throws ParseException {
        final String home = System.getProperty("user.home");
        final File splitsFile = new File(new File(new File(home,
                                                           DEFAULT_PSQUARED_DIRECTORY),
                                                  DEFAULT_SERVER_DIRECTORY),
                                         PARSE_ARGS_SCRIPT);
        final MessageFormat message = new MessageFormat(args);
        final String[] parameters = new String[] { item,
                                                   family,
                                                   process };
        final String[] cmdArray = new String[] { "bash",
                                                 "-c",
                                                 splitsFile + " "
                                                       + message.format(parameters) };
        final Command command = new Command(cmdArray);
        try {
            command.execute();
            final ExecutionFailedException e = command.getCmdException();
            if (null != e) {
                for (String line : command.getStdErr()) {
                    System.out.println(line);
                }
                throw new ParseException("Parsing script at \"" + splitsFile
                                         + "\" failed during execution",
                                         -1);
            }
            return command.getStdOut();
        } catch (InterruptedException e) {
            throw new ParseException("Parsing of arguments interrupted",
                                     -1);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ParseException("Failed to execute parsing script at \"" + splitsFile
                                     + "\"",
                                     -1);
        }
    }

    /**
     * Resolves any variables in a path to create a concrete directory path.
     * 
     * @param path
     *            the path to resolve.
     * 
     * @return the resolved directory path.
     */
    public static String resolveDirectory(String path) {
        if (path.startsWith(USER_HOME_STRING + File.separator)) {
            final String userHome = System.getProperty(USER_HOME_PROPERTY);
            return userHome + path.substring(USER_HOME_STRING.length());
        }
        return path;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
