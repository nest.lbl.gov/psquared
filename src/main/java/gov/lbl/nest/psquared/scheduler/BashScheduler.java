package gov.lbl.nest.psquared.scheduler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.text.MessageFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.external.Command;
import gov.lbl.nest.psquared.Family;
import gov.lbl.nest.psquared.PSquared;
import gov.lbl.nest.psquared.ProcessDefinition;
import gov.lbl.nest.psquared.SubmitException;
import gov.lbl.nest.psquared.ejb.SchedulerImpl;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

/**
 * This class submits item/processing pairs by executing the 'process command'
 * with a bash shell.
 * 
 * @author patton
 */
@Entity
@Table(name = "BashSubmitter2")
public class BashScheduler extends
                           SchedulerImpl {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} instance used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(BashScheduler.class);

    /**
     * The prefix to use for the temporary file in which a description of the result
     * of the submission is written.
     */
    private static final String TEMP_FILE_PREFIX = "pp-submit-";

    /**
     * The suffix to use for the temporary file in which a description of the result
     * of the submission is written.
     */
    private static final String TEMP_FILE_SUFFIX = null;

    // private static member data

    // private instance member data

    /**
     * Returns the {@link File} that will hold the errors from the submission
     * attempt.
     * 
     * @param item
     *            the identity of the item in this pairing.
     * @param family
     *            the family to which the process definition belongs.
     * @param name
     *            the name of the process definition associated in this pairing.
     * @param dateTime
     *            the date and time of when the job is being prepared for
     *            submission.
     * 
     * @return the {@link File} that will hold the errors from the submission
     *         attempt.
     */
    private static File getSubmissionError(String item,
                                           String family,
                                           String name,
                                           Date dateTime) {
        final File version = Utilities.getLocalLogDir(family,
                                                      name,
                                                      dateTime);
        return new File(version,
                        item + Utilities.DEFAULT_ERROR_SUFFIX);
    }

    /**
     * Returns the {@link File} that will hold the output from the submission
     * attempt.
     * 
     * @param item
     *            the identity of the item in this pairing.
     * @param family
     *            the family to which the process definition belongs.
     * @param name
     *            the name of the process definition associated in this pairing.
     * @param dateTime
     *            the date and time of when the job is being prepared for
     *            submission.
     * 
     * @return the {@link File} that will hold the output from the submission
     *         attempt.
     */
    private static File getSubmissionOutput(String item,
                                            String family,
                                            String name,
                                            Date dateTime) {
        final File version = Utilities.getLocalLogDir(family,
                                                      name,
                                                      dateTime);
        return new File(version,
                        item + Utilities.DEFAULT_OUTPUT_SUFFIX);
    }

    // constructors

    /**
     * The submission instruction defined by this object.
     */
    private String instruction;

    /**
     * The path on the executing machine of the script that will execute the
     * submitted process.
     */
    private String runner;

    // instance member method (alphabetic)

    /**
     * Create instance of this class.
     */
    protected BashScheduler() {
    }

    /**
     * Create instance of this class.
     * 
     * @param name
     *            the submission instruction defined by this object.
     * @param description
     *            the description of this scheduler.
     * @param instuctions
     *            the name by which this submission instruction is referenced.
     * @param runner
     *            the path on the executing machine of the script that will execute
     *            the submitted process.
     */
    public BashScheduler(String name,
                         String description,
                         String instuctions,
                         String runner) {
        super(name,
              description);
        setInstruction(instuctions);
        setRunner(runner);
    }

    /**
     * Returns the complete execution line that should be submitted to the batch
     * system.
     * 
     * @param item
     *            the identity of the item in this pairing.
     * @param process
     *            the name of the process definition associated in this pairing.
     * @param submissionUri
     *            the URI of the submmission's realized state.
     * 
     * @return the complete execution line that should be submitted to the batch
     *         system.
     * 
     * @throws NoSuchConfigException
     *             when there is no such configuration.
     */
    private String getCommandCmd(final String item,
                                 final ProcessDefinition process,
                                 final URI submissionUri) {
        final TwoPhaseDefinition definition = (TwoPhaseDefinition) process;
        final Family family = definition.getFamily();
        final String[] parameters = new String[] { item,
                                                   family.getName(),
                                                   definition.getName() };
        final String successCmd = definition.getSuccessCmd();
        final String successOption;
        if (null == successCmd || "".equals(successCmd)) {
            successOption = "";
        } else {
            successOption = " -s " + successCmd;
        }
        final String failureCmd = definition.getFailureCmd();
        final String failureOption;
        if (null == failureCmd || "".equals(failureCmd)) {
            failureOption = "";
        } else {
            failureOption = " -f " + failureCmd;
        }
        final String args = definition.getArgs();
        final String argsToUse;
        if (null == args || "".equals(args)) {
            argsToUse = "";
        } else {
            argsToUse = " " + args;
        }
        final File runnerFile = new File(Utilities.resolveDirectory(getRunner()));
        final MessageFormat request = new MessageFormat(runnerFile.getPath() + successOption
                                                        + failureOption
                                                        + " "
                                                        + submissionUri.toString()
                                                        + " "
                                                        + definition.getProcessCmd()
                                                        + argsToUse);
        return request.format(parameters);
    }

    /**
     * Returns the submission instruction defined by this object.
     * 
     * @return the submission instruction defined by this object.
     */
    @Column(nullable = false)
    protected String getInstruction() {
        return instruction;
    }

    /**
     * Returns the path on the executing machine of the script that will execute the
     * submitted process.
     * 
     * @return the path on the executing machine of the script that will execute the
     *         submitted process.
     */
    @Column(nullable = false)
    protected String getRunner() {
        return runner;
    }

    /**
     * Returns the complete command needed to submit the execution command to the
     * batch system.
     * 
     * @param item
     *            the identity of the item in this pairing.
     * @param family
     *            the family to which the process definition belongs.
     * @param process
     *            the name of the process definition associated in this pairing.
     * @param submissionUri
     *            the URI of the submmission's realized state.
     * @param dateTime
     *            the date and time of when the job is being prepared for
     * @param descriptionFile
     *            the temporary file in which a description of the result of the
     *            submission is written.
     * 
     * @return the complete command needed to submit the execution command to the
     *         batch system.
     * 
     * @throws FileNotFoundException
     *             if the log files paths can not be created.
     */
    private String getSubmissionCmd(final String item,
                                    final String family,
                                    final ProcessDefinition process,
                                    final URI submissionUri,
                                    final Date dateTime,
                                    final String descriptionFile) throws FileNotFoundException {
        final String name = process.getName();
        final String[] files = new String[] { getCommandCmd(item,
                                                            process,
                                                            submissionUri),
                                              (Utilities.getOutput(item,
                                                                   family,
                                                                   name,
                                                                   dateTime)).getPath(),
                                              (Utilities.getError(item,
                                                                  family,
                                                                  name,
                                                                  dateTime)).getPath(),
                                              descriptionFile };
        final MessageFormat request = new MessageFormat(getInstruction());
        return request.format(files);
    }

    /**
     * Sets the submission instruction defined by this object.
     * 
     * @param instruction
     *            the submission instruction defined by this object.
     */
    protected void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    // static member methods (alphabetic)

    /**
     * Sets the path on the executing machine of the script that will execute the
     * submitted process.
     * 
     * @param path
     *            the path on the executing machine of the script that will execute
     *            the submitted process.
     */
    protected void setRunner(String path) {
        this.runner = path;
    }

    @Override
    public String submit(String item,
                         ProcessDefinition process,
                         URI submissionUri,
                         PSquared psquared) throws SubmitException {
        final Date dateTime = new Date();
        final String family = (process.getFamily()).getName();
        final String name = process.getName();

        final File descriptionFile;
        try {
            descriptionFile = File.createTempFile(TEMP_FILE_PREFIX,
                                                  TEMP_FILE_SUFFIX);
        } catch (IOException e) {
            LOG.error("Failed to submit " + item
                      + " for processing using "
                      + process.getName());
            throw new SubmitException("IOException when creating temporary file during submission");
        }

        final String cmdString;
        try {
            cmdString = getSubmissionCmd(item,
                                         family,
                                         process,
                                         submissionUri,
                                         dateTime,
                                         descriptionFile.getAbsolutePath());
        } catch (FileNotFoundException e) {
            descriptionFile.delete();
            LOG.error("Failed to submit " + item
                      + " for processing using "
                      + process.getName());
            throw new SubmitException("FileNotFoundException during submission");
        }
        final String[] cmdArray = new String[] { "bash",
                                                 "-c",
                                                 cmdString };
        Command command = new Command(cmdArray);
        int retCode;
        try {
            final File output = getSubmissionOutput(item,
                                                    family,
                                                    name,
                                                    dateTime);
            final File error = getSubmissionError(item,
                                                  family,
                                                  name,
                                                  dateTime);
            retCode = command.execute(output,
                                      error);
        } catch (IOException e) {
            descriptionFile.delete();
            LOG.error("Failed to submit " + item
                      + " for processing using "
                      + process.getName());
            e.printStackTrace();
            throw new SubmitException("IOException during submission");
        } catch (InterruptedException e) {
            descriptionFile.delete();
            LOG.warn("Interupted while trying to submit " + item
                     + " for processing using "
                     + process.getName());
            throw new SubmitException("Interrupted during submission");
        }
        if (0 != retCode) {
            descriptionFile.delete();
            LOG.error("Unable to execute \"" + cmdString
                      + "\" for processing, return code "
                      + retCode);
            throw new SubmitException("Submission failed with return code " + retCode);
        }
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(descriptionFile))) {
            final StringBuilder builder = new StringBuilder();
            String prefix = "";
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                builder.append(prefix + line);
                prefix = "\n";
            }
            bufferedReader.close();
            final String result = (builder.toString()).trim();
            if (null == result || "".equals(result)) {
                return null;
            }
            return result;
        } catch (IOException e) {
            return "Failed to read submission result!";
        } finally {
            descriptionFile.delete();
        }
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
