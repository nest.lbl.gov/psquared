package gov.lbl.nest.psquared.rs;

import java.net.URI;
import java.util.List;

import gov.lbl.nest.common.rs.Resource;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to collect a set of {@link Synopsis} instances for a set
 * of items for a single process, via the RESTful interface.
 * 
 * @author patton
 */
@XmlRootElement(name = "synopses")
@XmlType(propOrder = { "version",
                       "contents" })
public class Synopses extends
                      Resource {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The URI of the version to which the contents of this object belongs.
     */
    private URI version;

    /**
     * The collection of {@link Synopsis} instances related to this object.
     */
    private List<Synopsis> contents;

    /**
     * The length of a page, in number of items.
     */
    private Integer length;

    /**
     * The page, of specified length, from the total collection, contained in this
     * object.
     */
    private Integer page;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Synopses() {
    }

    /**
     * Creates and instance of this class.
     * 
     * @param uri
     *            The URI of this resource.
     * @param version
     *            The URI of the version to which the contents of this object
     *            belongs.
     * @param states
     *            the collection of {@link Synopsis} instances related to this
     *            object.
     * @param page
     *            the page, of specified length, from the total collection,
     *            contained in this object.
     * @param length
     *            the length of a page, in number of items.
     * 
     */
    Synopses(final URI uri,
             final URI version,
             final List<Synopsis> states,
             final Integer page,
             final Integer length) {
        super(uri);
        setContents(states);
        setLength(length);
        setPage(page);
        setVersion(version);
    }

    // instance member method (alphabetic)

    /**
     * Returns the collection of {@link Synopsis} instances related to this object.
     * 
     * @return the collection of {@link Synopsis} instances related to this object.
     */
    @XmlElement(name = "synopsis")
    protected List<Synopsis> getContents() {
        return contents;
    }

    /**
     * Returns the length of a page, in number of items.
     * 
     * @return the length of a page, in number of items.
     */
    @XmlAttribute
    protected Integer getLength() {
        return length;
    }

    /**
     * Returns the page, of specified length, from the total collection, contained
     * in this object.
     * 
     * @return the page, of specified length, from the total collection, contained
     *         in this object.
     */
    @XmlAttribute
    protected Integer getPage() {
        return page;
    }

    /**
     * Returns the URI of the version to which the contents of this object belongs.
     * 
     * @return the URI of the version to which the contents of this object belongs.
     */
    @XmlElement
    protected URI getVersion() {
        return version;
    }

    /**
     * Sets the collection of {@link Synopsis} instances related to this object.
     * 
     * @param content
     *            the collection of {@link Synopsis} instances related to this
     *            object.
     */
    protected void setContents(List<Synopsis> content) {
        this.contents = content;
    }

    /**
     * Sets the length of a page, in number of items.
     * 
     * @param length
     *            the length of a page, in number of items.
     */
    protected void setLength(Integer length) {
        this.length = length;
    }

    /**
     * Sets the page, of specified length, from the total collection, contained in
     * this object.
     * 
     * @param page
     *            the page, of specified length, from the total collection,
     *            contained in this object.
     */
    protected void setPage(Integer page) {
        this.page = page;
    }

    /**
     * Sets the URI of the version to which the contents of this object belongs.
     * 
     * @param uri
     *            the URI of the version to which the contents of this object
     *            belongs.
     */
    protected void setVersion(URI uri) {
        version = uri;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
