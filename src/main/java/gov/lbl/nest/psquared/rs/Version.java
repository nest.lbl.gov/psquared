package gov.lbl.nest.psquared.rs;

import java.net.URI;
import java.util.List;

import gov.lbl.nest.common.rs.NamedResource;
import gov.lbl.nest.psquared.Transition;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class represents the particular version of a named set of processing
 * definition and provides access to resources that operate with this
 * definition.
 * 
 * @author patton
 */
@XmlRootElement(name = "version")
@XmlType(propOrder = { "configuration",
                       "last",
                       "commands",
                       "scheduler",
                       "actions",
                       "reports",
                       "development" })
public class Version extends
                     NamedResource {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The collection of {@link ActionsGroup} instances that can be used to initiate
     * a command, via a POST.
     */
    private List<ActionsGroup> actions;

    /**
     * The {@link Commands} instance that defines what this version executes.
     */
    private Commands commands;

    /**
     * The URI of the configuration to which this object belongs.
     */
    private URI configuration;

    /**
     * The level of detail the representation of this object.
     */
    private String details;

    /**
     * True when this definition is used during process development, which it to say
     * that it is mutable.
     */
    private Boolean development;

    /**
     * The {@link Synopsis} instance of the most recent {@link Transition} instance
     * associated with this object.
     */
    private Synopsis last;

    /**
     * The collection of {@link ReportsGroup} instances that can be used to request
     * a report.
     */
    private List<ReportsGroup> reports;

    /**
     * the URI to use to return the default scheduler for this object.
     */
    private URI scheduler;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected Version() {
    }

    /**
     * Create an instance of this class.
     * 
     * @param uri
     *            the URI that will refresh the client representation of this
     *            object.
     * @param details
     *            the level of detail the representation of this object.
     * @param name
     *            the name of this object, unique within the owning configuration.
     * @param description
     *            the human friendly description of this resource.
     * @param configuration
     *            the URI of the configuration to which this object belongs.
     * @param last
     *            the {@link Synopsis} instance of the most recent
     *            {@link Transition} instance associated with this object.
     * @param scheduler
     *            the URI to use to return the default scheduler for this object.
     * @param actions
     *            the collection of {@link ActionsGroup} instances that can be used
     *            to initiate a command, via a POST.
     * @param reports
     *            the collection of {@link ReportsGroup} instances that can be used
     *            to request a report.
     * @param development
     *            true when this definition is used during process development,
     *            which it to say that it is mutable.
     * @param current
     *            the URI to use to return the current state for a set of items.
     * @param histories
     *            the URI to use to return the history for a set of items.
     * @param submit
     *            the URI to use to submit a set of items for processing with this
     *            version.
     */
    Version(URI uri,
            String details,
            String name,
            String description,
            URI configuration,
            Synopsis last,
            URI scheduler,
            Commands commands,
            List<ActionsGroup> actions,
            List<ReportsGroup> reports,
            boolean development) {
        super(uri,
              name,
              description);
        setActions(actions);
        setCommands(commands);
        setConfiguration(configuration);
        setDevelopment(development);
        this.details = details;
        setLast(last);
        setReports(reports);
        setScheduler(scheduler);
    }

    // instance member method (alphabetic)

    /**
     * Returns the collection of {@link ActionsGroup} instances that can be used to
     * initiate a command, via a POST.
     * 
     * @return the collection of {@link ActionsGroup} instances that can be used to
     *         initiate a command, via a POST.
     */
    @XmlElement
    protected List<ActionsGroup> getActions() {
        return actions;
    }

    /**
     * Returns the {@link Commands} instance that defines what this version
     * executes.
     * 
     * @return the {@link Commands} instance that defines what this version
     *         executes.
     */
    @XmlElement
    public Commands getCommands() {
        return commands;
    }

    /**
     * Returns the URI of the configuration to which this object belongs.
     * 
     * @return the URI of the configuration to which this object belongs.
     */
    @XmlElement
    public URI getConfiguration() {
        return configuration;
    }

    /**
     * Returns the level of detail the representation of this object.
     * 
     * @return the level of detail the representation of this object.
     */
    @XmlAttribute
    protected String getDetails() {
        return details;
    }

    /**
     * Returns the {@link Synopsis} instance of the most recent {@link Transition}
     * instance associated with this object.
     * 
     * @return the {@link Synopsis} instance of the most recent {@link Transition}
     *         instance associated with this object.
     */
    @XmlElement
    protected Synopsis getLast() {
        return last;
    }

    /**
     * Returns the collection of {@link ReportsGroup} instances that can be used to
     * request a report.
     * 
     * @return the collection of {@link ReportsGroup} instances that can be used to
     *         request a report.
     */
    @XmlElement
    protected List<ReportsGroup> getReports() {
        return reports;
    }

    /**
     * Returns the URI to use to return the default scheduler for this object.
     * 
     * @return the URI to use to return the default scheduler for this object.
     */
    @XmlElement(name = "default_scheduler")
    public URI getScheduler() {
        return scheduler;
    }

    /**
     * Returns the URI of this resource.
     * 
     * @return the URI of this resource.
     */
    @Override
    public URI getUri() {
        return super.getUri();
    }

    /**
     * Returns true when this definition is used during process development, which
     * it to say that it is mutable.
     * 
     * @return true when this definition is used during process development, which
     *         it to say that it is mutable.
     */
    @XmlElement
    public Boolean isDevelopment() {
        return development;
    }

    /**
     * Sets the collection of {@link ActionsGroup} instances that can be used to
     * initiate a command, via a POST.
     * 
     * @param actions
     *            the collection of {@link ActionsGroup} instances that can be used
     *            to initiate a command, via a POST.
     */
    protected void setActions(List<ActionsGroup> actions) {
        this.actions = actions;
    }

    /**
     * Sets the {@link Commands} instance that defines what this version executes.
     * 
     * @param commands
     *            the {@link Commands} instance that defines what this version
     *            executes.
     */
    protected void setCommands(Commands commands) {
        this.commands = commands;
    }

    /**
     * Sets the URI of the configuration to which this object belongs.
     * 
     * @param uri
     *            the URI of the configuration to which this object belongs.
     */
    protected void setConfiguration(URI uri) {
        configuration = uri;
    }

    /**
     * Sets the level of detail the representation of this object.
     * 
     * @param details
     *            the level of detail the representation of this object.
     */
    protected void setDetails(String details) {
        this.details = details;
    }

    /**
     * Sets whether this definition is used during process development, which it to
     * say that it is mutable, nor not.
     * 
     * @param development
     *            True when this definition is used during process development
     */
    protected void setDevelopment(boolean development) {
        this.development = development;
    }

    /**
     * Sets the {@link Synopsis} instance of the most recent {@link Transition}
     * instance associated with this object.
     * 
     * @param last
     *            the {@link Synopsis} instance of the most recent
     *            {@link Transition} instance associated with this object.
     */
    protected void setLast(Synopsis last) {
        this.last = last;
    }

    /**
     * Sets the collection of {@link ReportsGroup} instances that can be used to
     * request a report.
     * 
     * @param reports
     *            the collection of {@link ReportsGroup} instances that can be used
     *            to request a report.
     */
    protected void setReports(List<ReportsGroup> reports) {
        this.reports = reports;
    }

    /**
     * Sets the URI to use to return the default scheduler for this object.
     * 
     * @param uri
     *            the URI to use to return the default scheduler for this object.
     */
    protected void setScheduler(URI uri) {
        scheduler = uri;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
