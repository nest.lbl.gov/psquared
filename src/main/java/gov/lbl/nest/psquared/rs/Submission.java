package gov.lbl.nest.psquared.rs;

import java.net.URI;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate a request to submit one or more items for
 * processing with a specified configuration, via the RESTful interface.
 * 
 * @author patton
 */
@XmlRootElement(name = "submission")
@XmlType(propOrder = { "message",
                       "scheduler" })
public class Submission extends
                        Selection {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The message, if any, to be associated with the submission.
     */
    private String message;

    /**
     * The URI identifying the scheduler, if any, to be used to schedule the
     * processing of the pairing.
     */
    private URI scheduler;

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the message, if any, to be used to annotate the submission.
     * 
     * @return the message, if any, to be used to annotate the submission.
     */
    @XmlElement
    protected String getMessage() {
        return message;
    }

    /**
     * Returns the URI identifying the scheduler, if any, to be used to schedule the
     * processing of the pairing.
     * 
     * @return the URI identifying the scheduler, if any, to be used to schedule the
     *         processing of the pairing.
     */
    @XmlElement
    protected URI getScheduler() {
        return scheduler;
    }

    /**
     * Sets the message, if any, to be used to annotate the submission.
     * 
     * @param message
     *            the message, if any, to be used to annotate the submission.
     */
    protected void setMessage(String message) {
        this.message = message;
    }

    /**
     * Sets the URI identifying the scheduler, if any, to be used to schedule the
     * processing of the pairing.
     * 
     * @param name
     *            the URI identifying the scheduler, if any, to be used to schedule
     *            the processing of the pairing.
     */
    protected void setScheduler(URI name) {
        scheduler = name;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
