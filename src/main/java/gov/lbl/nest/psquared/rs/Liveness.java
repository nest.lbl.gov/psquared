package gov.lbl.nest.psquared.rs;

import java.net.URI;

import gov.lbl.nest.common.rs.Resource;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlType(
        propOrder = {
                      "dbState"
        })
public class Liveness extends
                      Resource {

    /**
     * The date and time this entry was completed.
     */
    private String dbState;

    /**
     * Creates an instance of this class.
     */
    protected Liveness() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param dbState
     *            the String specifying the DB state.
     */
    protected Liveness(URI uri,
                       String dbState) {
        super(uri);
        this.dbState = dbState;
    }

    /**
     * Returns the String specifying the DB state.
     * 
     * @return the String specifying the DB state.
     */
    @XmlElement(name = "database")
    public String getDbState() {
        return dbState;
    }
}
