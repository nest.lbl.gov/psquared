package gov.lbl.nest.psquared.rs;

import java.net.URI;

import gov.lbl.nest.common.rs.Resource;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate an allowed exit transition from the state
 * of an item/configuration pairing via the RESTful interface.
 * 
 * @author patton
 */
@XmlType
public class Exit extends
                  Resource {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The name of the allowed exit transition.
     */
    private String name;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected Exit() {
    }

    /**
     * Create an instance of this class.
     * 
     * @param name
     *            the name of the allowed exit transition.
     * @param uri
     *            the URI to POST to in order to execute this transition.
     */
    Exit(String name,
         URI uri) {
        super(uri);
        setName(name);
    }

    // instance member method (alphabetic)

    /**
     * Returns the name of the allowed exit transition.
     * 
     * @return the name of the allowed exit transition.
     */
    @XmlElement(required = true)
    protected String getName() {
        return name;
    }

    /**
     * Sets the name of the allowed exit transition.
     * 
     * @param name
     *            the name of the allowed exit transition.
     */
    protected void setName(String name) {
        this.name = name;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
