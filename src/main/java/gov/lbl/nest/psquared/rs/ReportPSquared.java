package gov.lbl.nest.psquared.rs;

import java.net.URI;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import gov.lbl.nest.common.rs.NamedResource;
import gov.lbl.nest.common.rs.Resource;
import gov.lbl.nest.common.watching.Digest;
import gov.lbl.nest.common.watching.DigestChange;
import gov.lbl.nest.psquared.Family;
import gov.lbl.nest.psquared.ImplementationString;
import gov.lbl.nest.psquared.KnownScheduler;
import gov.lbl.nest.psquared.PSquared;
import gov.lbl.nest.psquared.ProcessDefinition;
import gov.lbl.nest.psquared.State;
import gov.lbl.nest.psquared.Transition;
import gov.lbl.nest.psquared.UriHandler;
import gov.lbl.nest.psquared.scheduler.TwoPhaseDefinition;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.UriInfo;

/**
 * The class provides a RESTful interface to access read-only data held by the
 * PSquared application.
 *
 * @author patton
 *
 * @see CommandPSquared
 */
@Stateless @Path("report")
public class ReportPSquared {

    static class PSquaredUriHandler implements
                                    UriHandler {

        final private URI baseUri;

        PSquaredUriHandler(UriInfo uriInfo,
                           PSquared psquared) {
            final URI mappedBase = psquared.mapBaseUri(uriInfo.getBaseUri());
            if (null == mappedBase) {
                baseUri = uriInfo.getBaseUri();
            } else {
                baseUri = mappedBase;
            }
        }

        @Override
        public URI buildFromIdentity(String identity) {
            return baseUri.resolve("report/state/" + identity);
        }

        @Override
        public String getIdentity(URI uri) {
            final String s = uri.toString();
            final String[] components = s.split("/");
            return components[components.length - 1];
        }

        @Override
        public URI resolve(String str) {
            return baseUri.resolve(str);
        }
    }

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The list of states to ignore when returning the set of unprocessed items.
     */
    private static final List<String> UNPROCESSED_EXCLUDE_STATES = Arrays.asList(new String[] { State.READY.toString(),
                                                                                                State.PROCESSED.toString(),
                                                                                                State.ABANDONED.toString() });

    /**
     * The list of states to be included when returning the set of processed items.
     */
    private static final List<String> PROCESSED_INCLUDE_STATE = Arrays.asList(new String[] { State.PROCESSED.toString() });

    /**
     * The format for date queries.
     */
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * The format for date and time queries.
     */
    private static final String DATE_AND_TIME_FORMAT = DATE_FORMAT + "'T'HH:mm:ss";

    /**
     * The format for date and time queries.
     */
    private static final String LONG_DATE_AND_TIME_FORMAT = DATE_FORMAT + "'T'HH:mm:ss.SSS";

    /**
     * The format for date and time queries.
     */
    private static final String ZONED_FORMAT = DATE_AND_TIME_FORMAT + "Z";

    /**
     * The format for date and time queries.
     */
    private static final String LONG_ZONED_FORMAT = LONG_DATE_AND_TIME_FORMAT + "Z";

    /**
     * The formatter for date supplied to this class as Strings.
     */
    private static final DateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT);

    /**
     * The formatter for date and time supplied to this class as Strings.
     */
    private static final DateFormat DATE_AND_TIME_FORMATTER = new SimpleDateFormat(DATE_AND_TIME_FORMAT);

    /**
     * The formatter for date and time supplied to this class as Strings.
     */
    private static final DateFormat LONG_DATE_AND_TIME_FORMATTER = new SimpleDateFormat(LONG_DATE_AND_TIME_FORMAT);

    /**
     * The formatter for date and time supplied to this class as Strings.
     */
    private static final DateFormat ZONED_FORMATTER = new SimpleDateFormat(ZONED_FORMAT);

    /**
     * The formatter to use to create a for date and time query element.
     */
    public static final DateFormat LONG_ZONED_FORMATTER = new SimpleDateFormat(LONG_ZONED_FORMAT);

    /**
     * The original version of the Details.
     */
    public static final String ORIGINAL_DETAILS = "2.2";

    /**
     * The {@link String} instance to return last transaction of a version.
     */
    public static final String LAST_DETAILS = "last";

    /**
     * The {@link String} instance to return action details of a version.
     */
    public static final String ACTIONS_DETAILS = "actions";

    /**
     * The {@link String} instance to return command details of a version.
     */
    public static final String COMMANDS_DETAILS = "commands";

    /**
     * The {@link String} instance to return report details of a version.
     */
    public static final String REPORTS_DETAILS = "reports";

    /**
     * The {@link String} instance to return full details of a version.
     */
    public static final String FULL_DETAILS = "full";

    /**
     * The {@link String} instance to return default details of a version.
     */
    public static final String DEFAULT_VERSION_DETAILS = "2.4";

    // private static member data

    // private instance member data

    /**
     * Creates the collection of actions a process can supply.
     *
     * @param responseBaseUri
     *            the {@link UriHandler} to use to create the {@link RealizedState}
     *            instance.
     * @param psquared
     *            the {@link PSquared} instance to create the {@link RealizedState}
     *            instance.
     * @param definition
     *            the {@link ProcessDefinition} whose summaries should be returned.
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     *
     * @return
     */
    private static List<ActionsGroup> createActions(final UriHandler responseBaseUri,
                                                    final PSquared psquared,
                                                    final ProcessDefinition definition) {
        if (null == definition) {
            return null;
        }
        final String identity = definition.getIdentity();

        final List<ActionsGroup> result = new ArrayList<>();

        /*
         * The following actions require a Submission object to be the contents of the
         * request.
         */
        final List<NamedResource> itemizedActions = new ArrayList<>();
        itemizedActions.add(new NamedResource(responseBaseUri.resolve("command/submit/" + identity),
                                              "submit",
                                              "Submits the selected items for processing, using the default scheduler unless on is specified."));
        result.add(new ActionsGroup("submission",
                                    "Commands an action for the submitted collection of items for a family/version",
                                    itemizedActions));
        return result;
    }

    private static Commands createCommands(final ProcessDefinition definition) {
        if (definition instanceof TwoPhaseDefinition) {
            final TwoPhaseDefinition twoPhase = (TwoPhaseDefinition) definition;
            return new Commands(twoPhase.getProcessCmd(),
                                twoPhase.getSuccessCmd(),
                                twoPhase.getFailureCmd(),
                                twoPhase.getArgs());
        }
        throw new IllegalArgumentException("Only TowPhaseDefinitions are currently supported");
    }

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the representation of the specified configuration.
     *
     * @param responseBaseUri
     *            the {@link UriHandler} to use to create the {@link RealizedState}
     *            instance.
     * @param psquared
     *            the {@link PSquared} instance to create the {@link RealizedState}
     *            instance.
     * @param family
     *            the {@link Family} instance associated with this configuration
     *            representation to be created.
     * @param details
     *            the level of detail of versions within the response. Currently
     *            supported details are "actions", "commands", "reports" and "full".
     *
     * @return the representation of this specified configuration.
     */
    static Configuration createConfiguration(UriHandler responseBaseUri,
                                             PSquared psquared,
                                             Family family,
                                             String details) {
        final List<Version> versions = new ArrayList<>();
        final URI familyUri = responseBaseUri.resolve("report/configuration/" + family.getIdentity());
        final ProcessDefinition defaultDefinition = family.getDefaultDefinition();
        final URI defaultUri;
        if (null == defaultDefinition || !defaultDefinition.isActive()) {
            defaultUri = null;
        } else {
            defaultUri = responseBaseUri.resolve("report/version/" + defaultDefinition.getIdentity());
        }

        final String detailsToUse;
        if (null == details) {
            detailsToUse = null;
        } else {
            detailsToUse = details;
        }

        final Synopsis synopsisToUse;
        if (FULL_DETAILS.equals(detailsToUse) || LAST_DETAILS.equals(detailsToUse)) {
            synopsisToUse = createSynopsis(psquared,
                                           psquared.getLatestTransition(family),
                                           -1);
        } else {
            synopsisToUse = null;
        }
        List<? extends ProcessDefinition> processDefinitions = family.getProcessDefinitions();
        if (null != processDefinitions) {
            for (ProcessDefinition definition : processDefinitions) {
                if (definition.isActive()) {
                    Version version = createVersion(responseBaseUri,
                                                    psquared,
                                                    definition,
                                                    detailsToUse);
                    // Removed duplicated configuration URI
                    version.setConfiguration(null);
                    versions.add(version);
                }
            }
        }
        Configuration configuration = new Configuration(familyUri,
                                                        detailsToUse,
                                                        family.getName(),
                                                        family.getDescription(),
                                                        synopsisToUse,
                                                        defaultUri,
                                                        versions);
        return configuration;
    }

    private static Entry createEntry(final UriHandler responseBaseUri,
                                     final Transition transition,
                                     final int depth) {
        if (transition.isFirst() || 0 == depth) {
            return new Entry(null == responseBaseUri,
                             transition.getState(),
                             transition.getWhenOccurred(),
                             transition.getMessage(),
                             null);
        }
        final Transition precedingTransition = transition.getPrecedingTransition();
        return new Entry(null == responseBaseUri,
                         transition.getState(),
                         transition.getWhenOccurred(),
                         transition.getMessage(),
                         createPrecedingState(responseBaseUri,
                                              precedingTransition,
                                              depth - 1));
    }

    private static History createHistory(final UriHandler responseBaseUri,
                                         final PSquared psquared,
                                         final List<? extends Transition> transitions) {
        if (null == transitions || transitions.isEmpty()) {
            return null;
        }
        final Transition initialTransition = transitions.get(transitions.size() - 1);
        final Transition lastTransition = transitions.get(0);
        return new History(responseBaseUri.resolve("report/history/" + initialTransition.getIdentity()),
                           createRealizedState(responseBaseUri,
                                               psquared,
                                               lastTransition,
                                               -1));
    }

    private static PrecedingState createPrecedingState(final UriHandler responseBaseUri,
                                                       final Transition transition,
                                                       final int depth) {
        final URI uri;
        if (null == responseBaseUri) {
            uri = null;
        } else {
            uri = responseBaseUri.buildFromIdentity(transition.getIdentity());
        }
        return new PrecedingState(uri,
                                  createEntry(responseBaseUri,
                                              transition,
                                              depth));
    }

    /**
     * Creates an {@link RealizedState} instance from the supplied
     * {@link Transition} instance.
     *
     * @param responseBaseUri
     *            the {@link UriHandler} to use to create the {@link RealizedState}
     *            instance.
     * @param psquared
     *            the {@link PSquared} instance to create the {@link RealizedState}
     *            instance.
     * @param transition
     *            the {@link Transition} instance from which to build the
     *            {@link RealizedState} instance.
     * @param depth
     *            the number of entries to include in the resulting
     *            {@link RealizedState} instance.
     *
     * @return the {@link RealizedState} instance created from the supplied
     *         {@link Transition}
     */
    static RealizedState createRealizedState(final UriHandler responseBaseUri,
                                             final PSquared psquared,
                                             final Transition transition,
                                             final int depth) {
        if (null == transition) {
            return null;
        }
        final List<Exit> exits;
        final Exit exited;
        if (transition.isLast()) {
            exits = new ArrayList<>();
            for (String allowedTransition : psquared.getAllowedExits(transition.getState())) {
                exits.add(new Exit(allowedTransition,
                                   responseBaseUri.resolve("command/exit/" + transition.getIdentity()
                                                           + "/"
                                                           + allowedTransition)));
            }
            exited = null;
        } else {
            exits = null;
            final Transition succeedingTransition = transition.getSucceedingTransition();
            exited = new Exit(succeedingTransition.getExit(),
                              responseBaseUri.buildFromIdentity(succeedingTransition.getIdentity()));
        }
        final ProcessDefinition definition = transition.getProcessDefinition();
        return new RealizedState(responseBaseUri.buildFromIdentity(transition.getIdentity()),
                                 createEntry(responseBaseUri,
                                             transition,
                                             depth),
                                 exits,
                                 exited,
                                 transition.getItem(),
                                 responseBaseUri.resolve("report/version/" + definition.getIdentity()));
    }

    /**
     * Creates the collection of reports a process can supply.
     *
     * @param responseBaseUri
     *            the {@link UriHandler} to use to create the {@link RealizedState}
     *            instance.
     * @param psquared
     *            the {@link PSquared} instance to create the {@link RealizedState}
     *            instance.
     * @param definition
     *            the {@link ProcessDefinition} whose summaries should be returned.
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     *
     * @return
     */
    private static List<ReportsGroup> createReports(final UriHandler responseBaseUri,
                                                    final PSquared psquared,
                                                    final ProcessDefinition definition) {
        if (null == definition) {
            return null;
        }
        final String identity = definition.getIdentity();

        final List<ReportsGroup> result = new ArrayList<>();

        /*
         * The following reports require a Selection object to be the contents of the
         * request.
         */
        final List<NamedResource> itemizedReports = new ArrayList<>();
        itemizedReports.add(new NamedResource(responseBaseUri.resolve("report/selected/executing/" + identity),
                                              "executing",
                                              "Returns a summary for selected items currently processing"));
        itemizedReports.add(new NamedResource(responseBaseUri.resolve("report/selected/failed/" + identity),
                                              "failed",
                                              "Returns a summary for selected items that have failed to be processed"));
        itemizedReports.add(new NamedResource(responseBaseUri.resolve("report/selected/history/" + identity),
                                              "history",
                                              "Returns the details of the history of the selected items."));
        itemizedReports.add(new NamedResource(responseBaseUri.resolve("report/selected/state/latest/" + identity),
                                              "latest",
                                              "Returns the details of the current state of the selected items."));
        itemizedReports.add(new NamedResource(responseBaseUri.resolve("report/selected/prepared/" + identity),
                                              "prepared",
                                              "Returns a summary for selected items currently prepared"));
        itemizedReports.add(new NamedResource(responseBaseUri.resolve("report/selected/processed/" + identity),
                                              "processed",
                                              "Returns a summary for selected items successfully processed"));
        itemizedReports.add(new NamedResource(responseBaseUri.resolve("report/selected/unprocessed/" + identity),
                                              "unprocessed",
                                              "Returns a summary for selected items that have not successfully completed processing"));
        itemizedReports.add(new NamedResource(responseBaseUri.resolve("report/selected/waiting/" + identity),
                                              "waiting",
                                              "Returns a summary for selected items waiting to be processed"));
        result.add(new ReportsGroup("itemized",
                                    "Requests the details of a specified collection of items for a family/version",
                                    itemizedReports));

        /*
         * The following require require the contents of the request to be empty.
         */
        final List<NamedResource> summaryReports = new ArrayList<>();
        summaryReports.add(new NamedResource(responseBaseUri.resolve("report/version/executing/" + identity),
                                             "executing",
                                             "Returns a summary for each item currently processing"));
        summaryReports.add(new NamedResource(responseBaseUri.resolve("report/version/failed/" + identity),
                                             "failed",
                                             "Returns a summary for each item that has failed to be processed"));
        summaryReports.add(new NamedResource(responseBaseUri.resolve("report/version/prepared/" + identity),
                                             "prepared",
                                             "Returns a summary for each item currently prepared"));
        summaryReports.add(new NamedResource(responseBaseUri.resolve("report/version/processed/" + identity),
                                             "processed",
                                             "Returns a summary for each item successfully processed"));
        summaryReports.add(new NamedResource(responseBaseUri.resolve("report/version/unprocessed/" + identity),
                                             "unprocessed",
                                             "Returns a summary for each item that has not successfully completed processing"));
        summaryReports.add(new NamedResource(responseBaseUri.resolve("report/version/waiting/" + identity),
                                             "waiting",
                                             "Returns a summary for each item waiting to be processed"));
        result.add(new ReportsGroup("summary",
                                    "Requests a summary of a collection of items for a family/version",
                                    summaryReports));
        return result;
    }

    private static Synopses createSynopses(UriInfo uriInfo,
                                           String resource,
                                           PSquared psquared,
                                           String versionId,
                                           Integer page,
                                           Integer length,
                                           List<? extends Transition> transitions) {
        final UriHandler responseBaseUri = new PSquaredUriHandler(uriInfo,
                                                                  psquared);
        final List<Synopsis> synopses = new ArrayList<>();
        if (null != transitions && !transitions.isEmpty()) {
            for (Transition transition : transitions) {
                final Synopsis synopsis = createSynopsis(psquared,
                                                         transition,
                                                         0);
                if (null != synopsis) {
                    synopses.add(synopsis);
                }
            }
        }
        return new Synopses(responseBaseUri.resolve("report/" + resource
                                                    + versionId),
                            responseBaseUri.resolve("report/version/" + versionId),
                            synopses,
                            page,
                            length);
    }

    /**
     * Creates an {@link RealizedState} instance from the supplied
     * {@link Transition} instance.
     *
     * @param psquared
     *            the {@link PSquared} instance to create the {@link RealizedState}
     *            instance.
     * @param transition
     *            the {@link Transition} instance from which to build the
     *            {@link RealizedState} instance.
     * @param depth
     *            the number of entries to include in the resulting
     *            {@link RealizedState} instance. -1 indicate none.
     *
     * @return the {@link RealizedState} instance created from the supplied
     *         {@link Transition}
     */
    static Synopsis createSynopsis(PSquared psquared,
                                   Transition transition,
                                   int depth) {
        if (null == transition) {
            return null;
        }

        if (transition.isFirst() || 0 > depth) {
            return new Synopsis(transition.getItem(),
                                transition.getState(),
                                transition.getWhenOccurred(),
                                transition.getMessage(),
                                null);
        }
        final Transition precedingTransition = transition.getPrecedingTransition();
        return new Synopsis(transition.getItem(),
                            transition.getState(),
                            transition.getWhenOccurred(),
                            transition.getMessage(),
                            createPrecedingState(null,
                                                 precedingTransition,
                                                 depth));
    }

    /**
     * Create a {@link Version} instance from the supplied {@link ProcessDefinition}
     * instance.
     *
     * @param responseBaseUri
     *            the {@link UriHandler} to use to create the {@link RealizedState}
     *            instance.
     * @param psquared
     *            the {@link PSquared} instance to create the {@link RealizedState}
     *            instance.
     * @param definition
     *            the {@link ProcessDefinition} instance from which to create the
     *            {@link Version} instance.
     * @param details
     *            the level of detail of versions within the response. Currently
     *            supported details are "last", "actions", "commands", "reports" and
     *            "full".
     *
     * @return
     */
    static Version createVersion(UriHandler responseBaseUri,
                                 PSquared psquared,
                                 ProcessDefinition definition,
                                 String details) {
        final String identity = definition.getIdentity();
        final Family family = definition.getFamily();
        final String detailsToUse;
        if (null == details) {
            detailsToUse = DEFAULT_VERSION_DETAILS;
        } else {
            detailsToUse = details;
        }
        final Synopsis synopsisToUse;
        if (FULL_DETAILS.equals(detailsToUse) || LAST_DETAILS.equals(detailsToUse)) {
            synopsisToUse = createSynopsis(psquared,
                                           psquared.getLatestTransition(definition),
                                           -1);
        } else {
            synopsisToUse = null;
        }

        final Commands commandsToUse;
        if (FULL_DETAILS.equals(detailsToUse) || COMMANDS_DETAILS.equals(detailsToUse)) {
            commandsToUse = createCommands(definition);
        } else {
            commandsToUse = null;
        }
        final List<ActionsGroup> actionsToUse;
        if (ORIGINAL_DETAILS.equals(detailsToUse) || FULL_DETAILS.equals(detailsToUse)
            || ACTIONS_DETAILS.equals(detailsToUse)) {
            actionsToUse = createActions(responseBaseUri,
                                         psquared,
                                         definition);
        } else {
            actionsToUse = null;
        }
        final List<ReportsGroup> reportsToUse;
        if (ORIGINAL_DETAILS.equals(detailsToUse) || FULL_DETAILS.equals(detailsToUse)
            || REPORTS_DETAILS.equals(detailsToUse)) {
            reportsToUse = createReports(responseBaseUri,
                                         psquared,
                                         definition);
        } else {
            reportsToUse = null;
        }
        final KnownScheduler scheduler = definition.getDefaultScheduler();
        return new Version(responseBaseUri.resolve("report/version/" + identity),
                           details,
                           definition.getName(),
                           definition.getDescription(),
                           responseBaseUri.resolve("report/configuration/" + family.getIdentity()),
                           synopsisToUse,
                           responseBaseUri.resolve("report/scheduler/" + scheduler.getIdentity()),
                           commandsToUse,
                           actionsToUse,
                           reportsToUse,
                           definition.isDevelopment());
    }

    /**
     * Creates an {@link RealizedState} instance from the supplied
     * {@link Transition} instance.
     *
     * @param responseBaseUri
     *            the {@link UriHandler} to use to create the {@link RealizedState}
     *            instance.
     * @param transition
     *            the {@link Transition} instance from which to build the
     *            {@link RealizedState} instance.
     *
     * @return the {@link RealizedState} instance created from the supplied
     *         {@link Transition}
     */
    static RealizedState createVetoed(final UriHandler responseBaseUri,
                                      final String item,
                                      final ProcessDefinition processDefinition,
                                      final String message) {
        return new RealizedState(new Entry(true,
                                           State.VETOED,
                                           new Date(),
                                           message,
                                           null),
                                 item,
                                 responseBaseUri.resolve("report/version/" + processDefinition.getIdentity()));
    }

    /**
     * Parses a query date and/or time into a {@link Date} instance.
     *
     * @param dateTime
     *            the date and/or time to parse
     *
     * @return the {@link Date} instance.
     *
     * @throws ParseException
     *             when the date and/or time can not be parsed.
     */
    static Date parseQueryDate(final String dateTime) throws ParseException {
        if (-1 == dateTime.indexOf('.')) {
            if (DATE_FORMAT.length() == dateTime.length()) {
                return DATE_FORMATTER.parse(dateTime);
            }
            if ((DATE_AND_TIME_FORMAT.length() - 2) == dateTime.length()) {
                return DATE_AND_TIME_FORMATTER.parse(dateTime);
            }
            return ZONED_FORMATTER.parse(dateTime);
        }
        if ((LONG_DATE_AND_TIME_FORMAT.length() - 2) == dateTime.length()) {
            return LONG_DATE_AND_TIME_FORMATTER.parse(dateTime);
        }
        return LONG_ZONED_FORMATTER.parse(dateTime);
    }

    /**
     * The {@link PSquared} instance used by this object.
     */
    @Inject
    private PSquared psquared;

    /**
     * The implementation version.
     */
    @Inject @ImplementationString
    private String implementation;

    /**
     * Returns the representation of this application.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param details
     *            the level of detail of versions within the response. Currently
     *            supported details are "last", "actions", "commands", "reports" and
     *            "full".
     *
     * @return the representation of this application.
     */
    @GET
    @Path("")
    @Produces({ PSquaredType.APPLICATION_XML,
                PSquaredType.APPLICATION_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Resource getApplication(@Context UriInfo uriInfo,
                                   @QueryParam("details") String details) {
        final UriHandler responseBaseUri = new PSquaredUriHandler(uriInfo,
                                                                  psquared);

        final URI baseUri = uriInfo.getBaseUri();
        final URI reportUri = baseUri.resolve("report/");
        final URI commandUri = baseUri.resolve("command/");
        final List<NamedResource> creationCommands = new ArrayList<>();
        creationCommands.add(new NamedResource(commandUri.resolve("create/configuration"),
                                               "configuration",
                                               "Creates a new configuration"));
        creationCommands.add(new NamedResource(commandUri.resolve("create/version"),
                                               "version",
                                               "Creates a new version for a configuration"));
        final ActionsGroup create = new ActionsGroup("creation",
                                                     null,
                                                     creationCommands);
        final List<ActionsGroup> actions = new ArrayList<>();
        actions.add(create);
        final List<? extends Family> families = psquared.getFamilies();
        final List<Configuration> configurations = new ArrayList<>();
        for (Family family : families) {
            if (family.isActive()) {
                configurations.add(new Configuration(reportUri.resolve("configuration/" + family.getIdentity()),
                                                     details,
                                                     family.getName(),
                                                     family.getDescription(),
                                                     null,
                                                     null,
                                                     null));
            }
        }
        final ConfigurationsGroup configurationGroup = new ConfigurationsGroup(null,
                                                                               "Available Configurations",
                                                                               configurations);

        final List<? extends KnownScheduler> schedulers = psquared.getSchedulers();
        final List<NamedResource> schedulersToList = new ArrayList<>();
        for (KnownScheduler scheduler : schedulers) {
            schedulersToList.add(new NamedResource(reportUri.resolve("scheduler/" + scheduler.getIdentity()),
                                                   scheduler.getName(),
                                                   scheduler.getDescription()));
        }
        final SchedulersGroup schedulerGroup = new SchedulersGroup(null,
                                                                   "Available Schedulers",
                                                                   schedulersToList);
        return new Application(responseBaseUri.resolve("report"),
                               actions,
                               configurationGroup,
                               schedulerGroup,
                               PSquared.SPECIFICATION,
                               implementation);
    }

    /**
     * Returns the representation of the specified configuration.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param configurationId
     *            the version whose URL should be returned.
     * @param details
     *            the level of detail of versions within the response. Currently
     *            supported details are "last", "actions", "commands", "reports" and
     *            "full".
     *
     * @return the representation of this specified configuration.
     */
    @GET
    @Path("configuration/{configurationId}")
    @Produces({ PSquaredType.CONFIGURATION_XML,
                PSquaredType.CONFIGURATION_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getConfiguration(@Context UriInfo uriInfo,
                                     @PathParam("configurationId") String configurationId,
                                     @QueryParam("details") String details) {
        final UriHandler responseBaseUri = new PSquaredUriHandler(uriInfo,
                                                                  psquared);
        final Family family = psquared.getFamily(configurationId);
        if (null == family || !family.isActive()) {
            return (Response.status(Response.Status.NOT_FOUND)).build();
        }
        final String detailsToUse;
        if (null == details || "".equals(details)) {
            detailsToUse = null;
        } else {
            detailsToUse = details;
        }
        Configuration configuration = createConfiguration(responseBaseUri,
                                                          psquared,
                                                          family,
                                                          detailsToUse);
        return (Response.ok(configuration)).build();
    }

    /**
     * Returns the {@link Synopses} of all items currently executing with the
     * specified version.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param versionId
     *            the identity of the a version whose current states should be
     *            returned.
     * @param page
     *            the page, of specified length, from the total collection, to
     *            return.
     * @param length
     *            the length of a page, in number of items.
     *
     * @return the {@link Synopses} of all items currently executing with the
     *         specified version.
     */
    @GET
    @Path("version/executing/{versionId}")
    @Produces({ PSquaredType.REALIZED_STATES_XML,
                PSquaredType.REALIZED_STATES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Synopses getExecuting(@Context UriInfo uriInfo,
                                 @PathParam("versionId") String versionId,
                                 @QueryParam("page") Integer page,
                                 @QueryParam("length") Integer length) {
        return getMatchLatestTransition(uriInfo,
                                        "version/executing/",
                                        versionId,
                                        State.EXECUTING.toString(),
                                        page,
                                        length);
    }

    /**
     * Returns the {@link Synopses} of all items that have failed to be processed
     * with the specified version.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param versionId
     *            the identity of the a version whose current states should be
     *            returned.
     * @param page
     *            the page, of specified length, from the total collection, to
     *            return.
     * @param length
     *            the length of a page, in number of items.
     *
     * @return the {@link Synopses} of all items have failed to be processed with
     *         the specified version.
     */
    @GET
    @Path("version/failed/{versionId}")
    @Produces({ PSquaredType.REALIZED_STATES_XML,
                PSquaredType.REALIZED_STATES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Synopses getFailed(@Context UriInfo uriInfo,
                              @PathParam("versionId") String versionId,
                              @QueryParam("page") Integer page,
                              @QueryParam("length") Integer length) {
        return getMatchLatestTransition(uriInfo,
                                        "version/failed/",
                                        versionId,
                                        State.FAILED.toString(),
                                        page,
                                        length);
    }

    /**
     * Returns the item that has the more recent {@link Transition} instance for the
     * supplied version.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param configurationId
     *            the identity of the a version whose more recent transition should
     *            be returned.
     *
     * @return the item that has the more recent {@link Transition} instance for the
     *         supplied version.
     */
    @GET
    @Path("configuration/last/{configurationId}")
    @Produces({ PSquaredType.REALIZED_STATES_XML,
                PSquaredType.REALIZED_STATES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Synopses getLastByConfiguration(@Context UriInfo uriInfo,
                                           @PathParam("configurationId") String configurationId) {
        final Family family = psquared.getFamily(configurationId);
        final Transition transition = psquared.getLatestTransition(family);
        final List<Transition> transitions = new ArrayList<>(1);
        transitions.add(transition);

        return createSynopses(uriInfo,
                              "version/last/",
                              psquared,
                              family.getIdentity(),
                              null,
                              null,
                              transitions);
    }

    /**
     * Returns the item that has the more recent {@link Transition} instance for the
     * supplied version.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param versionId
     *            the identity of the a version whose more recent transition should
     *            be returned.
     *
     * @return the item that has the more recent {@link Transition} instance for the
     *         supplied version.
     */
    @GET
    @Path("version/last/{versionId}")
    @Produces({ PSquaredType.REALIZED_STATES_XML,
                PSquaredType.REALIZED_STATES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Synopses getLastByVersion(@Context UriInfo uriInfo,
                                     @PathParam("versionId") String versionId) {
        final ProcessDefinition processDefinition = psquared.getProcessDefinition(versionId);
        final Transition transition = psquared.getLatestTransition(processDefinition);
        final List<Transition> transitions = new ArrayList<>(1);
        transitions.add(transition);

        return createSynopses(uriInfo,
                              "version/last/",
                              psquared,
                              versionId,
                              null,
                              null,
                              transitions);
    }

    private Synopses getMatchLatestTransition(UriInfo uriInfo,
                                              String resource,
                                              String versionId,
                                              String latestState,
                                              Integer page,
                                              Integer length) {
        final ProcessDefinition processDefinition = psquared.getProcessDefinition(versionId);
        final List<? extends Transition> transitions = psquared.getMatchLatestTransitions(processDefinition,
                                                                                          latestState,
                                                                                          page,
                                                                                          length);
        return createSynopses(uriInfo,
                              resource,
                              psquared,
                              versionId,
                              page,
                              length,
                              transitions);
    }

    /**
     * Returns the current Liveness data for this Application.
     */
    @GET
    @Path("liveness")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Liveness getLiveness(UriInfo uriInfo) {
        final UriHandler responseBaseUri = new PSquaredUriHandler(uriInfo,
                                                                  psquared);
        return new Liveness(responseBaseUri.resolve("application/liveness"),
                            "pass");
    }

    private Synopses getMatchLatestTransitions(UriInfo uriInfo,
                                               String resource,
                                               List<String> items,
                                               String versionId,
                                               String latestState) {
        final ProcessDefinition processDefinition = psquared.getProcessDefinition(versionId);
        final List<? extends Transition> transitions = psquared.getMatchLatestTransitions(items,
                                                                                          processDefinition,
                                                                                          latestState);
        return createSynopses(uriInfo,
                              resource,
                              psquared,
                              versionId,
                              null,
                              null,
                              transitions);
    }

    /**
     * Returns the {@link Synopses} of all items successfully prepared with the
     * specified version.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param versionId
     *            the identity of the a version whose current states should be
     *            returned.
     * @param page
     *            the page, of specified length, from the total collection, to
     *            return.
     * @param length
     *            the length of a page, in number of items.
     *
     * @return the {@link Synopses} of all items successfully prepared with the
     *         specified version.
     */
    @GET
    @Path("version/prepared/{versionId}")
    @Produces({ PSquaredType.REALIZED_STATES_XML,
                PSquaredType.REALIZED_STATES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Synopses getPrepared(@Context UriInfo uriInfo,
                                @PathParam("versionId") String versionId,
                                @QueryParam("page") Integer page,
                                @QueryParam("length") Integer length) {
        return getMatchLatestTransition(uriInfo,
                                        "version/prepared/",
                                        versionId,
                                        State.READY.toString(),
                                        page,
                                        length);
    }

    /**
     * Returns the {@link Synopses} of all items successfully processed with the
     * specified version.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param versionId
     *            the identity of the a version whose current states should be
     *            returned.
     * @param page
     *            the page, of specified length, from the total collection, to
     *            return.
     * @param length
     *            the length of a page, in number of items.
     *
     * @return the {@link Synopses} of all items successfully processed with the
     *         specified version.
     */
    @GET
    @Path("version/processed/{versionId}")
    @Produces({ PSquaredType.REALIZED_STATES_XML,
                PSquaredType.REALIZED_STATES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Synopses getProcessed(@Context UriInfo uriInfo,
                                 @PathParam("versionId") String versionId,
                                 @QueryParam("page") Integer page,
                                 @QueryParam("length") Integer length) {
        return getMatchLatestTransition(uriInfo,
                                        "version/processed/",
                                        versionId,
                                        State.PROCESSED.toString(),
                                        page,
                                        length);
    }

    // static member methods (alphabetic)

    /**
     * Returns the XML representation of the list of items that have been completely
     * processed between the specified dates.
     *
     * @param family
     *            the name of the family whose files should be returned.
     * @param since
     *            items whose processing succeeded on or after this time should be
     *            include in the returned list.
     * @param before
     *            items whose processing succeeded before this time should be
     *            include in the returned list. If <code>null</code> then no time
     *            limit is used.
     * @param max
     *            the maximum number of bundles to return, 0 or less means
     *            unlimited.
     *
     * @return the XML representation of the list of warehouse ids that have been
     *         modified since the specified date.
     */
    @GET
    @Path("date/processed")
    @Produces({ PSquaredType.DIGEST_XML,
                PSquaredType.DIGEST_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Digest getProcessedSince(@QueryParam("family") String family,
                                    @QueryParam("since") String since,
                                    @QueryParam("before") String before,
                                    @QueryParam("max") Integer max) {

        try {
            if (null == since) {
                return new Digest();
            }
            final Date begin = parseQueryDate(since);
            final Date end;
            if (null == before) {
                end = null;
            } else {
                end = parseQueryDate(before);
            }
            final int maxCount;
            if (null == max) {
                maxCount = 0;
            } else {
                maxCount = max.intValue();
            }
            final List<? extends Transition> transitions = psquared.getStateSince(family,
                                                                                  PROCESSED_INCLUDE_STATE,
                                                                                  begin,
                                                                                  end,
                                                                                  maxCount);
            final List<DigestChange> items = new ArrayList<>(transitions.size());
            for (Transition transition : transitions) {
                items.add(new DigestChange(transition.getItem(),
                                           transition.getWhenOccurred()));
            }
            if (items.isEmpty()) {
                return new Digest();
            }
            return new Digest(family + " processed",
                              items);
        } catch (ParseException e) {
            return new Digest();
        }
    }

    /**
     * Returns the results of the identified completed transaction.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param transitionId
     *            the identity of the completed transition whose resulting realized
     *            state should be returned.
     *
     * @return the results of the identified completed transaction.
     */
    @GET
    @Path("state/{transitionId}")
    @Produces({ PSquaredType.REALIZED_STATES_XML,
                PSquaredType.REALIZED_STATES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getRealizedState(@Context UriInfo uriInfo,
                                     @PathParam("transitionId") String transitionId) {
        final Transition transition = psquared.getTransition(transitionId);
        final ResponseBuilder builder;
        if (null == transition) {
            builder = Response.status(Response.Status.NOT_FOUND);
        } else {
            final UriHandler responseBaseUri = new PSquaredUriHandler(uriInfo,
                                                                      psquared);
            builder = Response.ok(createRealizedState(responseBaseUri,
                                                      psquared,
                                                      transition,
                                                      1));
        }
        return builder.build();
    }

    /**
     * Returns the {@link Synopses} of selected items currently executing with the
     * specified version.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param selection
     *            the select of one or more items together with a specified version
     *            whose latest state should be returned.
     * @param versionId
     *            the identity of the a version whose current states should be
     *            returned.
     *
     * @return the {@link Synopses} of selected items currently being processed with
     *         the specified version.
     */
    @GET
    @Path("selected/executing/{versionId}")
    @Produces({ PSquaredType.REALIZED_STATES_XML,
                PSquaredType.REALIZED_STATES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Synopses getSelectedExecuting(@Context UriInfo uriInfo,
                                         Selection selection,
                                         @PathParam("versionId") String versionId) {
        return getMatchLatestTransitions(uriInfo,
                                         "selected/executing/",
                                         selection.getItems(),
                                         versionId,
                                         State.EXECUTING.toString());
    }

    /**
     * Returns the {@link Synopses} of selected items that have failed to be
     * processed with the specified version.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param selection
     *            the select of one or more items together with a specified version
     *            whose latest state should be returned.
     * @param versionId
     *            the identity of the a version whose current states should be
     *            returned.
     *
     * @return the {@link Synopses} of selected items that have failed to be
     *         processed with the specified version.
     */
    @GET
    @Path("selected/failed/{versionId}")
    @Produces({ PSquaredType.REALIZED_STATES_XML,
                PSquaredType.REALIZED_STATES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Synopses getSelectedFailed(@Context UriInfo uriInfo,
                                      Selection selection,
                                      @PathParam("versionId") String versionId) {
        return getMatchLatestTransitions(uriInfo,
                                         "selected/failed/",
                                         selection.getItems(),
                                         versionId,
                                         State.FAILED.toString());
    }

    /**
     * Returns the histories of the selected items with the specified version.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param selection
     *            the select of one or more items together with a specified version
     *            whose history should be returned.
     * @param versionId
     *            the identity of the a version whose histories should be returned.
     *
     * @return the results of the identified completed transaction.
     */
    @GET
    @Path("selected/history/{versionId}")
    @Consumes({ PSquaredType.SELECTION_XML,
                PSquaredType.SELECTION_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ PSquaredType.HISTORIES_XML,
                PSquaredType.HISTORIES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Histories getSelectedHistories(@Context UriInfo uriInfo,
                                          Selection selection,
                                          @PathParam("versionId") String versionId) {
        final ProcessDefinition processDefinition = psquared.getProcessDefinition(versionId);
        final UriHandler responseBaseUri = new PSquaredUriHandler(uriInfo,
                                                                  psquared);
        final List<History> histories = new ArrayList<>();
        for (String item : selection.getItems()) {
            histories.add(createHistory(responseBaseUri,
                                        psquared,
                                        psquared.getTransitions(item,
                                                                processDefinition)));
        }
        return new Histories(histories);
    }

    /**
     * Returns the latest state of processing of the items with the specified
     * version.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param selection
     *            the select of one or more items together with a specified version
     *            whose latest state should be returned.
     * @param versionId
     *            the identity of the a version whose latest states should be
     *            returned.
     *
     * @return the results of the identified completed transaction.
     */
    @GET
    @Path("selected/state/latest/{versionId}")
    @Consumes({ PSquaredType.SELECTION_XML,
                PSquaredType.SELECTION_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ PSquaredType.REALIZED_STATES_XML,
                PSquaredType.REALIZED_STATES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })

    public RealizedStates getSelectedLatestStates(@Context UriInfo uriInfo,
                                                  Selection selection,
                                                  @PathParam("versionId") String versionId) {
        final ProcessDefinition processDefinition = psquared.getProcessDefinition(versionId);
        final List<? extends Transition> latestTransitions = psquared.getLatestTransitions(selection.getItems(),
                                                                                           processDefinition);
        final UriHandler responseBaseUri = new PSquaredUriHandler(uriInfo,
                                                                  psquared);
        final List<RealizedState> states = new ArrayList<>();
        for (Transition latestTransition : latestTransitions) {
            final RealizedState realizedState = createRealizedState(responseBaseUri,
                                                                    psquared,
                                                                    latestTransition,
                                                                    1);
            if (null != realizedState) {
                states.add(realizedState);
            }
        }
        return new RealizedStates(states);
    }

    /**
     * Returns the {@link Synopses} of all items successfully prepared with the
     * specified version.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param selection
     *            the select of one or more items together with a specified version
     *            whose latest state should be returned.
     * @param versionId
     *            the identity of the a version whose current states should be
     *            returned.
     *
     * @return the {@link Synopses} of selected items successfully prepared with the
     *         specified version.
     */
    @GET
    @Path("selected/prepared/{versionId}")
    @Produces({ PSquaredType.REALIZED_STATES_XML,
                PSquaredType.REALIZED_STATES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Synopses getSelectedPrepared(@Context UriInfo uriInfo,
                                        Selection selection,
                                        @PathParam("versionId") String versionId) {
        return getMatchLatestTransitions(uriInfo,
                                         "selected/prepared/",
                                         selection.getItems(),
                                         versionId,
                                         State.READY.toString());
    }

    /**
     * Returns the {@link Synopses} of all items successfully processed with the
     * specified version.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param selection
     *            the select of one or more items together with a specified version
     *            whose latest state should be returned.
     * @param versionId
     *            the identity of the a version whose current states should be
     *            returned.
     *
     * @return the {@link Synopses} of selected items successfully processed with
     *         the specified version.
     */
    @GET
    @Path("selected/processed/{versionId}")
    @Produces({ PSquaredType.REALIZED_STATES_XML,
                PSquaredType.REALIZED_STATES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Synopses getSelectedProcessed(@Context UriInfo uriInfo,
                                         Selection selection,
                                         @PathParam("versionId") String versionId) {
        return getMatchLatestTransitions(uriInfo,
                                         "selected/processed/",
                                         selection.getItems(),
                                         versionId,
                                         State.PROCESSED.toString());
    }

    /**
     * Returns the current state of selected items that are unprocessed with the
     * specified version. Unprocessed is defines as not being in the READY,
     * PROCESSED or ABANDON states.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param selection
     *            the select of one or more items together with a specified version
     *            whose latest state should be returned.
     * @param versionId
     *            the identity of the a version whose current states should be
     *            returned.
     *
     * @return the results of the identified completed transaction.
     */
    @GET
    @Path("selected/unprocessed/{versionId}")
    @Produces({ PSquaredType.REALIZED_STATES_XML,
                PSquaredType.REALIZED_STATES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Synopses getSelectedUnprocessed(@Context UriInfo uriInfo,
                                           Selection selection,
                                           @PathParam("versionId") String versionId) {
        final ProcessDefinition processDefinition = psquared.getProcessDefinition(versionId);
        final List<? extends Transition> transitions = psquared.getUnmatchLatestTransitions(selection.getItems(),
                                                                                            processDefinition,
                                                                                            UNPROCESSED_EXCLUDE_STATES);
        return createSynopses(uriInfo,
                              "selected/unprocessed/",
                              psquared,
                              versionId,
                              null,
                              null,
                              transitions);
    }

    /**
     * Returns the {@link Synopses} of selected items currently waiting to be
     * processed with the specified version.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param selection
     *            the select of one or more items together with a specified version
     *            whose latest state should be returned.
     * @param versionId
     *            the identity of the a version whose current states should be
     *            returned.
     *
     * @return the {@link Synopses} of selected items currently being waiting to be
     *         with the specified version.
     */
    @GET
    @Path("selected/waiting/{versionId}")
    @Produces({ PSquaredType.REALIZED_STATES_XML,
                PSquaredType.REALIZED_STATES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Synopses getSelectedWaiting(@Context UriInfo uriInfo,
                                       Selection selection,
                                       @PathParam("versionId") String versionId) {
        return getMatchLatestTransitions(uriInfo,
                                         "selected/waiting/",
                                         selection.getItems(),
                                         versionId,
                                         State.SUBMITTED.toString());
    }

    /**
     * Returns the all items that are unprocessed with the specified version.
     * Unprocessed is defines as not being in the READY, PROCESSED or ABANDON
     * states.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param versionId
     *            the identity of the a version whose current states should be
     *            returned.
     * @param page
     *            the page, of specified length, from the total collection, to
     *            return.
     * @param length
     *            the length of a page, in number of items.
     *
     * @return the results of the identified completed transaction.
     */
    @GET
    @Path("version/unprocessed/{versionId}")
    @Produces({ PSquaredType.REALIZED_STATES_XML,
                PSquaredType.REALIZED_STATES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Synopses getUnprocessed(@Context UriInfo uriInfo,
                                   @PathParam("versionId") String versionId,
                                   @QueryParam("page") Integer page,
                                   @QueryParam("length") Integer length) {
        final ProcessDefinition processDefinition = psquared.getProcessDefinition(versionId);
        final List<? extends Transition> transitions = psquared.getUnmatchLatestTransitions(processDefinition,
                                                                                            UNPROCESSED_EXCLUDE_STATES,
                                                                                            page,
                                                                                            length);
        return createSynopses(uriInfo,
                              "version/unprocessed/",
                              psquared,
                              versionId,
                              page,
                              length,
                              transitions);
    }

    /**
     * Returns the version.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param versionId
     *            the identity of the a version whose current states should be
     *            returned.
     * @param details
     *            the level of detail of versions within the response. Currently
     *            supported details are "actions", "commands", "reports" and "full".
     *
     * @return the results of the identified completed transaction.
     */
    @GET
    @Path("version/{versionId}")
    @Produces({ PSquaredType.VERSION_XML,
                PSquaredType.VERSION_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Version getVersion(@Context UriInfo uriInfo,
                              @PathParam("versionId") String versionId,
                              @QueryParam("details") String details) {
        final UriHandler responseBaseUri = new PSquaredUriHandler(uriInfo,
                                                                  psquared);
        final ProcessDefinition definition = psquared.getProcessDefinition(versionId);
        return createVersion(responseBaseUri,
                             psquared,
                             definition,
                             details);
    }

    /**
     * Returns the {@link Synopses} of all items currently waiting to be processed
     * with the specified version.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param versionId
     *            the identity of the a version whose current states should be
     *            returned.
     * @param page
     *            the page, of specified length, from the total collection, to
     *            return.
     * @param length
     *            the length of a page, in number of items.
     *
     * @return the {@link Synopses} of all items currently being waiting to be with
     *         the specified version.
     */
    @GET
    @Path("version/waiting/{versionId}")
    @Produces({ PSquaredType.REALIZED_STATES_XML,
                PSquaredType.REALIZED_STATES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Synopses getWaiting(@Context UriInfo uriInfo,
                               @PathParam("versionId") String versionId,
                               @QueryParam("page") Integer page,
                               @QueryParam("length") Integer length) {
        return getMatchLatestTransition(uriInfo,
                                        "version/waiting/",
                                        versionId,
                                        State.SUBMITTED.toString(),
                                        page,
                                        length);
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
