package gov.lbl.nest.psquared.rs;

import java.util.List;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to collect a set of {@link RealizedState} instances for a
 * set of items, via the RESTful interface.
 * 
 * @author patton
 */
@XmlRootElement(name = "realized-states")
@XmlType
public class RealizedStates {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The collection of {@link RealizedState} instances related to this object.
     */
    private List<RealizedState> contents;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected RealizedStates() {
    }

    /**
     * Creates and instance of this class.
     * 
     * @param states
     *            the collection of {@link RealizedState} instances related to this
     *            object.
     */
    RealizedStates(List<RealizedState> states) {
        setContents(states);
    }

    // instance member method (alphabetic)

    /**
     * Returns the collection of {@link RealizedState} instances related to this
     * object.
     * 
     * @return the collection of {@link RealizedState} instances related to this
     *         object.
     */
    @XmlElement(name = "realized-state")
    protected List<RealizedState> getContents() {
        return contents;
    }

    /**
     * Sets the collection of {@link RealizedState} instances related to this
     * object.
     * 
     * @param states
     *            the collection of {@link RealizedState} instances related to this
     *            object.
     */
    protected void setContents(List<RealizedState> states) {
        contents = states;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
