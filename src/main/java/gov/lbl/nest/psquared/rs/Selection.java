package gov.lbl.nest.psquared.rs;

import java.util.List;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to select one or more items together with a specified
 * configuration, via the RESTful interface.
 * 
 * @author patton
 */
@XmlRootElement(name = "selection")
@XmlType(propOrder = { "items" })
public class Selection {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The set of identifiers of the items that make up this selection.
     */
    private List<String> items;

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the set of identifiers of the items that make up this selection.
     * 
     * @return the set of identifiers of the items that make up this selection.
     */
    @XmlElement(name = "item")
    @XmlElementWrapper(name = "items")
    protected List<String> getItems() {
        return items;
    }

    /**
     * Sets the set of identifiers of the items that make up this selection.
     * 
     * @param items
     *            the set of identifiers of the items that make up this selection.
     */
    protected void setItems(List<String> items) {
        this.items = items;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
