package gov.lbl.nest.psquared.rs;

import java.util.Date;

import gov.lbl.nest.psquared.State;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate the entry into a new state via the RESTful
 * interface.
 * 
 * @author patton
 */
@XmlType(propOrder = { "state",
                       "completed",
                       "message",
                       "precedingEntry",
                       "precedingState" })
public class Entry {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The date and time this entry was completed.
     */
    private Date completed;

    /**
     * True if only a limited amount of information is available is in object.
     */
    private boolean condensed = false;

    /**
     * The message, is any, that was associated to this entry.
     */
    private String message;

    /**
     * The {@link PrecedingState} whose exit created this entry.
     */
    private PrecedingState precedingState;

    /**
     * The state that resulted from this object.
     */
    private State state;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Entry() {
    }

    /**
     * Create an instance of this class.
     * 
     * @param condensed
     *            true if only a limited amount of information is available is in
     *            object.
     * @param state
     *            the state that resulted from this object.
     * @param completed
     *            the date and time this entry was completed.
     * @param message
     *            the message, is any, that was associated to this entry.
     * @param precedingState
     *            the {@link PrecedingState} whose exit created this entry.
     */
    Entry(final boolean condensed,
          final State state,
          final Date completed,
          final String message,
          final PrecedingState precedingState) {
        setCompleted(completed);
        setCondensed(condensed);
        if (!condensed) {
            setMessage(message);
        }
        setPrecedingState(precedingState);
        setState(state);
    }

    // instance member method (alphabetic)

    /**
     * Returns the date and time this transition completed.
     * 
     * @return the date and time this transition completed.
     */
    @XmlElement(required = true)
    protected Date getCompleted() {
        return completed;
    }

    /**
     * Returns the message, is any, that was associated to this transition.
     * 
     * @return the message, is any, that was associated to this transition.
     */
    @XmlElement
    protected String getMessage() {
        return message;
    }

    /**
     * Returns the {@link Entry} of the state whose exit created this entry.
     * 
     * @return the {@link Entry} of the state whose exit created this entry.
     */
    @XmlElement(name = "preceding")
    protected Entry getPrecedingEntry() {
        if (condensed && null != precedingState) {
            return precedingState.getEntry();
        }
        return null;
    }

    /**
     * Returns the {@link PrecedingState} whose exit created this entry.
     * 
     * @return the {@link PrecedingState} whose exit created this entry.
     */
    @XmlElement(name = "preceding-state")
    protected PrecedingState getPrecedingState() {
        if (condensed) {
            return null;
        }
        return precedingState;
    }

    /**
     * Returns the state that resulted from this object.
     * 
     * @return the state that resulted from this object.
     */
    @XmlElement(required = true)
    protected State getState() {
        return state;
    }

    /**
     * Returns the date and time this transition completed.
     * 
     * @param completed
     *            the date and time this transition completed.
     */
    protected void setCompleted(final Date completed) {
        this.completed = completed;
    }

    /**
     * Set whether only a limited amount of information is available is in object,
     * or not
     * 
     * @param condensed
     *            true if only a limited amount of information is available is in
     *            object.
     */
    protected void setCondensed(final boolean condensed) {
        this.condensed = condensed;
    }

    /**
     * Sets the message, is any, that was associated to this transition.
     * 
     * @param message
     *            the message, is any, that was associated to this transition.
     */
    protected void setMessage(final String message) {
        this.message = message;
    }

    /**
     * Sets the {@link Entry} of the state whose exit created this entry.
     * 
     * @param entry
     *            the {@link Entry} of the state whose exit created this entry.
     */
    protected void setPrecedingEntry(final Entry entry) {
        this.precedingState = new PrecedingState(entry);
    }

    /**
     * Sets the {@link PrecedingState} whose exit created this entry.
     * 
     * @param precedingState
     *            the {@link PrecedingState} whose exit created this entry.
     */
    protected void setPrecedingState(final PrecedingState precedingState) {
        this.precedingState = precedingState;
    }

    /**
     * Sets the state that resulted from this object.
     * 
     * @param state
     *            the state that resulted from this object.
     */
    protected void setState(final State state) {
        this.state = state;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
