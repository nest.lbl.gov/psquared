package gov.lbl.nest.psquared.rs;

import java.net.URI;
import java.util.List;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate complete information about the a state,
 * realized in the application, of an item within the context of a
 * configuration, via the RESTful interface.
 * 
 * @author patton
 */
@XmlRootElement(name = "realized-state")
@XmlType(propOrder = { "exits",
                       "exited",
                       "item",
                       "configuration" })
public class RealizedState extends
                           PrecedingState {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The URI of the configuration that is the context of this object.
     */
    private URI configuration;

    /**
     * The exit, if any, that was taken to the succeeding realized state.
     */
    private Exit exited;

    /**
     * The set of allowed exits from this object, if none has been taken.
     */
    private List<Exit> exits;

    /**
     * The identifier of the item whose state is captured in this object.
     */
    private String item;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected RealizedState() {
    }

    /**
     * Create an instance of this class.
     * 
     * @param entry
     *            the entry that cause this object to be created.
     * @param item
     *            the identifier of the item whose state is captured in this object
     * @param configuration
     *            the URI of the configuration to which this state belongs.
     */
    RealizedState(Entry entry,
                  String item,
                  URI configuration) {
        super(null,
              entry);
        setConfiguration(configuration);
        setItem(item);
    }

    /**
     * Create an instance of this class.
     * 
     * @param uri
     *            the URI that will return the {@link RealizedState} of this object.
     * @param entry
     *            the entry that cause this object to be created.
     * @param exits
     *            the set of allowed exit transitions from this state.
     * @param exited
     *            the exit taken from this state.
     * @param item
     *            the identifier of the item whose state is captured in this object
     * @param configuration
     *            the URI of the configuration to which this state belongs.
     */
    RealizedState(URI uri,
                  Entry entry,
                  List<Exit> exits,
                  Exit exited,
                  String item,
                  URI configuration) {
        super(uri,
              entry);
        setConfiguration(configuration);
        setEntry(entry);
        setExited(exited);
        setExits(exits);
        setItem(item);
    }

    // instance member method (alphabetic)

    /**
     * Returns the URI of the configuration that is the context of this object.
     * 
     * @return the URI of the configuration that is the context of this object.
     */
    @XmlElement
    public URI getConfiguration() {
        return configuration;
    }

    /**
     * Returns the exit, if any, that was taken to the succeeding realized state.
     * 
     * @return the exit, if any, that was taken to the succeeding realized state.
     */
    @XmlElement
    public Exit getExited() {
        return exited;
    }

    /**
     * Returns the set of allowed exits from this object, if none has been taken.
     * 
     * @return the set of allowed exits from this object, if none has been taken.
     */
    @XmlElement(name = "exit")
    @XmlElementWrapper(name = "exits")
    public List<Exit> getExits() {
        return exits;
    }

    /**
     * Returns the identifier of the item whose state is captured in this object.
     * 
     * @return the identifier of the item whose state is captured in this object.
     */
    @XmlElement
    public String getItem() {
        return item;
    }

    /**
     * Returns the URI of this resource.
     * 
     * @return the URI of this resource.
     */
    @Override
    public URI getUri() {
        return super.getUri();
    }

    /**
     * Sets the URI of the configuration that is the context of this object.
     * 
     * @param uri
     *            the URI of the configuration that is the context of this object.
     */
    protected void setConfiguration(URI uri) {
        configuration = uri;
    }

    /**
     * Sets the exit, if any, that was taken to the succeeding realized state.
     * 
     * @param exited
     *            the exit, if any, that was taken to the succeeding realized state.
     */
    protected void setExited(Exit exited) {
        this.exited = exited;
    }

    /**
     * Sets the set of allowed exits from this object, if none has been taken.
     * 
     * @param exits
     *            the set of allowed exits from this object, if none has been taken.
     */
    protected void setExits(List<Exit> exits) {
        this.exits = exits;
    }

    /**
     * Sets the identifier of the item whose state is captured in this object.
     * 
     * @param identify
     *            the identifier of the item whose state is captured in this object.
     */
    protected void setItem(String identify) {
        item = identify;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
