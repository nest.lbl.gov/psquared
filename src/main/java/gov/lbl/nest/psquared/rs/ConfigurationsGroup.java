package gov.lbl.nest.psquared.rs;

import java.util.List;

import gov.lbl.nest.common.rs.NamedResource;
import gov.lbl.nest.common.rs.NamedResourcesGroup;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, the group of
 * available configurations as {@link NamedResource} instances.
 * 
 * @author patton
 */
@XmlType(propOrder = { "configurations" })
@XmlSeeAlso(value = { Configuration.class })
public class ConfigurationsGroup extends
                                 NamedResourcesGroup {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected ConfigurationsGroup() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param name
     *            the name by which the group should be referenced.
     * @param description
     *            the short description of the group.
     * @param resources
     *            the {@link NamedResource} instances that are in this group.
     */
    public ConfigurationsGroup(final String name,
                               final String description,
                               final List<? extends NamedResource> resources) {
        super(name,
              description,
              resources);
    }

    // instance member method (alphabetic)

    /**
     * Returns the {@link NamedResource} instances that are the configurations in
     * this group.
     * 
     * @return the {@link NamedResource} instances that are the configurations in
     *         this group.
     */
    @XmlElement(name = "configuration")
    protected List<? extends NamedResource> getConfigurations() {
        return getResources();
    }

    /**
     * Sets the {@link NamedResource} instances that are the configurations in this
     * group.
     * 
     * @param configurations
     *            the {@link NamedResource} instances that are the configurations in
     *            this group.
     */
    protected void setConfigurations(final List<? extends NamedResource> configurations) {
        setResources(configurations);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
