package gov.lbl.nest.psquared.rs;

import java.util.List;

import gov.lbl.nest.common.rs.NamedResource;
import gov.lbl.nest.common.rs.NamedResourcesGroup;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, the group of
 * available schedulers as {@link NamedResource} instances.
 * 
 * @author patton
 */
@XmlType(propOrder = { "schedulers" })
public class SchedulersGroup extends
                             NamedResourcesGroup {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected SchedulersGroup() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param name
     *            the name by which the group should be referenced.
     * @param description
     *            the short description of the group.
     * @param resources
     *            the {@link NamedResource} instances that are in this group.
     */
    public SchedulersGroup(final String name,
                           final String description,
                           final List<? extends NamedResource> resources) {
        super(name,
              description,
              resources);
    }

    // instance member method (alphabetic)

    /**
     * Returns the {@link NamedResource} instances that are the schedulers in this
     * group.
     * 
     * @return the {@link NamedResource} instances that are the schedulers in this
     *         group.
     */
    @XmlElement(name = "scheduler")
    protected List<? extends NamedResource> getSchedulers() {
        return getResources();
    }

    /**
     * Sets the {@link NamedResource} instances that are the schedulers in this
     * group.
     * 
     * @param schedulers
     *            the {@link NamedResource} instances that are the schedulers in
     *            this group.
     */
    protected void setSchedulers(final List<? extends NamedResource> schedulers) {
        setResources(schedulers);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
