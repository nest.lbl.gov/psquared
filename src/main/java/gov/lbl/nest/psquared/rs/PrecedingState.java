package gov.lbl.nest.psquared.rs;

import java.net.URI;

import gov.lbl.nest.common.rs.Resource;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate the receding state of a pairing.
 * 
 * @author patton
 */
@XmlType(propOrder = { "entry" })
public class PrecedingState extends
                            Resource {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The entry into the state captured in this object.
     */
    private Entry entry;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected PrecedingState() {
    }

    /**
     * Create an instance of this class.
     * 
     * @param entry
     *            the entry that cause this object to be created.
     */
    PrecedingState(Entry entry) {
        super(null);
        setEntry(entry);
        entry.setCondensed(true);
    }

    /**
     * Create an instance of this class.
     * 
     * @param uri
     *            the URI that will return the {@link RealizedState} of this object.
     * @param entry
     *            the entry that cause this object to be created.
     */
    PrecedingState(URI uri,
                   Entry entry) {
        super(uri);
        setEntry(entry);
    }
    // instance member method (alphabetic)

    /**
     * Returns the entry that cause this object to be created.
     * 
     * @return the entry that cause this object to be created.
     */
    @XmlElement(required = true)
    public Entry getEntry() {
        return entry;
    }

    /**
     * Sets the entry that cause this object to be created.
     * 
     * @param entry
     *            the entry that cause this object to be created.
     */
    protected void setEntry(Entry entry) {
        this.entry = entry;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
