package gov.lbl.nest.psquared.rs;

import gov.lbl.nest.common.watching.Digest;

/**
 * The class contains the media types used by the RESTful interface.
 * 
 * @author patton
 * 
 */
public class PSquaredType {

    /**
     * The media type for the XML representation of an {@link Application} instance.
     */
    public static final String APPLICATION_XML = "application/gov.lbl.nest.psquared.rs.Application+xml";

    /**
     * The media type for the JSON representation of an {@link Application}
     * instance.
     */
    public static final String APPLICATION_JSON = "application/gov.lbl.nest.psquared.rs.Application+json";

    /**
     * The media type for the XML representation of an {@link Attachment} instance.
     */
    public static final String ATTACHMENT_XML = "application/gov.lbl.nest.psquared.rs.Attachment+xml";

    /**
     * The media type for the JSON representation of an {@link Application}
     * instance.
     */
    public static final String ATTACHMENT_JSON = "application/gov.lbl.nest.psquared.rs.Attachment+json";

    /**
     * The media type for the XML representation of an {@link Configuration}
     * instance.
     */
    public static final String CONFIGURATION_XML = "application/gov.lbl.nest.psquared.rs.Configuration+xml";

    /**
     * The media type for the JSON representation of an {@link Configuration}
     * instance.
     */
    public static final String CONFIGURATION_JSON = "application/gov.lbl.nest.psquared.rs.Configuration+json";

    /**
     * The media type for the XML representation of an {@link Digest} instance.
     */
    public static final String DIGEST_XML = "application/gov.lbl.nest.common.watching.Digest+xml";

    /**
     * The media type for the JSON representation of an {@link Digest} instance.
     */
    public static final String DIGEST_JSON = "application/gov.lbl.nest.common.watching.Digest+json";

    /**
     * The media type for the XML representation of an {@link Histories} instance.
     */
    public static final String HISTORIES_XML = "application/gov.lbl.nest.psquared.rs.Histories+xml";

    /**
     * The media type for the JSON representation of an {@link Configuration}
     * instance.
     */
    public static final String HISTORIES_JSON = "application/gov.lbl.nest.psquared.rs.Histories+json";

    /**
     * The media type for the XML representation of an {@link History} instance.
     */
    public static final String HISTORY_XML = "application/gov.lbl.nest.psquared.rs.History+xml";

    /**
     * The media type for the JSON representation of an {@link Configuration}
     * instance.
     */
    public static final String HISTORY_JSON = "application/gov.lbl.nest.psquared.rs.History+json";

    /**
     * The media type for the XML representation of an {@link RealizedState}
     * instance.
     */
    public static final String REALIZED_STATE_XML = "application/gov.lbl.nest.psquared.rs.RealizedState+xml";

    /**
     * The media type for the JSON representation of an {@link RealizedState}
     * instance.
     */
    public static final String REALIZED_STATE_JSON = "application/gov.lbl.nest.psquared.rs.RealizedState+json";

    /**
     * The media type for the XML representation of an {@link RealizedStates}
     * instance.
     */
    public static final String REALIZED_STATES_XML = "application/gov.lbl.nest.psquared.rs.RealizedStates+xml";

    /**
     * The media type for the JSON representation of an {@link RealizedStates}
     * instance.
     */
    public static final String REALIZED_STATES_JSON = "application/gov.lbl.nest.psquared.rs.RealizedStates+json";

    /**
     * The media type for the XML representation of an {@link Selection} instance.
     */
    public static final String SELECTION_XML = "application/gov.lbl.nest.psquared.rs.Selection+xml";

    /**
     * The media type for the JSON representation of an {@link Configuration}
     * instance.
     */
    public static final String SELECTION_JSON = "application/gov.lbl.nest.psquared.rs.Selection+json";

    /**
     * The media type for the XML representation of an {@link Submission} instance.
     */
    public static final String SUBMISSION_XML = "application/gov.lbl.nest.psquared.rs.Submission+xml";

    /**
     * The media type for the JSON representation of an {@link Submission} instance.
     */
    public static final String SUBMISSION_JSON = "application/gov.lbl.nest.psquared.rs.Submission+json";

    /**
     * The media type for the XML representation of an {@link Version} instance.
     */
    public static final String VERSION_XML = "application/gov.lbl.nest.psquared.rs.Version+xml";

    /**
     * The media type for the JSON representation of an {@link Configuration}
     * instance.
     */
    public static final String VERSION_JSON = "application/gov.lbl.nest.psquared.rs.Version+json";

}
