package gov.lbl.nest.psquared.rs;

import java.util.Date;

import gov.lbl.nest.psquared.State;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate limited information about the a state,
 * realized in the application, of an item within the context of a
 * configuration, via the RESTful interface.
 * 
 * @author patton
 */
@XmlType(propOrder = { "item" })
public class Synopsis extends
                      Entry {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The identifier of the item whose state is captured in this object.
     */
    private String item;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Synopsis() {
    }

    /**
     * Create an instance of this class.
     * 
     * @param item
     *            the identifier of the item whose state is captured in this object.
     * @param state
     *            the state that resulted from this object.
     * @param completed
     *            the date and time this entry was completed.
     * @param message
     *            the message, is any, that was associated to this entry.
     * @param precedingState
     *            the {@link PrecedingState} whose exit created this entry.
     */
    Synopsis(final String item,
             final State state,
             final Date completed,
             final String message,
             final PrecedingState precedingState) {
        super(true,
              state,
              completed,
              null,
              precedingState);
        setItem(item);
    }

    // instance member method (alphabetic)

    /**
     * Returns the identifier of the item whose state is captured in this object.
     * 
     * @return the identifier of the item whose state is captured in this object.
     */
    @XmlElement
    public String getItem() {
        return item;
    }

    /**
     * Sets the identifier of the item whose state is captured in this object.
     * 
     * @param identity
     *            the identifier of the item whose state is captured in this object.
     */
    protected void setItem(String identity) {
        item = identity;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
