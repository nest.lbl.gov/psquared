package gov.lbl.nest.psquared.rs;

import java.net.URI;
import java.util.List;

import gov.lbl.nest.common.rs.NamedResource;
import gov.lbl.nest.psquared.Transition;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class represents a set of processing definitions.
 * 
 * @author patton
 */
@XmlRootElement(name = "configuration")
@XmlType(propOrder = { "last",
                       "defaultVersion",
                       "versions" })
public class Configuration extends
                           NamedResource {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The URI of the version to use is none is explicitly requested.
     */
    private URI defaultVersion;

    /**
     * The level of detail the representation of this object.
     */
    private String details;

    /**
     * The {@link Synopsis} instance of the most recent {@link Transition} instance
     * associated with this object.
     */
    private Synopsis last;

    /**
     * The collection of versions associated with this object.
     */
    private List<Version> versions;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Configuration() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param uri
     *            the URI that will refresh the client representation of this this
     *            object.
     * @param details
     *            the level of detail of versions within the response. Currently
     *            supported details are "last", "actions", "commands", "reports" and
     *            "full".
     * @param name
     *            the name of this object, unique within the owning application.
     * @param description
     *            the human friendly description of this resource.
     * @param last
     *            the {@link Synopsis} instance of the most recent
     *            {@link Transition} instance associated with this object.
     * @param versions
     *            the set of versions of this set of processing definition.
     * @param defaultDefinition
     *            the URI of the version to use is none is explicitly requested.
     */
    Configuration(URI uri,
                  String details,
                  String name,
                  String description,
                  Synopsis last,
                  URI defaultVersion,
                  List<Version> versions) {
        super(uri,
              name,
              description);
        setDefaultVersion(defaultVersion);
        setDetails(details);
        setLast(last);
        setVersions(versions);
    }

    // instance member method (alphabetic)

    /**
     * Returns the URI of the version to use is none is explicitly requested.
     * 
     * @return the URI of the version to use is none is explicitly requested.
     */
    @XmlElement(name = "default-version")
    public URI getDefaultVersion() {
        return defaultVersion;
    }

    /**
     * Returns the level of detail the representation of this object.
     * 
     * @return the level of detail the representation of this object.
     */
    @XmlAttribute
    protected String getDetails() {
        return details;
    }

    /**
     * Returns the {@link Synopsis} instance of the most recent {@link Transition}
     * instance associated with this object.
     * 
     * @return the {@link Synopsis} instance of the most recent {@link Transition}
     *         instance associated with this object.
     */
    @XmlElement
    protected Synopsis getLast() {
        return last;
    }

    /**
     * Returns the URI of this resource.
     * 
     * @return the URI of this resource.
     */
    @Override
    public URI getUri() {
        return super.getUri();
    }

    /**
     * Returns the collection of versions associated with this object.
     * 
     * @return the collection of versions associated with this object.
     */
    @XmlElement(name = "known-version")
    @XmlElementWrapper(name = "known-versions")
    protected List<Version> getVersions() {
        return versions;
    }

    /**
     * Sets the URI of the version to use is none is explicitly requested.
     * 
     * @param uri
     *            the URI of the version to use is none is explicitly requested.
     */
    protected void setDefaultVersion(URI uri) {
        defaultVersion = uri;
    }

    /**
     * Sets the level of detail the representation of this object.
     * 
     * @param details
     *            the level of detail the representation of this object.
     */
    protected void setDetails(String details) {
        this.details = details;
    }

    /**
     * Sets the {@link Synopsis} instance of the most recent {@link Transition}
     * instance associated with this object.
     * 
     * @param last
     *            the {@link Synopsis} instance of the most recent
     *            {@link Transition} instance associated with this object.
     */
    protected void setLast(Synopsis last) {
        this.last = last;
    }

    /**
     * Returns the collection of versions instances associated with this object.
     * 
     * @param versions
     *            the collection of versions instances associated with this object.
     */
    protected void setVersions(List<Version> versions) {
        this.versions = versions;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
