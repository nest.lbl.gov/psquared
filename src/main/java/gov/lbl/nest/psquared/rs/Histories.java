package gov.lbl.nest.psquared.rs;

import java.util.List;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate the history of a set of items within the
 * context of a configuration, via the RESTful interface.
 * 
 * @author patton
 */
@XmlRootElement(name = "histories")
@XmlType
public class Histories {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The set of {@link History} instances that make up the history.
     */
    private List<History> itemHistories;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected Histories() {
    }

    /**
     * Create an instance of this class.
     * 
     * @param histories
     *            the set of {@link History} instances that make up the history.
     */
    Histories(List<History> histories) {
        setItemHistories(histories);
    }

    // instance member method (alphabetic)

    /**
     * Returns the set of {@link History} instances that make up the history.
     * 
     * @return the set of {@link History} instances that make up the history.
     */
    @XmlElement(name = "history")
    protected List<History> getItemHistories() {
        return itemHistories;
    }

    /**
     * Sets the set of {@link History} instances that make up the history.
     * 
     * @param histories
     *            the set of {@link History} instances that make up the history.
     */
    protected void setItemHistories(List<History> histories) {
        itemHistories = histories;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
