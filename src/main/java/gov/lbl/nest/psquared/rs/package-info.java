/**
 * This package implements the classes used to support the RESTful interface to
 * the PSquared application.
 * 
 * @author patton
 *
 */
package gov.lbl.nest.psquared.rs;