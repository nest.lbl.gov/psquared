package gov.lbl.nest.psquared.rs;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.psquared.AlreadyExitedException;
import gov.lbl.nest.psquared.ExistingEntityException;
import gov.lbl.nest.psquared.Family;
import gov.lbl.nest.psquared.ForbiddenTransitionException;
import gov.lbl.nest.psquared.KnownScheduler;
import gov.lbl.nest.psquared.PSquared;
import gov.lbl.nest.psquared.ProcessDefinition;
import gov.lbl.nest.psquared.StateMachineError;
import gov.lbl.nest.psquared.Transition;
import gov.lbl.nest.psquared.UriHandler;
import gov.lbl.nest.psquared.VetoedException;
import gov.lbl.nest.psquared.rs.ReportPSquared.PSquaredUriHandler;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.UriInfo;

/**
 * The class provides a RESTful interface to manipulate the PSquared
 * application.
 * 
 * @author patton
 * 
 * @see ReportPSquared
 */
@Path("command")
@Stateless
public class CommandPSquared {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The value to return no details in the response.
     */
    private static final String DETAILS_NONE = "None";

    /**
     * The {@link Logger} instance used bu this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(CommandPSquared.class);

    // private static member data

    // private instance member data

    /**
     * The {@link PSquared} instance used by this object.
     */
    @Inject
    private PSquared psquared;

    // constructors

    // instance member method (alphabetic)

    /**
     * Create a new Submits a item/configuration pairing for processing.
     * 
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param configuration
     *            the {@link Configuration} instance describing the family to be
     *            created.
     * 
     * @return the resulting {@link RealizedState}
     */
    @POST
    @Path("create/configuration")
    @Consumes({ PSquaredType.CONFIGURATION_XML,
                PSquaredType.CONFIGURATION_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ PSquaredType.CONFIGURATION_XML,
                PSquaredType.CONFIGURATION_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response createConfiguration(@Context UriInfo uriInfo,
                                        Configuration configuration) {

        ResponseBuilder builder;
        try {
            final Family result = psquared.createFamily(configuration.getName(),
                                                        configuration.getDescription());
            final UriHandler responseBaseUri = new PSquaredUriHandler(uriInfo,
                                                                      psquared);
            final Configuration config = ReportPSquared.createConfiguration(responseBaseUri,
                                                                            psquared,
                                                                            result,
                                                                            ReportPSquared.FULL_DETAILS);
            builder = Response.created(config.getUri());
            builder.entity(config);
        } catch (ExistingEntityException e) {
            LOG.error(e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST);
        } catch (Exception e) {
            LOG.error("Failed when attempting to create a Family");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Create a new Submits a item/configuration pairing for processing.
     * 
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param version
     *            the {@link Version} instance describing the process definition to
     *            be created.
     * 
     * @return the resulting {@link RealizedState}
     */
    @POST
    @Path("create/version")
    @Consumes({ PSquaredType.VERSION_XML,
                PSquaredType.VERSION_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ PSquaredType.VERSION_XML,
                PSquaredType.VERSION_XML,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response createVersion(@Context UriInfo uriInfo,
                                  Version version) {

        ResponseBuilder builder;
        try {
            final UriHandler uriBuilder = new PSquaredUriHandler(uriInfo,
                                                                 psquared);
            final Boolean development = version.isDevelopment();
            final boolean developmentToUse;
            if (null == development) {
                developmentToUse = false;
            } else {
                developmentToUse = version.isDevelopment();
            }
            final Commands commands = version.getCommands();
            final ProcessDefinition result;
            if (null == commands) {
                result = psquared.createProcessDefinition(version.getName(),
                                                          version.getDescription(),
                                                          version.getConfiguration(),
                                                          null,
                                                          null,
                                                          null,
                                                          null,
                                                          version.getScheduler(),
                                                          developmentToUse,
                                                          uriBuilder);
            } else {
                result = psquared.createProcessDefinition(version.getName(),
                                                          version.getDescription(),
                                                          version.getConfiguration(),
                                                          commands.getProcessCmd(),
                                                          commands.getSuccessCmd(),
                                                          commands.getFailureCmd(),
                                                          commands.getArgs(),
                                                          version.getScheduler(),
                                                          developmentToUse,
                                                          uriBuilder);
            }
            final Version vers = ReportPSquared.createVersion(uriBuilder,
                                                              psquared,
                                                              result,
                                                              ReportPSquared.FULL_DETAILS);
            builder = Response.created(vers.getUri());
            builder.entity(vers);
        } catch (ExistingEntityException e) {
            LOG.error(e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST);
        } catch (Exception e) {
            LOG.error("Failed when attempting to create a Family");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Transitions an item/configuration pairing to the specified state.
     * 
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param transitionId
     *            the identity of the previously completed transition for the
     *            item/configuration pairing.
     * @param exit
     *            the name of the exit to take out of the state that resulted from
     *            the previously completed transition.
     * @param attachment
     *            the {@link Attachment} instance, if any containing the message, if
     *            any, to associate with the new transition if it completes.
     * @param details
     *            the level of detail in the response. "None" will produce no
     *            response, and other value will produce a standard one.
     * 
     * @return the options for exiting the resultant state.
     */
    @POST
    @Path("exit/{transitionId}/{exit}")
    @Consumes({ PSquaredType.ATTACHMENT_XML,
                PSquaredType.APPLICATION_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ PSquaredType.REALIZED_STATE_XML,
                PSquaredType.REALIZED_STATE_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response exitState(@Context UriInfo uriInfo,
                              @PathParam("transitionId") String transitionId,
                              @PathParam("exit") String exit,
                              Attachment attachment,
                              @QueryParam("details") String details) {
        LOG.debug("Entering \"exitState\" for " + transitionId);
        final String messageToUse;
        if (null == attachment || null == attachment.getMessage()) {
            messageToUse = null;
        } else {
            messageToUse = attachment.getMessage();
        }
        final UriHandler responseBaseUri = new ReportPSquared.PSquaredUriHandler(uriInfo,
                                                                                 psquared);
        ResponseBuilder builder;
        try {
            final Transition transition = psquared.exitState(responseBaseUri.buildFromIdentity(transitionId),
                                                             exit,
                                                             messageToUse,
                                                             responseBaseUri);
            if (null == transition) {
                builder = Response.status(Response.Status.NOT_FOUND);
            } else {
                builder = Response.status(Response.Status.OK);
                if (null != details && DETAILS_NONE.equalsIgnoreCase(details)) {
                    builder = Response.created(responseBaseUri.buildFromIdentity(transition.getIdentity()));
                    // Do nothing more
                } else {
                    final RealizedState realizedState = ReportPSquared.createRealizedState(responseBaseUri,
                                                                                           psquared,
                                                                                           transition,
                                                                                           1);
                    builder = Response.created(realizedState.getUri());
                    builder.entity(realizedState);
                }
            }
        } catch (StateMachineError e) {
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        } catch (ForbiddenTransitionException e) {
            builder = Response.status(Response.Status.NOT_FOUND);
            if (null != details && DETAILS_NONE.equalsIgnoreCase(details)) {
                // Do nothing more
            } else {
                builder.entity(ReportPSquared.createRealizedState(responseBaseUri,
                                                                  psquared,
                                                                  e.getTransition(),
                                                                  0));
            }
        } catch (AlreadyExitedException e) {
            builder = Response.status(Response.Status.CONFLICT);
            if (null != details && DETAILS_NONE.equalsIgnoreCase(details)) {
                // Do nothing more
            } else {
                builder.entity(ReportPSquared.createRealizedState(responseBaseUri,
                                                                  psquared,
                                                                  e.getTransition(),
                                                                  0));
            }
        }
        LOG.debug("Exit \"exitState\" for " + transitionId);
        return builder.build();
    }

    /**
     * Submits a item/configuration pairing for processing.
     * 
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param versionId
     *            the identity of the a versionId to which the item should be
     *            submitted.
     * @param submission
     *            the set one or more items for processing with a specified
     *            configuration.
     * @param veto
     *            the name of the veto, is any, to apply to the submission.
     * @param details
     *            the level of detail in the response. "None" will produce on
     *            response, and other value will produce a standard one.
     * 
     * @return the resulting {@link RealizedState}
     */
    @POST
    @Path("submit/{versionId}")
    @Consumes({ PSquaredType.SUBMISSION_XML,
                PSquaredType.SUBMISSION_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ PSquaredType.REALIZED_STATES_XML,
                PSquaredType.REALIZED_STATES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response submit(@Context UriInfo uriInfo,
                           @PathParam("versionId") String versionId,
                           Submission submission,
                           @QueryParam("veto") String veto,
                           @QueryParam("details") String details) {
        LOG.debug("Entering \"submit\"");
        ResponseBuilder builder;
        try {
            final ProcessDefinition processDefinition = psquared.getProcessDefinition(versionId);
            final List<RealizedState> states;
            if (null != details && DETAILS_NONE.equalsIgnoreCase(details)) {
                states = null;
            } else {
                states = new ArrayList<RealizedState>();
            }
            final URI uri = submission.getScheduler();
            final KnownScheduler schedulerToUse;
            if (null == uri) {
                schedulerToUse = null;
            } else {
                final String path = uri.getPath();
                final int beginIndex = path.lastIndexOf("/");
                schedulerToUse = psquared.getScheduler(path.substring(beginIndex + 1));
            }
            final UriHandler responseBaseUri = new ReportPSquared.PSquaredUriHandler(uriInfo,
                                                                                     psquared);
            for (String item : submission.getItems()) {
                try {
                    final Transition transition = psquared.submitPairing(item,
                                                                         processDefinition,
                                                                         submission.getMessage(),
                                                                         veto,
                                                                         schedulerToUse,
                                                                         responseBaseUri);
                    if (null != states) {
                        final RealizedState realizedState = ReportPSquared.createRealizedState(responseBaseUri,
                                                                                               psquared,
                                                                                               transition,
                                                                                               0);
                        if (null != realizedState) {
                            states.add(realizedState);
                        }
                    }
                } catch (VetoedException e) {
                    LOG.warn("Item \"" + item
                             + "\" was vetoed using the \""
                             + veto
                             + "\" veto");
                    if (null != states) {
                        states.add(ReportPSquared.createVetoed(responseBaseUri,
                                                               item,
                                                               processDefinition,
                                                               e.getMessage()));
                    }
                } catch (ForbiddenTransitionException e) {
                    final Transition transition = e.getTransition();
                    final String label;
                    if (null == transition) {
                        label = "Initial Transition";
                    } else {
                        label = "Transition \"" + transition.getIdentity()
                                + "\"";
                    }
                    LOG.warn(label + ": "
                             + e.getMessage());
                    if (null != states) {
                        states.add(ReportPSquared.createRealizedState(responseBaseUri,
                                                                      psquared,
                                                                      e.getTransition(),
                                                                      0));
                    }
                } catch (AlreadyExitedException e) {
                    LOG.warn(e.getMessage());
                    if (null != states) {

                        states.add(ReportPSquared.createRealizedState(responseBaseUri,
                                                                      psquared,
                                                                      e.getTransition(),
                                                                      0));
                    }
                }
            }
            builder = Response.status(Response.Status.OK);
            if (null != states) {
                builder.entity(new RealizedStates(states));
            }
        } catch (Exception e) {
            LOG.error("Failed when attempting to submit a set of items");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        LOG.debug("Exiting \"submit\"");
        return builder.build();
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
