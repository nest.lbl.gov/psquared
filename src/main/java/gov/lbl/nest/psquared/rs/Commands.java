package gov.lbl.nest.psquared.rs;

import java.text.MessageFormat;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate the commands associated with a processing
 * definition via the RESTful interface.
 * 
 * @author patton
 */
@XmlType(propOrder = { "processCmd",
                       "successCmd",
                       "failureCmd",
                       "args" })
public class Commands {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link MessageFormat} string that will be any command's arguments.
     */
    private String args;

    /**
     * The command the will be run after processing has failed.
     */
    private String failureCmd;

    /**
     * The command that will be run to process an item.
     */
    private String processCmd;

    /**
     * The command the will be run after processing has succeeded.
     */
    private String successCmd;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Commands() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param processCmd
     *            the command that will be run to process an item.
     * @param successCmd
     *            the command the will be run after processing has succeeded.
     * @param failureCmd
     *            the command the will be run after processing has failed.
     * @param args
     *            the {@link MessageFormat} string that will be any command's
     *            arguments.
     */
    public Commands(final String processCmd,
                    final String successCmd,
                    final String failureCmd,
                    final String args) {
        setArgs(args);
        setFailureCmd(failureCmd);
        setProcessCmd(processCmd);
        setSuccessCmd(successCmd);
    }

    // instance member method (alphabetic)

    /**
     * Returns the {@link MessageFormat} string that will be any command's
     * arguments.
     * 
     * @return the {@link MessageFormat} string that will be any command's
     *         arguments.
     */
    @XmlElement
    public String getArgs() {
        return args;
    }

    /**
     * Returns the command the will be run after processing has failed.
     * 
     * @return the command the will be run after processing has failed.
     */
    @XmlElement(name = "failure")
    public String getFailureCmd() {
        return failureCmd;
    }

    /**
     * Returns the command that will be run to process an item.
     * 
     * @return the command that will be run to process an item.
     */
    @XmlElement(name = "process")
    public String getProcessCmd() {
        return processCmd;
    }

    /**
     * Returns the command the will be run after processing has succeeded.
     * 
     * @return the command the will be run after processing has succeeded.
     */
    @XmlElement(name = "success")
    public String getSuccessCmd() {
        return successCmd;
    }

    /**
     * Stes the {@link MessageFormat} string that will be any command's arguments.
     * 
     * @param args
     *            the {@link MessageFormat} string that will be any command's
     *            arguments.
     */
    public void setArgs(String args) {
        this.args = args;
    }

    /**
     * Sets the command the will be run after processing has failed.
     * 
     * @param command
     *            the command the will be run after processing has failed.
     */
    public void setFailureCmd(String command) {
        failureCmd = command;
    }

    /**
     * Sets the command that will be run to process an item.
     * 
     * @param command
     *            the command that will be run to process an item.
     */
    public void setProcessCmd(String command) {
        processCmd = command;
    }

    /**
     * Sets the command the will be run after processing has succeeded.
     * 
     * @param command
     *            the command the will be run after processing has succeeded.
     */
    public void setSuccessCmd(String command) {
        successCmd = command;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
