package gov.lbl.nest.psquared.rs;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class contains the message to be associated with the creation of a new
 * Realized State.
 * 
 * @author patton
 */
@XmlRootElement(name = "attachment")
@XmlType
public class Attachment {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The message to associate with the creation of a new Realized State.
     */
    private String message;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Attachment() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param message
     *            the message to associate with the creation of a new Realized
     *            State.
     */
    Attachment(String message) {
        setMessage(message);
    }

    // instance member method (alphabetic)

    /**
     * Returns the message to associate with the creation of a new Realized State.
     * 
     * @return the message to associate with the creation of a new Realized State.
     */
    @XmlElement
    protected String getMessage() {
        return message;
    }

    /**
     * Sets the message to associate with the creation of a new Realized State.
     * 
     * @param message
     *            the message to associate with the creation of a new Realized
     *            State.
     */
    protected void setMessage(String message) {
        this.message = message;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
