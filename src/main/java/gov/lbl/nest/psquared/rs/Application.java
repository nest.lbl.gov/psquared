package gov.lbl.nest.psquared.rs;

import java.net.URI;
import java.util.List;

import gov.lbl.nest.common.rs.Resource;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class represents a user's entry point to all accessible resources in the
 * application.
 * 
 * @author patton
 */
@XmlRootElement(name = "application")
@XmlType(propOrder = { "actions",
                       "configurations",
                       "schedulers",
                       "specification",
                       "implementation" })
public class Application extends
                         Resource {

    /**
     * The collection of {@link ActionsGroup} instances that can be used to initiate
     * a action, via a POST.
     */
    private List<ActionsGroup> actions;

    /**
     * The collection of configurations associated with this object.
     */
    private ConfigurationsGroup configurations;

    /**
     * The implementation version of this application.
     */
    private String implementation;

    /**
     * The collection of scheduler associated with this object.
     */
    private SchedulersGroup schedulers;

    /**
     * The specification version this application implements.
     */
    private String specification;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Application() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param uri
     *            the URI of this object.
     * @param commands
     *            the collection of {@link ActionsGroup} instances that can be used
     *            to initiate a command, via a POST.
     * @param configurations
     *            the collection of configurations associated with this object.
     * @param schedulers
     *            the collection of scheduler associated with this object.
     * @param specification
     *            the specification version this application implements.
     * @param implementation
     *            the implementation version of this application.
     */
    Application(final URI uri,
                final List<ActionsGroup> commands,
                final ConfigurationsGroup configurations,
                final SchedulersGroup schedulers,
                final String specification,
                final String implementation) {
        super(uri);
        setActions(commands);
        setConfigurations(configurations);
        setImplementation(implementation);
        setSchedulers(schedulers);
        setSpecification(specification);
    }

    // instance member method (alphabetic)

    /**
     * Returns the collection of {@link ActionsGroup} instances that can be used to
     * initiate a command, via a POST.
     * 
     * @return the collection of {@link ActionsGroup} instances that can be used to
     *         initiate a command, via a POST.
     */
    @XmlElement
    protected List<ActionsGroup> getActions() {
        return actions;
    }

    /**
     * Returns the collection of configurations associated with this object.
     * 
     * @return the collection of configurations associated with this object.
     */
    @XmlElement
    protected ConfigurationsGroup getConfigurations() {
        return configurations;
    }

    /**
     * Returns the implementation version of this application.
     * 
     * @return the implementation version of this application.
     */
    @XmlElement
    protected String getImplementation() {
        return implementation;
    }

    /**
     * Returns the collection of scheduler associated with this object.
     * 
     * @return the collection of scheduler associated with this object.
     */
    @XmlElement
    protected SchedulersGroup getSchedulers() {
        return schedulers;
    }

    /**
     * Returns the specification version this application implements.
     * 
     * @return the specification version this application implements.
     */
    @XmlElement
    protected String getSpecification() {
        return specification;
    }

    /**
     * Sets the collection of {@link ActionsGroup} instances that can be used to
     * initiate a command, via a POST.
     * 
     * @param actions
     *            the collection of {@link ActionsGroup} instances that can be used
     *            to initiate a command, via a POST.
     */
    protected void setActions(final List<ActionsGroup> actions) {
        this.actions = actions;
    }

    /**
     * Sets the collection of configurations associated with this object.
     * 
     * @param configurations
     *            the collection of configurations associated with this object.
     */
    protected void setConfigurations(final ConfigurationsGroup configurations) {
        this.configurations = configurations;
    }

    /**
     * Sets the implementation version of this application.
     * 
     * @param implementation
     *            the implementation version of this application.
     */
    protected void setImplementation(final String implementation) {
        this.implementation = implementation;
    }

    /**
     * Returns the collection of scheduler associated with this object.
     * 
     * @param schedulers
     *            the collection of scheduler associated with this object.
     */
    protected void setSchedulers(final SchedulersGroup schedulers) {
        this.schedulers = schedulers;
    }

    /**
     * Sets the specification version this application implements.
     * 
     * @param specification
     *            the specification version this application implements.
     */
    protected void setSpecification(final String specification) {
        this.specification = specification;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
