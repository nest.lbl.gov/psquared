package gov.lbl.nest.psquared.rs;

import java.net.URI;

import gov.lbl.nest.common.rs.Resource;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate the history of an item within the context
 * of a configuration, via the RESTful interface.
 * 
 * @author patton
 */
@XmlRootElement(name = "history")
@XmlType(propOrder = { "currentState" })
public class History extends
                     Resource {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The current state of this pairing.
     */
    private RealizedState currentState;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected History() {
    }

    /**
     * Create an instance of this class.
     * 
     * @param uri
     *            the URI to GET to in order to refresh this object.
     * @param state
     *            the current state of this pairing.
     */
    History(URI uri,
            RealizedState state) {
        super(uri);
        setCurrentState(state);
    }

    // instance member method (alphabetic)

    /**
     * Returns the current state of this pairing.
     * 
     * @return the current state of this pairing.
     */
    @XmlElement(name = "current-state")
    public RealizedState getCurrentState() {
        return currentState;
    }

    /**
     * Sets the current state of this pairing.
     * 
     * @param state
     *            the current state of this pairing.
     */
    protected void setCurrentState(RealizedState state) {
        currentState = state;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
