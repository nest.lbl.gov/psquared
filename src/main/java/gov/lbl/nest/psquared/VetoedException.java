package gov.lbl.nest.psquared;

/**
 * This exception reports when an requested transition is not allowed because of
 * the current state of an item/processing pair.
 * 
 * @author patton
 */
public class VetoedException extends
                             PSquaredException {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by the Serialized mechanism.
     */
    private static final long serialVersionUID = 0L;

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     */
    public VetoedException() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param message
     *            the detailed message.
     */
    public VetoedException(String message) {
        super(message);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param message
     *            the detailed message.
     * @param transition
     *            the {@link Transition} that created the state whose requested exit
     *            transition is forbidden.
     */
    public VetoedException(String message,
                           Transition transition) {
        super(message,
              transition);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param transition
     *            the {@link Transition} that created the state whose requested exit
     *            transition is forbidden.
     */
    public VetoedException(Transition transition) {
        super(transition);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
