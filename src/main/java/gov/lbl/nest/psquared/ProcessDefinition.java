package gov.lbl.nest.psquared;

/**
 * This interface is used by each uniquely identified process definition.
 * 
 * @author patton
 */
public interface ProcessDefinition {

    /**
     * Returns the scheduler to to use when executing this definition and none is
     * specified.
     * 
     * @return the scheduler to to use when executing this definition and none is
     *         specified.
     */
    KnownScheduler getDefaultScheduler();

    /**
     * Returns the description of this object within the family of definitions.
     * 
     * @return the description of this object within the family of definitions.
     */
    String getDescription();

    /**
     * Returns the family of definitions to which this instance belongs.
     * 
     * @return the family of definitions to which this instance belongs.
     */
    Family getFamily();

    /**
     * Returns the identity of this object.
     * 
     * @return the identity of this object.
     */
    String getIdentity();

    /**
     * Returns the name of this object within the family of definitions.
     * 
     * @return the name of this object within the family of definitions.
     */
    String getName();

    /**
     * Returns true when items can be processed with this definition.
     * 
     * @return true when items can be processed with this definition.
     */
    boolean isActive();

    /**
     * Returns true when this definition is used during process development, which
     * it to say that it is mutable.
     * 
     * @return true when this definition is used during process development, which
     *         it to say that it is mutable.
     */
    Boolean isDevelopment();
}
