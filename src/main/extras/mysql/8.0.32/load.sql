-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: localhost    Database: psquared
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `BashSubmitter2`
--

DROP TABLE IF EXISTS `BashSubmitter2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `BashSubmitter2` (
  `instruction` varchar(255) NOT NULL,
  `runner` varchar(255) NOT NULL,
  `schedulerKey` int NOT NULL,
  PRIMARY KEY (`schedulerKey`),
  CONSTRAINT `FK2jxwl69h27jaca3p14yundtti` FOREIGN KEY (`schedulerKey`) REFERENCES `Scheduler2` (`schedulerKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Exit2`
--

DROP TABLE IF EXISTS `Exit2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Exit2` (
  `exitKey` int NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `origin_stateKey` int DEFAULT NULL,
  `target_stateKey` int NOT NULL,
  PRIMARY KEY (`exitKey`),
  KEY `FK5ugrehfm55av6mjf7d5b5jfy6` (`origin_stateKey`),
  KEY `FK24dyinw1q14n9ppea6ite6r3h` (`target_stateKey`),
  CONSTRAINT `FK24dyinw1q14n9ppea6ite6r3h` FOREIGN KEY (`target_stateKey`) REFERENCES `State2` (`stateKey`),
  CONSTRAINT `FK5ugrehfm55av6mjf7d5b5jfy6` FOREIGN KEY (`origin_stateKey`) REFERENCES `State2` (`stateKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Family2`
--

DROP TABLE IF EXISTS `Family2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Family2` (
  `familyKey` int NOT NULL,
  `active` bit(1) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `defaultDefinition_processDefinitionKey` int DEFAULT NULL,
  PRIMARY KEY (`familyKey`),
  KEY `FKimdlj3yqwbgsvkiwwa5iiyxie` (`defaultDefinition_processDefinitionKey`),
  CONSTRAINT `FKimdlj3yqwbgsvkiwwa5iiyxie` FOREIGN KEY (`defaultDefinition_processDefinitionKey`) REFERENCES `ProcessDefinition2` (`processDefinitionKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LastWatched`
--

DROP TABLE IF EXISTS `LastWatched`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `LastWatched` (
  `lastWatchedKey` int NOT NULL,
  `lastDateTime` datetime(6) NOT NULL,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`lastWatchedKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `NopSubmitter2`
--

DROP TABLE IF EXISTS `NopSubmitter2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `NopSubmitter2` (
  `schedulerKey` int NOT NULL,
  PRIMARY KEY (`schedulerKey`),
  CONSTRAINT `FK49bf3riq4952pdi56uqp9y2gm` FOREIGN KEY (`schedulerKey`) REFERENCES `Scheduler2` (`schedulerKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pairing2`
--

DROP TABLE IF EXISTS `Pairing2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Pairing2` (
  `pairingKey` int NOT NULL,
  `item` varchar(255) NOT NULL,
  `uuid` binary(255) DEFAULT NULL,
  `initialTransition_transitionKey` int DEFAULT NULL,
  `processDefinition_processDefinitionKey` int NOT NULL,
  `scheduler_schedulerKey` int NOT NULL,
  PRIMARY KEY (`pairingKey`),
  KEY `FKavq779clmw2edevow42eqy5aj` (`initialTransition_transitionKey`),
  KEY `FK10oqggqcxvyxewjq9hxspiy4e` (`processDefinition_processDefinitionKey`),
  KEY `FK5xv2pocvsd0ob5bsw89ygexkd` (`scheduler_schedulerKey`),
  CONSTRAINT `FK10oqggqcxvyxewjq9hxspiy4e` FOREIGN KEY (`processDefinition_processDefinitionKey`) REFERENCES `ProcessDefinition2` (`processDefinitionKey`),
  CONSTRAINT `FK5xv2pocvsd0ob5bsw89ygexkd` FOREIGN KEY (`scheduler_schedulerKey`) REFERENCES `Scheduler2` (`schedulerKey`),
  CONSTRAINT `FKavq779clmw2edevow42eqy5aj` FOREIGN KEY (`initialTransition_transitionKey`) REFERENCES `Transition2` (`transitionKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PoI`
--

DROP TABLE IF EXISTS `PoI`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PoI` (
  `pointOfInterestKey` int NOT NULL,
  `level` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `whenOccurred` datetime(6) DEFAULT NULL,
  `guid_poIGuidKey` int DEFAULT NULL,
  `type_poITypeKey` int DEFAULT NULL,
  PRIMARY KEY (`pointOfInterestKey`),
  KEY `FKf2erw9j5mh3342utu16on4egp` (`guid_poIGuidKey`),
  KEY `FKgkqlhivye5d9x3ca1jwr6h08v` (`type_poITypeKey`),
  CONSTRAINT `FKf2erw9j5mh3342utu16on4egp` FOREIGN KEY (`guid_poIGuidKey`) REFERENCES `PoIGuid` (`poIGuidKey`),
  CONSTRAINT `FKgkqlhivye5d9x3ca1jwr6h08v` FOREIGN KEY (`type_poITypeKey`) REFERENCES `PoIType` (`poITypeKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PoIAttribute`
--

DROP TABLE IF EXISTS `PoIAttribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PoIAttribute` (
  `poIAttributeKey` int NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `name_poIAttributeNameKey` int DEFAULT NULL,
  `pointOfInterest_pointOfInterestKey` int DEFAULT NULL,
  PRIMARY KEY (`poIAttributeKey`),
  KEY `FKt89qj0m0jxapdi0mypkw02xgl` (`name_poIAttributeNameKey`),
  KEY `FKhbmf9108sv87mr5eswkso49br` (`pointOfInterest_pointOfInterestKey`),
  CONSTRAINT `FKhbmf9108sv87mr5eswkso49br` FOREIGN KEY (`pointOfInterest_pointOfInterestKey`) REFERENCES `PoI` (`pointOfInterestKey`),
  CONSTRAINT `FKt89qj0m0jxapdi0mypkw02xgl` FOREIGN KEY (`name_poIAttributeNameKey`) REFERENCES `PoIAttributeName` (`poIAttributeNameKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PoIAttributeName`
--

DROP TABLE IF EXISTS `PoIAttributeName`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PoIAttributeName` (
  `poIAttributeNameKey` int NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`poIAttributeNameKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PoIGuid`
--

DROP TABLE IF EXISTS `PoIGuid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PoIGuid` (
  `poIGuidKey` int NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`poIGuidKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PoIType`
--

DROP TABLE IF EXISTS `PoIType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PoIType` (
  `poITypeKey` int NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`poITypeKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProcessDefinition2`
--

DROP TABLE IF EXISTS `ProcessDefinition2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ProcessDefinition2` (
  `processDefinitionKey` int NOT NULL,
  `active` bit(1) NOT NULL,
  `args` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `development` bit(1) DEFAULT NULL,
  `failureCmd` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `ordinal` int DEFAULT NULL,
  `processCmd` varchar(255) NOT NULL,
  `successCmd` varchar(255) DEFAULT NULL,
  `defaultScheduler_schedulerKey` int NOT NULL,
  `family_familykey` int NOT NULL,
  PRIMARY KEY (`processDefinitionKey`),
  KEY `FKs2jir7f5eds9bv8cl3hb6ac9t` (`defaultScheduler_schedulerKey`),
  KEY `FKk05a1bi98kem2h2eqd9wsf90y` (`family_familykey`),
  CONSTRAINT `FKk05a1bi98kem2h2eqd9wsf90y` FOREIGN KEY (`family_familykey`) REFERENCES `Family2` (`familyKey`),
  CONSTRAINT `FKs2jir7f5eds9bv8cl3hb6ac9t` FOREIGN KEY (`defaultScheduler_schedulerKey`) REFERENCES `Scheduler2` (`schedulerKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RabbitMQSubmitter2`
--

DROP TABLE IF EXISTS `RabbitMQSubmitter2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RabbitMQSubmitter2` (
  `managementScript` varchar(255) DEFAULT NULL,
  `properties` varchar(255) DEFAULT NULL,
  `queue` varchar(255) NOT NULL,
  `schedulerKey` int NOT NULL,
  PRIMARY KEY (`schedulerKey`),
  CONSTRAINT `FK32wmjoecuwat4w2hk88jglpd` FOREIGN KEY (`schedulerKey`) REFERENCES `Scheduler2` (`schedulerKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Scheduler2`
--

DROP TABLE IF EXISTS `Scheduler2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Scheduler2` (
  `schedulerKey` int NOT NULL,
  `active` bit(1) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `resubmit` bit(1) NOT NULL,
  PRIMARY KEY (`schedulerKey`),
  UNIQUE KEY `UK_5p5sp3vuwoksv8ivo5yka6auc` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SeenItem`
--

DROP TABLE IF EXISTS `SeenItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `SeenItem` (
  `seenItemKey` int NOT NULL,
  `item` varchar(256) NOT NULL,
  `lastWatched_lastWatchedKey` int NOT NULL,
  PRIMARY KEY (`seenItemKey`),
  KEY `FKle7rsa4d2lrpkjajtuvc6qmyj` (`lastWatched_lastWatchedKey`),
  CONSTRAINT `FKle7rsa4d2lrpkjajtuvc6qmyj` FOREIGN KEY (`lastWatched_lastWatchedKey`) REFERENCES `LastWatched` (`lastWatchedKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `State2`
--

DROP TABLE IF EXISTS `State2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `State2` (
  `stateKey` int NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`stateKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Transition2`
--

DROP TABLE IF EXISTS `Transition2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Transition2` (
  `transitionKey` int NOT NULL,
  `whenOccurred` datetime(6) NOT NULL,
  `exitImpl_exitKey` int DEFAULT NULL,
  `pairingImpl_pairingKey` int NOT NULL,
  `precedingTransition_transitionKey` int DEFAULT NULL,
  `succeedingTransition_transitionKey` int DEFAULT NULL,
  PRIMARY KEY (`transitionKey`),
  KEY `FKh4gikkmanx23b9o6q3nsne25q` (`exitImpl_exitKey`),
  KEY `FKgy82kkywqtw6a7nrdji5jgh16` (`pairingImpl_pairingKey`),
  KEY `FKpmibxy2p0765bq73nsf20b5si` (`precedingTransition_transitionKey`),
  KEY `FK1p3srm9b6sd6cg6u1lw2hased` (`succeedingTransition_transitionKey`),
  CONSTRAINT `FK1p3srm9b6sd6cg6u1lw2hased` FOREIGN KEY (`succeedingTransition_transitionKey`) REFERENCES `Transition2` (`transitionKey`),
  CONSTRAINT `FKgy82kkywqtw6a7nrdji5jgh16` FOREIGN KEY (`pairingImpl_pairingKey`) REFERENCES `Pairing2` (`pairingKey`),
  CONSTRAINT `FKh4gikkmanx23b9o6q3nsne25q` FOREIGN KEY (`exitImpl_exitKey`) REFERENCES `Exit2` (`exitKey`),
  CONSTRAINT `FKpmibxy2p0765bq73nsf20b5si` FOREIGN KEY (`precedingTransition_transitionKey`) REFERENCES `Transition2` (`transitionKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TransitionMessage2`
--

DROP TABLE IF EXISTS `TransitionMessage2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `TransitionMessage2` (
  `message` varchar(255) DEFAULT NULL,
  `transitionKey` int NOT NULL,
  PRIMARY KEY (`transitionKey`),
  CONSTRAINT `FKm27u5eek13ye88rkacl736yh7` FOREIGN KEY (`transitionKey`) REFERENCES `Transition2` (`transitionKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-02-07  3:25:17
