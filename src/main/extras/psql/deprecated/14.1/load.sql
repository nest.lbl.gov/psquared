--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1
-- Dumped by pg_dump version 14.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: basemapping2; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.basemapping2 (
    mappingkey integer NOT NULL,
    key character varying(255),
    value character varying(255)
);


--
-- Name: bashsubmitter2; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bashsubmitter2 (
    instruction character varying(255) NOT NULL,
    runner character varying(255) NOT NULL,
    schedulerkey integer NOT NULL
);


--
-- Name: exit2; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.exit2 (
    exitkey integer NOT NULL,
    name character varying(255),
    origin_statekey integer,
    target_statekey integer NOT NULL
);


--
-- Name: family2; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.family2 (
    familykey integer NOT NULL,
    active boolean NOT NULL,
    description character varying(255),
    name character varying(255),
    defaultdefinition_processdefinitionkey integer
);


--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 10
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lastwatched; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lastwatched (
    lastwatchedkey integer NOT NULL,
    lastdatetime timestamp with time zone NOT NULL,
    role character varying(255) NOT NULL
);


--
-- Name: nopsubmitter2; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.nopsubmitter2 (
    schedulerkey integer NOT NULL
);


--
-- Name: pairing2; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pairing2 (
    pairingkey integer NOT NULL,
    item character varying(511) NOT NULL,
    uuid uuid,
    initialtransition_transitionkey integer,
    processdefinition_processdefinitionkey integer NOT NULL,
    scheduler_schedulerkey integer NOT NULL
);


--
-- Name: poi; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poi (
    pointofinterestkey integer NOT NULL,
    level integer,
    status integer,
    whenoccurred timestamp with time zone,
    guid_poiguidkey integer,
    type_poitypekey integer
);


--
-- Name: poiattribute; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poiattribute (
    poiattributekey integer NOT NULL,
    value character varying(255),
    name_poiattributenamekey integer,
    pointofinterest_pointofinterestkey integer
);


--
-- Name: poiattributename; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poiattributename (
    poiattributenamekey integer NOT NULL,
    value character varying(255)
);


--
-- Name: poiguid; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poiguid (
    poiguidkey integer NOT NULL,
    value character varying(255)
);


--
-- Name: poitype; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poitype (
    poitypekey integer NOT NULL,
    value character varying(255)
);


--
-- Name: processdefinition2; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.processdefinition2 (
    processdefinitionkey integer NOT NULL,
    active boolean NOT NULL,
    args character varying(511),
    description character varying(511),
    development boolean,
    failurecmd character varying(511),
    name character varying(511) NOT NULL,
    ordinal integer,
    processcmd character varying(511) NOT NULL,
    successcmd character varying(511),
    defaultscheduler_schedulerkey integer NOT NULL,
    family_familykey integer NOT NULL
);


--
-- Name: rabbitmqsubmitter2; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.rabbitmqsubmitter2 (
    managementscript character varying(255),
    properties character varying(255),
    queue character varying(255) NOT NULL,
    schedulerkey integer NOT NULL
);


--
-- Name: scheduler2; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.scheduler2 (
    schedulerkey integer NOT NULL,
    active boolean NOT NULL,
    description character varying(255),
    name character varying(255) NOT NULL,
    resubmit boolean NOT NULL
);


--
-- Name: seenitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.seenitem (
    seenitemkey integer NOT NULL,
    item character varying(256) NOT NULL,
    lastwatched_lastwatchedkey integer NOT NULL
);


--
-- Name: state2; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.state2 (
    statekey integer NOT NULL,
    name character varying(255)
);


--
-- Name: transition2; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.transition2 (
    transitionkey integer NOT NULL,
    whenoccurred timestamp with time zone NOT NULL,
    exitimpl_exitkey integer,
    pairingimpl_pairingkey integer NOT NULL,
    precedingtransition_transitionkey integer,
    succeedingtransition_transitionkey integer
);


--
-- Name: transitionmessage2; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.transitionmessage2 (
    message character varying(511),
    transitionkey integer NOT NULL
);


--
-- Name: basemapping2 basemapping2_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.basemapping2
    ADD CONSTRAINT basemapping2_pkey PRIMARY KEY (mappingkey);


--
-- Name: bashsubmitter2 bashsubmitter2_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bashsubmitter2
    ADD CONSTRAINT bashsubmitter2_pkey PRIMARY KEY (schedulerkey);


--
-- Name: exit2 exit2_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.exit2
    ADD CONSTRAINT exit2_pkey PRIMARY KEY (exitkey);


--
-- Name: family2 family2_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.family2
    ADD CONSTRAINT family2_pkey PRIMARY KEY (familykey);


--
-- Name: lastwatched lastwatched_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lastwatched
    ADD CONSTRAINT lastwatched_pkey PRIMARY KEY (lastwatchedkey);


--
-- Name: nopsubmitter2 nopsubmitter2_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.nopsubmitter2
    ADD CONSTRAINT nopsubmitter2_pkey PRIMARY KEY (schedulerkey);


--
-- Name: pairing2 pairing2_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pairing2
    ADD CONSTRAINT pairing2_pkey PRIMARY KEY (pairingkey);


--
-- Name: poi poi_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poi
    ADD CONSTRAINT poi_pkey PRIMARY KEY (pointofinterestkey);


--
-- Name: poiattribute poiattribute_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiattribute
    ADD CONSTRAINT poiattribute_pkey PRIMARY KEY (poiattributekey);


--
-- Name: poiattributename poiattributename_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiattributename
    ADD CONSTRAINT poiattributename_pkey PRIMARY KEY (poiattributenamekey);


--
-- Name: poiguid poiguid_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiguid
    ADD CONSTRAINT poiguid_pkey PRIMARY KEY (poiguidkey);


--
-- Name: poitype poitype_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poitype
    ADD CONSTRAINT poitype_pkey PRIMARY KEY (poitypekey);


--
-- Name: processdefinition2 processdefinition2_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.processdefinition2
    ADD CONSTRAINT processdefinition2_pkey PRIMARY KEY (processdefinitionkey);


--
-- Name: rabbitmqsubmitter2 rabbitmqsubmitter2_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rabbitmqsubmitter2
    ADD CONSTRAINT rabbitmqsubmitter2_pkey PRIMARY KEY (schedulerkey);


--
-- Name: scheduler2 scheduler2_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduler2
    ADD CONSTRAINT scheduler2_pkey PRIMARY KEY (schedulerkey);


--
-- Name: seenitem seenitem_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seenitem
    ADD CONSTRAINT seenitem_pkey PRIMARY KEY (seenitemkey);


--
-- Name: state2 state2_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.state2
    ADD CONSTRAINT state2_pkey PRIMARY KEY (statekey);


--
-- Name: transition2 transition2_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transition2
    ADD CONSTRAINT transition2_pkey PRIMARY KEY (transitionkey);


--
-- Name: transitionmessage2 transitionmessage2_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transitionmessage2
    ADD CONSTRAINT transitionmessage2_pkey PRIMARY KEY (transitionkey);


--
-- Name: scheduler2 uk_5p5sp3vuwoksv8ivo5yka6auc; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduler2
    ADD CONSTRAINT uk_5p5sp3vuwoksv8ivo5yka6auc UNIQUE (name);


--
-- Name: pairing2 fk10oqggqcxvyxewjq9hxspiy4e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pairing2
    ADD CONSTRAINT fk10oqggqcxvyxewjq9hxspiy4e FOREIGN KEY (processdefinition_processdefinitionkey) REFERENCES public.processdefinition2(processdefinitionkey);


--
-- Name: transition2 fk1p3srm9b6sd6cg6u1lw2hased; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transition2
    ADD CONSTRAINT fk1p3srm9b6sd6cg6u1lw2hased FOREIGN KEY (succeedingtransition_transitionkey) REFERENCES public.transition2(transitionkey);


--
-- Name: exit2 fk24dyinw1q14n9ppea6ite6r3h; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.exit2
    ADD CONSTRAINT fk24dyinw1q14n9ppea6ite6r3h FOREIGN KEY (target_statekey) REFERENCES public.state2(statekey);


--
-- Name: bashsubmitter2 fk2jxwl69h27jaca3p14yundtti; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bashsubmitter2
    ADD CONSTRAINT fk2jxwl69h27jaca3p14yundtti FOREIGN KEY (schedulerkey) REFERENCES public.scheduler2(schedulerkey);


--
-- Name: rabbitmqsubmitter2 fk32wmjoecuwat4w2hk88jglpd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rabbitmqsubmitter2
    ADD CONSTRAINT fk32wmjoecuwat4w2hk88jglpd FOREIGN KEY (schedulerkey) REFERENCES public.scheduler2(schedulerkey);


--
-- Name: nopsubmitter2 fk49bf3riq4952pdi56uqp9y2gm; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.nopsubmitter2
    ADD CONSTRAINT fk49bf3riq4952pdi56uqp9y2gm FOREIGN KEY (schedulerkey) REFERENCES public.scheduler2(schedulerkey);


--
-- Name: exit2 fk5ugrehfm55av6mjf7d5b5jfy6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.exit2
    ADD CONSTRAINT fk5ugrehfm55av6mjf7d5b5jfy6 FOREIGN KEY (origin_statekey) REFERENCES public.state2(statekey);


--
-- Name: pairing2 fk5xv2pocvsd0ob5bsw89ygexkd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pairing2
    ADD CONSTRAINT fk5xv2pocvsd0ob5bsw89ygexkd FOREIGN KEY (scheduler_schedulerkey) REFERENCES public.scheduler2(schedulerkey);


--
-- Name: pairing2 fkavq779clmw2edevow42eqy5aj; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pairing2
    ADD CONSTRAINT fkavq779clmw2edevow42eqy5aj FOREIGN KEY (initialtransition_transitionkey) REFERENCES public.transition2(transitionkey);


--
-- Name: poi fkf2erw9j5mh3342utu16on4egp; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poi
    ADD CONSTRAINT fkf2erw9j5mh3342utu16on4egp FOREIGN KEY (guid_poiguidkey) REFERENCES public.poiguid(poiguidkey);


--
-- Name: poi fkgkqlhivye5d9x3ca1jwr6h08v; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poi
    ADD CONSTRAINT fkgkqlhivye5d9x3ca1jwr6h08v FOREIGN KEY (type_poitypekey) REFERENCES public.poitype(poitypekey);


--
-- Name: transition2 fkgy82kkywqtw6a7nrdji5jgh16; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transition2
    ADD CONSTRAINT fkgy82kkywqtw6a7nrdji5jgh16 FOREIGN KEY (pairingimpl_pairingkey) REFERENCES public.pairing2(pairingkey);


--
-- Name: transition2 fkh4gikkmanx23b9o6q3nsne25q; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transition2
    ADD CONSTRAINT fkh4gikkmanx23b9o6q3nsne25q FOREIGN KEY (exitimpl_exitkey) REFERENCES public.exit2(exitkey);


--
-- Name: poiattribute fkhbmf9108sv87mr5eswkso49br; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiattribute
    ADD CONSTRAINT fkhbmf9108sv87mr5eswkso49br FOREIGN KEY (pointofinterest_pointofinterestkey) REFERENCES public.poi(pointofinterestkey);


--
-- Name: family2 fkimdlj3yqwbgsvkiwwa5iiyxie; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.family2
    ADD CONSTRAINT fkimdlj3yqwbgsvkiwwa5iiyxie FOREIGN KEY (defaultdefinition_processdefinitionkey) REFERENCES public.processdefinition2(processdefinitionkey);


--
-- Name: processdefinition2 fkk05a1bi98kem2h2eqd9wsf90y; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.processdefinition2
    ADD CONSTRAINT fkk05a1bi98kem2h2eqd9wsf90y FOREIGN KEY (family_familykey) REFERENCES public.family2(familykey);


--
-- Name: seenitem fkle7rsa4d2lrpkjajtuvc6qmyj; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seenitem
    ADD CONSTRAINT fkle7rsa4d2lrpkjajtuvc6qmyj FOREIGN KEY (lastwatched_lastwatchedkey) REFERENCES public.lastwatched(lastwatchedkey);


--
-- Name: transitionmessage2 fkm27u5eek13ye88rkacl736yh7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transitionmessage2
    ADD CONSTRAINT fkm27u5eek13ye88rkacl736yh7 FOREIGN KEY (transitionkey) REFERENCES public.transition2(transitionkey);


--
-- Name: transition2 fkpmibxy2p0765bq73nsf20b5si; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transition2
    ADD CONSTRAINT fkpmibxy2p0765bq73nsf20b5si FOREIGN KEY (precedingtransition_transitionkey) REFERENCES public.transition2(transitionkey);


--
-- Name: processdefinition2 fks2jir7f5eds9bv8cl3hb6ac9t; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.processdefinition2
    ADD CONSTRAINT fks2jir7f5eds9bv8cl3hb6ac9t FOREIGN KEY (defaultscheduler_schedulerkey) REFERENCES public.scheduler2(schedulerkey);


--
-- Name: poiattribute fkt89qj0m0jxapdi0mypkw02xgl; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiattribute
    ADD CONSTRAINT fkt89qj0m0jxapdi0mypkw02xgl FOREIGN KEY (name_poiattributenamekey) REFERENCES public.poiattributename(poiattributenamekey);

ALTER TABLE public.pairing2
    ADD CONSTRAINT pairing2_unique UNIQUE (item, processdefinition_processdefinitionkey);

ALTER TABLE public.processdefinition2
    ADD CONSTRAINT processdefinition2_unique UNIQUE (name, family_familykey);

--
-- PostgreSQL database dump complete
--

