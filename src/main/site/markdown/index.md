# Overview of PSquared #

PSquared is an application to manage Production Processing. Its two main responsibilities are:

-   To document the processing environment for a given execution of an item.
-   To avoid accidental re-running or overwriting of a given execution of an item.

It achieves this by binding a submitted item for processing with a pre-defined software configuration into an entity called a __Pairing__. This Pairing then follows the state machine diagrammed below.

![Conceptual PSquared State Machine](png/conceptual_state_machine.png "Conceptual PSquared State Machine")

In this state machine a Pairing only is assigned a state once the has been a request to process the item with the particular software configuration, and that first state is __PROCESSING__. The PROCESSING state has two sub-states in order to accomodate queued execution of the process. The states starts in the __QUEUED__ sub-state until the execution system is ready to actually start the processing, at which point it moves to the __EXECUTING__ sub-state. When processing is complete it is either successful, in which case the Pairing enters the __PROCESSED__ state, or fails, in which case the Pairing enters the __FAILED__ state.

(The full state machine is detailed [elsewhere](full_state_machine.html))


# Organization #

The PSquared system is organized into a number of distinct parts as shown below.

![PSquared Organization](png/psquared_organization.png "PSquared Organization")

-   The main application, which runs in a JEE 7 [Java Enterprise Edition](http://www.oracle.com/technetwork/java/javaee/overview/index.html) application server - [Wildfly 10](http://wildfly.org/) is the currently recommended one and all discussion in this documentation will be based on that.

    *   A `scheduler`, which is part of the main application and prepares requests for its associated execution system. 

-   The client, which uses a [RESTful](https://en.wikipedia.org/wiki/Representational_state_transfer) API to place requests to and receive reports from the main application.

-   A _runner_, which is dependent on a selected `scheduler` component of the main application, and acts as a container in which the process is actually executed in whichever processing system has been requested.


# User Interface #

Most interactions with the PSquared system take place using client that use the RESTful API. (Some actions are not currently available through the API so must be done directly in the main application, and these should only be done by experts.) Currently there is only one PSquared client, `pp-cli`, the PSquared command line interface.

## PSquared Command Line Interface ##

The PSquared command line interface command, `pp-cli`, can be used to for the following tasks for one or more Pairings.

-   Display the current status.
-   Display the processing history.
-   Submit for processing.
-   Cancel submitted processings.
-   Resolve processings so they can be re-submitted.
-   Reset processings so they can processed again.
-   Abandon processings so the Pairings are effectively "forgotten".
-   Recover Pairings that were abandoned so that they can be handle by PSquared again.
-   List known processing families.
-   List known configurations of a known family.

The full details of how the use `pp-cli` are documented [elsewhere](pp-cli.html)

