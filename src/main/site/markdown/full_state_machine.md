# Compete PSquared State Machine #

Of course, processing never goes as planned. For example a processing may fail because of a missing directory, or you may want to cancel the processing to a later data when newer information is available. To be able to support such abnormal behaviors the full PSquared State Diagram is diagramed below.

![Complete PSquared State Machine](png/full_state_machine.png "Complete PSquared State Machine")


## Resolving a Failure ##

The most obvious abnormal is when a Pairing ends up in the FAILED state an it needs to be submitted again. However the PSquared system does not allow it to be simply resubmitted as it is expected that some action must be taken outside of the PSquared system to rectify the failure before resubmission. Therefore the PSquared system must be explicitly told when a Pairing's failure has been resolved at which time the Pairing is moved into the __READY__ state and can now be re-submitted.


## Cancelling an Execution ##

In order to stop an Pairing from completing its processing it is possible to cancel that processing at any time until the processing terminates. If this is done the Pairing simply moves to the READY state so that is can be re-submitted at a later date.

It should be noted the PSquared may not actually stop a processing when it is cancelled. The processing may still run to completion, and if it does the State Machine will ignore any attempt to move it into the PROCESSED or FAILED states.


## Resubmitting a Processing ##


## Resetting a Processing so it can be Processed again ##


## Abandoning a Processing ##


## Rescuing a Processing that has been abandoned ##
