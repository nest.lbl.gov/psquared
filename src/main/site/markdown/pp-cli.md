# The PSquared Command Line Interface #

The PSquared command line interface command, `pp-cli`, can be used to for the following tasks for one or more Pairings.

-   Display the current status.
-   Display the processing history.
-   Submit for processing.
-   Cancel submitted processings.
-   Resolve processings so they can be re-submitted.
-   Reset processings so they can processed again.
-   Abandon processings so the Pairings are effectively "forgotten".
-   Recover Pairings that were abandoned so that they can be handle by PSquared again.
-   List known processing families.
-   List known configurations of a known family.


## Client Preparation ##

The `pp-cli` command is a python script and therefore needs an appropriate python environment in which to execute. The following commands will download the `pp-cli` command and put it in the `${HOME}/bin` directory. If this is not already in the ${PATH} variable is needs to be added as shown.

    PSQUARED_VERSION=1.1.0
    mkdir -p ${HOME}/bin
    export PATH=${HOME}/bin:${PATH}
    wget -O ${HOME}/bin/pp-cli \
            http://nest.lbl.gov/projects/psquared/resources/${PSQUARED_VERSION}/client/pp-cli
    chmod +x bin/pp-cli
    ~/bin/pp-cli -?

If the lat command above fails because of a missing python module, the following commands can be executed to, hopefully, create an appropriate python environment.

    wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
    bash Miniconda2-latest-Linux-x86_64.sh
    export PATH="${HOME}/miniconda2/bin:$PATH"
    mkdir -p ${HOME}/.psquared/client
    conda create -p ${HOME}/.psquared/client/conda pip
    source activate /home/lzprod/.psquared/client/conda
    pip install requests
    pip install pika
    ~/bin/pp-cli -?

(If this does not work then you'll need to consult a local python expert to resolve the issue.)

