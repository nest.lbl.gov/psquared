# Process a Pairing #


## Overview ##

This use case describes how the processing of an item and configuration pairing is managed.


## Pre-requisites #

The PSquared instance must be running.


## Scenario ##

-   The processor supplied with details of the submitted item and configuration pairing.
-   The processor signals the PSquared instance that it is beginning to process the pairing.
-   The processor begins processing the pairing.
-   The processing of the pairing completes.
-   The processor signals the system that the pairing been processed.


## Exception Scenarios ##


### Failed Processing ###

If processing fails at any stage the processor signals the PSquared instance that the pairing has failed.