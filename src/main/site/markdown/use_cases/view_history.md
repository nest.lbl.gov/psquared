# View History #


## Overview ##

This use case describes how the history of an item and configuration pairing can be viewed


## Pre-requisites #

The client needs to know the item and configuration whose history is to be viewed and the PSquared instance must be running.


# Scenario

-   The client accesses the PSquared instance.
-   The client requests the history of the item and configuration pairing from the system.
-   The system updates client on the history of the item and configuration pairing until the client terminates the request.
