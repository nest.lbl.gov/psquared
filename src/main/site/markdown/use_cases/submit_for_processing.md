# Submit for Processing #


## Overview ##
This use case describes how to submit an item and configuration pairing to be processed


## Pre-requisites #

The client needs to know the item and configuration with which the processing will be done, and the PSquared instance must be running.

## Scenario ##

-   The client accesses the PSquared instance.
-   The client requests that the system submit the item and configuration pairing for processing.
-   The system updates client on the progress of the item and configuration pairing until the processing terminates.


## Exception Scenarios ##


### Cancel Processing###

The processing of an item and configuration pairing can be cancelled after it has been submitted up until the time it terminates.
