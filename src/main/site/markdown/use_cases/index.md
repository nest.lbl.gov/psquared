# Use Cases #

This section explain the use cases that PSquared is designed to execute.

-   [Submit a Pairing](submit_for_processing.html)
-   [Process an Pairing](process_a_pairing.html)
-   [View a Pairing's History](view_history.html)
