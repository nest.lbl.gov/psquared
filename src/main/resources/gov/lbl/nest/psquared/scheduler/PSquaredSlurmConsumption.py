import os

class PSquaredSlurmConsumption(PSquaredConsumption):

    def __init__(self,
                 properties,
                 body,
                 redelivered):
        PSquaredConsumption.__init__(self,
							         properties,
                 					 body,
                 					 redelivered)

    def executionMessage(self):
        slurm_job_id = os.environ['SLURM_JOB_ID']
        if None == slurm_id:
            slurm_message = ''
        else:
            slurm_message = ' SLURM_JOBID'
            slurm_localid = os.environ['SLURM_LOCALID']
            if None == slurm_localid:
                slurm_message += ' : ' + slurm_id
            else:
                slurm_message += '/SLURM_LOCALID' : ' + slurm_id + '/' + slurm_localid
        return 'Executing on ' + socket.gethostname() + slurm_message
