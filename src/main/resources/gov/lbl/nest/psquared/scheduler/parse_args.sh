#!/bin/sh

while [ "X" != "X${1}" ] ; do
    if [ "-n" = "${1}" ] ; then
        cat << EOF
-n
EOF
    else
        echo ${1}
    fi	
    shift
done
