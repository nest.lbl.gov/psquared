#!/usr/bin/env python

# Prepare environment
import sys
from pika.frame import Body

class NullConsumption(object):

    def __init__(self,
                 properties,
                 body,
                 redelivered):
        self._body = body
        self._properties = properties
        self._redelivered = redelivered

    def consume(self):
        try:
            print 'BEGIN CONSUMPTION'
            print 'END CONSUMPTION'
        except PSquaredFailure as e:
            print 'Failed to complete state machine'
            print >> sys.stderr, 'Failed in PSquared:\n' + e.message
        except Exception as e:
            print 'Failed to complete state machine'
            print >> sys.stderr, e.message
