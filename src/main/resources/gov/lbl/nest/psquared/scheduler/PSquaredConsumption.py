#!/usr/bin/env python

# Prepare environment
import sys
from pika.frame import Body
pyxml=None
index = 0
for p in sys.path:
    if -1 != p.find('pyxml'):
         pyxml = p
    index += 1

if None != pyxml:
    sys.path.remove(pyxml)


import errno
def mkdirs(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise e

import json
import os
import socket
import subprocess
import xml.etree.ElementTree as ET

import pp_engine

import Consumption

class PSquaredConsumption(object):

    def __init__(self,
                 properties,
                 body,
                 redelivered):
        self._body = body
        self._properties = properties
        self._redelivered = redelivered

    def executionMessage(self):
        return 'Executing on ' + socket.gethostname()

    def consume(self):
        try:
            document = ET.fromstring(self._body)
            action = document.get('action')
            if 'stop_listening' == action:
                raise Consumption.StopListeningException()
            if 'stop_worker' == action:
                raise Consumption.StopWorkerException()
            if 'ignore' == action:
                print 'Ignoring execution request'
            else:
                element = document.find('submission_uri')
                if None != element:
                    submission_uri = element.text
                    cmd = document.find('command').text
                    args = document.findall('argument')
                    if 0 == len(args):
                        arguments = None
                    else:
                        arguments = []
                        for arg in args:
                            arguments.append(arg.text)
                    element = document.find('success_cmd')
                    if None == element or '' == element.text:
                        success_cmd = None
                    else:
                        success_cmd = element.text
                    element = document.find('failure_cmd')
                    if None == element or '' == element.text:
                        failure_cmd = None
                    else:
                        failure_cmd = element.text
                    attribute = document.get('stdout')
                    if None == attribute or '' == attribute:
                        cmd_out = None
                    else:
                        if attribute.startswith('/'):
                            filepath = attribute
                        else:
                            filepath = os.path.expanduser('~/' + attribute)
                        outDir = os.path.dirname(filepath)
                        if None != outDir and '' != outDir:
                            mkdirs(outDir)
                        cmd_out = open(filepath, 'w')
                    attribute = document.get('stderr')
                    if None == attribute or '' == attribute:
                        cmd_err = None
                    else:
                        if attribute.startswith('/'):
                            filepath = attribute
                        else:
                            filepath = os.path.expanduser('~/' + attribute)
                        errDir = os.path.dirname(filepath)
                        if None != errDir and '' != errDir:
                            mkdirs(errDir)
                        cmd_err = open(filepath, 'w')
                    print 'Beginning state machine completion'
                    pp_engine.execute(self.executionMessage(),
                                      cmd,
                                      arguments,
                                      submission_uri,
                                      cmd_out,
                                      cmd_err,
                                      success_cmd,
                                      failure_cmd,
                                      self._redelivered)
                    print 'Completed state machine'
        except Consumption.StopListeningException as e:
            raise e
        except Consumption.StopWorkerException as e:
            raise e
        except pp_engine.PSquaredFailure as e:
            print 'Failed to complete state machine'
            print >> sys.stderr, 'Failed in PSquared:\n' + e.message
            print 'END CONSUMPTION'
        except Exception as e:
            print 'Failed to complete state machine'
            print >> sys.stderr, e.message
            print 'ABORT CONSUMPTION'
