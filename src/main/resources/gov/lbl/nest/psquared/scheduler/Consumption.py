#!/usr/bin/env python

class StopWorkerException(Exception):
    def __init__(self, message = None):
        if None != message:
            super(StopWorkerException, self).__init__(message)

class StopListeningException(Exception):
    def __init__(self, message = None):
        if None != message:
            super(StopListeningException, self).__init__(message)

