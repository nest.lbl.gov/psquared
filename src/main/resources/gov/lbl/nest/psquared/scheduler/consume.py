#!/usr/bin/env python

"""
Based on http://pika.readthedocs.org/en/latest/examples/asynchronous_consumer_example.html

Changes include, but not limited to, removal of Exchange binding and dont the consumption
in a separate thread.
"""

# Make sure pp_engine.py is in path
import os
import sys

psquared_dir=os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))
sys.path.insert(0, os.path.join(psquared_dir,
                                "runner"))

import logging
import multiprocessing
import sys
import threading
import time
import traceback

import pika

import Consumption
from parameter_utils import get_parameters

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)

class Worker(threading.Thread):
    def __init__(self,
                 consumer,
                 basic_deliver,
                 properties,
                 body):
        threading.Thread.__init__(self)
        self._basic_deliver = basic_deliver
        self._consumer = consumer
        self._consumption = ConsumptionImpl(properties,
                                            body,
                                            basic_deliver.redelivered)

    def run(self):
        try:
            self._consumption.consume()
        except Consumption.StopListeningException as s:
            LOGGER.info('Worker requested a stop to listening')
            self._consumer.add_timeout(0,
                                       self.stop_listening)
            return
        except Consumption.StopWorkerException as s:
            LOGGER.info('Worker requested a stop of itself')
            self._consumer.add_timeout(0,
                                       self.request_stop)
            return
        except Exception as e:
            traceback.print_exc()
        self._consumer.add_timeout(0,
                                   self.acknowledge_message)

    def acknowledge_message(self):
        """Binds this object's deliver tag to the consumers 'acknowledge_message'
        method
        
        """
        self._consumer.acknowledge_message(self._basic_deliver.delivery_tag)

    def request_stop(self):
        """Binds this object's 'acknowledge_message' to the consumers 'request_stop'
        method
        
        """
        self._consumer.request_stop(self.acknowledge_message)


    def stop_listening(self):
        """Binds this object's 'acknowledge_message' to the consumers 'request_stop'
        method
        
        """
        self._consumer.stop_listening()
        self._consumer.acknowledge_message(self._basic_deliver.delivery_tag)


class Consumer(object):
    """This is an example consumer that will handle unexpected interactions
    with RabbitMQ such as channel and connection closures.

    If RabbitMQ closes the connection, it will reopen it. You should
    look at the output, as there are limited reasons why the connection may
    be closed, which usually are tied to permission related issues or
    socket timeouts.

    If the channel is closed, it will indicate a problem with one of the
    commands that were issued and that should surface in the output as well.

    """

    def __init__(self, parameters, queue, prefetch=1, restart=0, durable=True, live_time=0.0, wait_time=0.0):
        """Create a new instance of the consumer class, passing in the AMQP
        URL used to connect to RabbitMQ.

        :param str amqp_url: The AMQP url to connect with

        """
        self._active = 0
        self._connection = None
        self._channel = None
        self._closing = False
        self._consumer_tag = None
        self._durable = durable
        self._listening = True
        self._parameters = parameters
        self._prefetch = prefetch
        self._queue = queue
        self._restart = restart
        self._wait_time = wait_time
        if live_time > 0:
            self._live = threading.Timer(live_time,
                                         self.expired_live)
            self._live.start()
        self.start_waiting()
        LOGGER.info('Worker count set to ' + str(self._prefetch))

    def expired_live(self):
        self.add_timeout(0,
                         self.stop_listening)

    def stop_listening(self):
        """This method is called to gracefully shut down the connection when the
        'live_time' parameters has been exceeded.
        
        """
        changed = False
        if self._prefetch != self._active:
            # Reduce the pre-fetch to the number of currently active connections.
            self._prefetch = self._active
            changed = True
            self._channel.basic_qos(prefetch_count = self._prefetch)
        if 0 == self._prefetch :
            LOGGER.info('Stopping as there no Workers and no longer listening')
            self.add_timeout(0,
                             self.stop)
        else:
            self._listening = False
            if changed:
                LOGGER.info('Worker count changed to ' + str(self._prefetch)
                    + ' as no longer listening')

    def start_waiting(self):
        if self._wait_time > 0:
            self._wait = threading.Timer(self._wait_time,
                                         self.expired_wait)
            self._wait.start()
        else:
            self._wait = None

    def expired_wait(self):
        self.add_timeout(0,
                         self.stop_waiting)

    def stop_waiting(self):
        """This method is called to gracefully shut down the connection when it's
        waiting time has been exceeded and no message received.

        """
        if 0 != self._active:
            return
        self._channel.basic_qos(prefetch_count = 0)
        LOGGER.info('Stopping as the wait time has been exceeded and no message received')
        self.add_timeout(0,
                         self.stop)

    def connect(self):
        """This method connects to RabbitMQ, returning the connection handle.
        When the connection is established, the on_connection_open method
        will be invoked by pika.

        :rtype: pika.SelectConnection

        """
        LOGGER.info('Connecting to %s', self._parameters)
        return pika.SelectConnection(self._parameters,
                                     self.on_connection_open,
                                     stop_ioloop_on_close=False)

    def on_connection_open(self, unused_connection):
        """This method is called by pika once the connection to RabbitMQ has
        been established. It passes the handle to the connection object in
        case we need it, but in this case, we'll just mark it unused.

        :type unused_connection: pika.SelectConnection

        """
        LOGGER.info('Connection opened')
        self.add_on_connection_close_callback()
        self.open_channel()

    def add_on_connection_close_callback(self):
        """This method adds an on close callback that will be invoked by pika
        when RabbitMQ closes the connection to the publisher unexpectedly.

        """
        LOGGER.info('Adding connection close callback')
        self._connection.add_on_close_callback(self.on_connection_closed)

    def on_connection_closed(self, connection, reply_code, reply_text):
        """This method is invoked by pika when the connection to RabbitMQ is
        closed unexpectedly. Since it is unexpected, we will reconnect to
        RabbitMQ if it disconnects.

        :param pika.connection.Connection connection: The closed connection obj
        :param int reply_code: The server provided reply_code if given
        :param str reply_text: The server provided reply_text if given

        """
        self._channel = None
        if self._closing or 0 >= self._restart:
            self._connection.ioloop.stop()
        else:
            LOGGER.warning('Connection closed, reopening in 5 seconds: (%s) %s',
                           reply_code, reply_text)
            self._connection.add_timeout(self._restart, self.reconnect)

    def reconnect(self):
        """Will be invoked by the IOLoop timer if the connection is
        closed. See the on_connection_closed method.

        """
        # This is the old connection IOLoop instance, stop its ioloop
        self._connection.ioloop.stop()

        if not self._closing:

            # Create a new connection
            self._connection = self.connect()

            # There is now a new connection, needs a new ioloop to run
            self._connection.ioloop.start()

    def open_channel(self):
        """Open a new channel with RabbitMQ by issuing the Channel.Open RPC
        command. When RabbitMQ responds that the channel is open, the
        on_channel_open callback will be invoked by pika.

        """
        LOGGER.info('Creating a new channel')
        self._connection.channel(on_open_callback=self.on_channel_open)

    def on_channel_open(self, channel):
        """This method is invoked by pika when the channel has been opened.
        The channel object is passed in so we can make use of it.

        Since the channel is now open, we'll declare the queue to use.

        :param pika.channel.Channel channel: The channel object

        """
        LOGGER.info('Channel opened')
        self._channel = channel
        self._channel.basic_qos(prefetch_count = self._prefetch)
        self.add_on_channel_close_callback()
        self.setup_queue(self._queue)

    def add_on_channel_close_callback(self):
        """This method tells pika to call the on_channel_closed method if
        RabbitMQ unexpectedly closes the channel.

        """
        LOGGER.info('Adding channel close callback')
        self._channel.add_on_close_callback(self.on_channel_closed)

    def on_channel_closed(self, channel, reply_code, reply_text):
        """Invoked by pika when RabbitMQ unexpectedly closes the channel.
        Channels are usually closed if you attempt to do something that
        violates the protocol, such as re-declare an exchange or queue with
        different parameters. In this case, we'll close the connection
        to shutdown the object.

        :param pika.channel.Channel: The closed channel
        :param int reply_code: The numeric reason the channel was closed
        :param str reply_text: The text reason the channel was closed

        """
        LOGGER.warning('Channel %i was closed: (%s) %s',
                       channel, reply_code, reply_text)
        self._connection.close()

    def setup_queue(self, queue_name):
        """Setup the queue on RabbitMQ by invoking the Queue.Declare RPC
        command. When it is complete, the on_queue_declareok method will
        be invoked by pika.

        :param str|unicode queue_name: The name of the queue to declare.

        """
        LOGGER.info('Declaring queue %s', queue_name)
        self._channel.queue_declare(self.on_queue_declareok,
                                    queue_name,
                                    durable=self._durable)

    def on_queue_declareok(self, method_frame):
        """Method invoked by pika when the Queue.Declare RPC call made in
        setup_queue has completed. In this method we start the consumption
        of the queue.

        :param pika.frame.Method method_frame: The Queue.DeclareOk frame

        """
        LOGGER.info('Starting consumption')
        self.start_consuming()

    def start_consuming(self):
        """This method sets up the consumer by first calling
        add_on_cancel_callback so that the object is notified if RabbitMQ
        cancels the consumer. It then issues the Basic.Consume RPC command
        which returns the consumer tag that is used to uniquely identify the
        consumer with RabbitMQ. We keep the value to use it when we want to
        cancel consuming. The on_message method is passed in as a callback pika
        will invoke when a message is fully received.

        """
        LOGGER.info('Issuing consumer related RPC commands')
        self.add_on_cancel_callback()
        self._consumer_tag = self._channel.basic_consume(self.on_message,
                                                         self._queue)

    def add_on_cancel_callback(self):
        """Add a callback that will be invoked if RabbitMQ cancels the consumer
        for some reason. If RabbitMQ does cancel the consumer,
        on_consumer_cancelled will be invoked by pika.

        """
        LOGGER.info('Adding consumer cancellation callback')
        self._channel.add_on_cancel_callback(self.on_consumer_cancelled)

    def on_consumer_cancelled(self, method_frame):
        """Invoked by pika when RabbitMQ sends a Basic.Cancel for a consumer
        receiving messages.

        :param pika.frame.Method method_frame: The Basic.Cancel frame

        """
        LOGGER.info('Consumer was cancelled remotely, shutting down: %r',
                    method_frame)
        if self._channel:
            self._channel.close()

    def on_message(self, unused_channel, basic_deliver, properties, body):
        """Invoked by pika when a message is delivered from RabbitMQ. The
        channel is passed for your convenience. The basic_deliver object that
        is passed in carries the exchange, routing key, delivery tag and
        a redelivered flag for the message. The properties passed in is an
        instance of BasicProperties with the message properties and the body
        is the message that was sent.

        :param pika.channel.Channel unused_channel: The channel object
        :param pika.Spec.Basic.Deliver: basic_deliver method
        :param pika.Spec.BasicProperties: properties
        :param str|unicode body: The message body

        """
        LOGGER.info('Received message # %s from %s: %s',
                    basic_deliver.delivery_tag, properties.app_id, body)
        self._active += 1
        if None != self._wait:
            self._wait.cancel()
            self._wait = None
        runner = Worker(self,
                        basic_deliver,
                        properties,
                        body);
        runner.start();

    def acknowledge_message(self, delivery_tag):
        """Acknowledge the message delivery from RabbitMQ by sending a
        Basic.Ack RPC method for the delivery tag.

        :param int delivery_tag: The delivery tag from the Basic.Deliver frame

        """
        LOGGER.info('Acknowledging message %s', delivery_tag)
        self._channel.basic_ack(delivery_tag)
        self._active -= 1
        if 0 == self._active:
            self.start_waiting()
        if not self._listening:
            self._prefetch -= 1
            self._channel.basic_qos(prefetch_count = self._prefetch)
            if 0 == self._prefetch :
                LOGGER.info('Stopping as there no Workers and no longer listening')
                self.add_timeout(0,
                                 self.stop)
            else:
                LOGGER.info('Worker count changed to ' + str(self._prefetch)
                    + ' as no longer listening')


    def stop_consuming(self):
        """Tell RabbitMQ that you would like to stop consuming by sending the
        Basic.Cancel RPC command.

        """
        if self._channel:
            LOGGER.info('Sending a Basic.Cancel RPC command to RabbitMQ')
            self._channel.basic_cancel(self.on_cancelok, self._consumer_tag)

    def on_cancelok(self, unused_frame):
        """This method is invoked by pika when RabbitMQ acknowledges the
        cancellation of a consumer. At this point we will close the channel.
        This will invoke the on_channel_closed method once the channel has been
        closed, which will in-turn close the connection.

        :param pika.frame.Method unused_frame: The Basic.CancelOk frame

        """
        LOGGER.info('RabbitMQ acknowledged the cancellation of the consumer')
        self.close_channel()

    def close_channel(self):
        """Call to close the channel with RabbitMQ cleanly by issuing the
        Channel.Close RPC command.

        """
        LOGGER.info('Closing the channel')
        self._channel.close()

    def add_timeout(self, deadline, callback_method):
        """Requests that the specified callback be executed in the thread
        monitoring the coinnection after the requested number of seconds"""
        self._connection.add_timeout(deadline, callback_method)

    def run(self):
        """Run the example consumer by connecting to RabbitMQ and then
        starting the IOLoop to block and allow the SelectConnection to operate.

        """
        self._connection = self.connect()
        self._connection.ioloop.start()

    def stop(self):
        """Cleanly shutdown the connection to RabbitMQ by stopping the consumer
        with RabbitMQ. When RabbitMQ confirms the cancellation, on_cancelok
        will be invoked by pika, which will then closing the channel and
        connection. The IOLoop is started again because this method is invoked
        when CTRL-C is pressed raising a KeyboardInterrupt exception. This
        exception stops the IOLoop which needs to be running for pika to
        communicate with RabbitMQ. All of the commands issued prior to starting
        the IOLoop will be buffered but not processed.

        """
        if self._closing:
            return
        LOGGER.info('Stopping')
        self._closing = True
        self.stop_consuming()
        self._connection.ioloop.start()
        LOGGER.info('Stopped')

    def close_connection(self):
        """This method closes the connection to RabbitMQ."""
        LOGGER.info('Closing connection')
        self._connection.close()

    def request_stop(self, request):
        self._prefetch -= 1
        self._channel.basic_qos(prefetch_count = self._prefetch)
        request()
        if 0 == self._prefetch :
            LOGGER.info('Stopping as there are not more Workers')
            self.add_timeout(0,
                             self.stop)
        else:
            LOGGER.info('Worker count changed to ' + str(self._prefetch))


if __name__ == '__main__':
    import logging
    import os
    import argparse

    import pp_engine

    parser = argparse.ArgumentParser(description='Generic Client to consume a RabbitMQ queue.')
    parser.add_argument('-d',
                      '--debug',
                      dest='DEBUG',
                      help='print out detail information to stdout.',
                      action='store_true',
                      default=False)
    parser.add_argument('--live_time',
                        dest='LIVE_TIME',
                        help='The number of seconds that this client should listen for new messages.' + 
                        ' After this time, and once any consumption that was in progress when it expired, the client with gracefully exit.',
                        type=float,
                        default=0.0)
    parser.add_argument('--log_file',
                        dest='LOG_FILE',
                        help='The file, as opposed to stdout, into which to write log messages')
    parser.add_argument('-i',
                        '--rabbit_ini',
                        dest='RABBITMQ_INI',
                        help='The path to the file contains the RabbitMQ INI file, the default is $HOME/.rabbitMQ.ini')
    parser.add_argument('-r',
                        '--restart',
                        dest='RESTART',
                        help='The number of seconds to wait before attempting to reconnect after a lost connection.' + 
                             ' Zero or less means no reconnect will be attempted.',
                        type=int,
                        default=0)
    parser.add_argument('-s',
                        '--ini_section',
                        dest='INI_SECTION',
                        help='The section of the INI file to use for this execution, the default is "RabbitMQ"',
                        default='RabbitMQ')
    parser.add_argument('-t',
                      '--transient',
                      dest='TRANSIENT',
                      help='true if the specified queue is transient rather than durable.',
                      action='store_true',
                      default=False)
    parser.add_argument('-w',
                        '--workers',
                        dest='WORKERS',
                        help='The maximum number of workers that will run in parallel',
                        type=int,
                        default=1)
    parser.add_argument('--wait_time',
                        dest='WAIT_TIME',
                        help='The number of seconds that this client should wait with no message after which the client with gracefully exit.',
                        type=float,
                        default=0.0)
    parser.add_argument('queue',
                        help='The rabbitMQ queue which this client should consume')
    parser.add_argument('consumption_class',
                        help='The module.class of the class that will consumer the messages of the queue')
    options = parser.parse_args()

    if options.DEBUG:
        log_level = logging.INFO
        pp_engine.DEBUG = True
    else:
        log_level = logging.WARNING
    if options.LOG_FILE:
        logging.basicConfig(filename=options.LOG_FILE,
                            level=log_level)
    else:
        logging.basicConfig(Stream=sys.stdout,
                            level=log_level)

    consumption = options.consumption_class
    end = consumption.rfind('.')
    start = end + 1
    if -1 == end or start == len(consumption):
        print >> sys.stderr, "failed to parse specified class name"
        sys.exit(2)
    consumption_module = consumption[:end]
    consumption_class = consumption[start:]
    exec('from ' + consumption_module + ' import ' + consumption_class + ' as ConsumptionImpl')

    parameters = get_parameters(options.RABBITMQ_INI,
                                options.INI_SECTION)
    consumer = Consumer(parameters,
                       options.queue,
                       prefetch=options.WORKERS,
                       restart=options.RESTART,
                       durable=not options.TRANSIENT,
                       live_time=options.LIVE_TIME,
                       wait_time=options.WAIT_TIME)
    try:
        consumer.run()
    except Consumption.StopWorkerException as s:
        LOGGER.info('A stop to consumption has been requested')        
        consumer.stop()
    except KeyboardInterrupt:
        consumer.stop()
