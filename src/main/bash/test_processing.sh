#!/bin/sh
echo Processing now!
while [ "X" != "X${1}" ] ; do
    echo "arg \""${1}"\""
    shift
done
x=$RANDOM 
y=$((x/2))
z=$((y*2))
echo "X, Y and Z are ${x}, ${y} and ${z}" >> $PP_MESSAGE_FILE
exit $((x-z))
