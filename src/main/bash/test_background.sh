#!/bin/sh

out=$2
echo "out:" ${out}
err=$4
echo "err:" ${err}
msg=$6
echo "msg:" ${msg}

out_dir=`dirname ${out}`
if [ ! -d ${out_dir} ] ; then
    mkdir -p ${out_dir}
fi

err_dir=`dirname ${err}`
if [ ! -d ${err_dir} ] ; then
    mkdir -p ${err_dir}
fi

shift 6

echo "Running in Background" > ${msg}

echo "args:" "$@"

nohup "$@" < /dev/null >> ${out} 2>> ${err} &
