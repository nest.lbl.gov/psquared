#!/bin/sh
#!/bin/bash -l
#PBS -q regular
#PBS -l nodes=4:ppn=8
#PBS -l walltime=48:00:00

EXPERIMENT=dyb

. ${HOME}/psquared/${EXPERIMENT}/setup.sh
init_nuwa /global/project/projectdirs/dayabay/releases/NuWa/3.10.1-opt/NuWa-3.10.1

module add python/2.7.1

mpirun -np 32 ${HOME}/psquared/bin/Consume.py -d --listen 169200 -i ${HOME}/psquared/${EXPERIMENT}/config.ini -s RabbitMQ \
       DYB_ESCALIB PsquaredConsumption.PsquaredConsumption
