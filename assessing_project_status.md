# Assessing the project status #

After a while away from a project it is easy to forget the status of it when returning to work with it. This document is a quick cheat-sheet on how to check the current status.

1.  Go to the [psquared repository](https://gitlab.com/nest.lbl.gov/psquared) and see which is the latest tag for that project.
